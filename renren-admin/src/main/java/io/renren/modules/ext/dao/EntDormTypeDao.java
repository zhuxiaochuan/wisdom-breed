package io.renren.modules.ext.dao;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.EntDormDTO;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.entity.EntDormTypeEntity;
import io.renren.modules.ext.form.dorm.DormListQueryForm;
import io.renren.modules.ext.form.dormType.DormTypeListQueryForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 栋舍类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Mapper
public interface EntDormTypeDao extends BaseDao<EntDormTypeEntity> {
    public List<EntDormTypeDTO> listData(@Param("form") final DormTypeListQueryForm form, @Param("pager") Pager pager);

    public int listDataCount(@Param("form") final DormTypeListQueryForm form);
}