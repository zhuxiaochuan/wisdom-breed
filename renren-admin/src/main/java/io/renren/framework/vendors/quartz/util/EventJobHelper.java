package io.renren.framework.vendors.quartz.util;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import io.renren.framework.vendors.quartz.form.FixedTimeForm;
import io.renren.framework.vendors.quartz.form.RepeatJobForm;
import lombok.extern.slf4j.Slf4j;

/**
 * 定时任务工具
 * <p>
 *
 * @author   朱晓川
 */
@Slf4j
public class EventJobHelper {
	
	public static final String PAYLOAD_KEY = "DRP_PAYLOAD"; 
	private static final String JOB_PEFIX = "DRP_JOB_";
	private static final String TRIGGER_PEFIX = "DRP_TRIGGER_";

	private Scheduler scheduler ;
	
	public EventJobHelper(Scheduler scheduler) {
		this.scheduler = scheduler ;
	}
	
	/** 
	 * 调度一个指定时间点的任务 
	 */
	public <T> void quartzFixedTime(Class<? extends Job> jobClazz,FixedTimeForm<T> form) { 
		Date startDate = form.getStartDate(); 
		if(null == startDate) { 
			throw new IllegalArgumentException("任务执行时间不能为空") ;
		}
		try {
			String taskId = form.getTaskId(); 
			// 1、创建jobDetail定义任务内容
			String jobName = JOB_PEFIX + taskId;// + "_" + UUID.fastUUID().toString();
			JobDetail jobDetail = JobBuilder.newJob(jobClazz).withIdentity(jobName).build(); 
					
			JobDataMap jobDataMap = jobDetail.getJobDataMap();
			jobDataMap.put(PAYLOAD_KEY, form.getData());

			// 2、使用trigger定义如何调度
			String triggerName = TRIGGER_PEFIX + taskId; 
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(triggerName).startAt(startDate).build();
			
			// 开始调度
			scheduler.scheduleJob(jobDetail, trigger);
			scheduler.start();
			log.info("----------启动任务:" + jobName + ",时间:" + DateUtil.date());
			log.info("----------任务:" + jobName + ",预计执行时间:" + startDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 调度重复任务
	 * <p>
	 * 注意: 每个任务应该单独使用一个EventJobHelper实例进行调度
	 */
	public <T> void quartzRepeatJob(Class<? extends Job> jobClazz,RepeatJobForm<T> form) { 
		Date startDate = form.getStartDate(); 
		if(null == startDate) { 
			throw new IllegalArgumentException("任务执行时间不能为空") ;
		}
		try {
			String taskId = form.getTaskId();
			// 1、创建jobDetail定义任务内容
			String jobName = JOB_PEFIX + taskId;// + "_" + UUID.fastUUID().toString();
			JobDetail jobDetail = JobBuilder.newJob(jobClazz).withIdentity(jobName).build(); 
			JobDataMap jobDataMap = jobDetail.getJobDataMap();
			jobDataMap.put(PAYLOAD_KEY, form.getData());

			// 2、使用trigger定义如何调度
			SimpleScheduleBuilder simpleSchedule = SimpleScheduleBuilder.simpleSchedule();
			
			int interval = form.getInterval(); 
			DateUnit dateUnit = form.getDateUnit() ; 
			switch(dateUnit) {
			case WEEK:
				simpleSchedule.withIntervalInHours(7 * 24 * interval);
				break ;
			case DAY:
				simpleSchedule.withIntervalInHours(interval * 24);
				break ;
			case HOUR:
				simpleSchedule.withIntervalInHours(interval);
				break ;
			case MINUTE:
				simpleSchedule.withIntervalInMinutes(interval);
				break ;
			case SECOND:
				simpleSchedule.withIntervalInSeconds(interval) ;
				break ;
			case MS:
				simpleSchedule.withIntervalInMilliseconds(interval) ;
				break ;
			default :
				throw new IllegalArgumentException("不支持的时间单位");
			}
			simpleSchedule.repeatForever(); 
			
			String triggerName = TRIGGER_PEFIX + taskId ; 
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(triggerName).startAt(startDate).withSchedule(simpleSchedule).build();
			
			// 开始调度
			scheduler.scheduleJob(jobDetail, trigger);
			scheduler.start();
			log.info("----------启动任务:" + jobName + ",时间:" + DateUtil.date());
			log.info("----------任务:" + jobName + ",预计执行时间:" + startDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 移除一个定时任务
	 * <p>
	 * JobKey与TriggerKey可以在任务执行的上下文进行获取
	 */
	public void removeJob(String taskId) {
		try {
			String jobName = JOB_PEFIX + taskId;
			String triggerName = TRIGGER_PEFIX + taskId ; 
			
			JobKey jobKey = JobKey.jobKey(jobName); 
			TriggerKey triggerKey = TriggerKey.triggerKey(triggerName); 
			
			scheduler.pauseTrigger(triggerKey);// 停止触发器
			scheduler.unscheduleJob(triggerKey);// 移除触发器
			scheduler.deleteJob(jobKey);// 删除任务 
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
}
