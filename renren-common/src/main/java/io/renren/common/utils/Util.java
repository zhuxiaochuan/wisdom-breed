package io.renren.common.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

import cn.hutool.core.util.ObjectUtil;

public class Util {
	
	/**
	 * 判断一个对象是否为空。它支持如下对象类型：
	 * <ul>
	 * <li>null : 一定为空
	 * <li>字符串     : ""为空,多个空格也为空
	 * <li>数组
	 * <li>集合
	 * <li>Map
	 * <li>其他对象 : 一定不为空
	 * </ul>
	 * 
	 * @param obj
	 *            任意对象
	 * @return 是否为空
	 */
	public final static boolean isEmpty(final Object obj) {
		if (obj == null) {
			return true;
		}
		if (obj instanceof String) {
			return "".equals(String.valueOf(obj).trim());
		}
		if (obj.getClass().isArray()) {
			return Array.getLength(obj) == 0;
		}
		if (obj instanceof Collection<?>) {
			return ((Collection<?>) obj).isEmpty();
		}
		if (obj instanceof Map<?, ?>) {
			return ((Map<?, ?>) obj).isEmpty();
		}

		return false;
	}
	
	/**
	 * 判断2个对象是否相等
	 * <p>
	 * 
	 * 比较两个对象是否相等，相等需满足以下条件之一：
	 * obj1 == null && obj2 == null
	 * obj1.equals(obj2)
	 */
	public final static boolean eq(final Object a, final Object b) {
		return ObjectUtil.equals(a, b);
	}

}
