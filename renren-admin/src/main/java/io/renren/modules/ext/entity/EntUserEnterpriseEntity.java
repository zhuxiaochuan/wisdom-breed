package io.renren.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 责任分配表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-24
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_user_enterprise")
public class EntUserEnterpriseEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
	private Long userId;
    /**
     * 企业id
     */
	private Long enterpriseId;
}