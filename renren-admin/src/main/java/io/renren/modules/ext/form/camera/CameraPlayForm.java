package io.renren.modules.ext.form.camera;

import io.renren.framework.vendors.ezviz.enums.EzvizQualityEnum;
import lombok.Data;

@Data
public class CameraPlayForm {
	
	/**
	 * 摄像头id
	 */
	private Long id ;
	
	// 视频清晰度，1-高清（主码流）、2-流畅（子码流）
	private int quality = EzvizQualityEnum.FLUENT.intKey() ;

}
