package io.renren.common.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

public class HttpResponseUtil {
	
	private HttpResponseUtil() {
	}

	/**
	 * json 视图
	 */
	public static void jsonView(HttpServletResponse response,int status, Object obj) {
		response.setCharacterEncoding("UTF-8");

		String result = "";
		if (obj instanceof String) {
			result = (String) obj;
		} else {
			result = JsonUtils.toJsonString(obj);
		}

		//http响应设置

		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("application/json");
		response.setContentLength(-1);
		response.setStatus(status); 

		//输出到客户端

		try {
			response.getWriter().write(result);
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void jsonStatusView(HttpServletResponse response,int status) {
		response.setCharacterEncoding("UTF-8");
		
		String result = "";
		Map<String,Object> data = new HashMap<>() ;
		data.put("statusCode", status);
		result = JsonUtils.toJsonString(data);

		//http响应设置
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("application/json");
		response.setContentLength(-1);
		response.setStatus(200); 

		//输出到客户端
		try {
			response.getWriter().write(result);
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
