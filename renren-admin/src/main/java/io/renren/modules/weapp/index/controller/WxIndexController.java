package io.renren.modules.weapp.index.controller;


import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.page.Pager;
import io.renren.common.utils.RR;
import io.renren.modules.weapp.index.form.WxIndexImageQueryForm;
import io.renren.modules.weapp.index.form.WxIndexQueryForm;
import io.renren.modules.weapp.index.service.WxIndexService;
import lombok.RequiredArgsConstructor;


/**
 * 首页
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/index")
public class WxIndexController {
	
    private final WxIndexService wxIndexService;

    @GetMapping("indexData")
    public RR indexData(final WxIndexQueryForm form,Pager pager){
    	Map<String,Object> data = wxIndexService.indexData(form, pager);
        return RR.ok(data);
    }
    
    @GetMapping("indexDetail")
    public RR indexDetail(final WxIndexQueryForm form){
    	Map<String,Object> data = wxIndexService.indexDetail(form);
        return RR.ok(data);
    }
    
    @GetMapping("deliverDetail")
    public RR deliverDetail(final WxIndexQueryForm form){
    	Map<String,Object> data = wxIndexService.deliverDetail(form);
        return RR.ok(data);
    }
    
    /**
     * 查看图片
     */
    @GetMapping("imageDetail")
    public RR imageDetail(final WxIndexImageQueryForm form){
        return wxIndexService.imageDetail(form);
    }
    
    @GetMapping("enterpriseList")
    public RR enterpriseList(){
        return wxIndexService.enterpriseList();
    }
    
    @GetMapping("placeList")
    public RR placeList(final WxIndexQueryForm form,Pager pager){
        return wxIndexService.placeList(form, pager);
    }
}