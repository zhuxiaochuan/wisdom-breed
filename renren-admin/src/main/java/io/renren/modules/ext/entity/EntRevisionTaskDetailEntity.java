package io.renren.modules.ext.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;

import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 任务详情
 *
 * @author zxc 
 * @since 1.0.0 2022-07-27
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_revision_task_detail")
public class EntRevisionTaskDetailEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 任务id
     */
	private Long taskId;
    /**
     * 摄像头id
     */
	private Long cameraId;
    /**
     * 摄像头SN
     */
	private String cameraSn;
    /**
     * 数量结果
     */
	private Integer revisionCount;
	/**
	 * 已分娩
	 */
	private Integer delivered;
    /**
     * 完成状态(是/否)
     */
	private Integer completeStatus;
    /**
     * 任务完成时间
     */
	private Date completeTime;
}