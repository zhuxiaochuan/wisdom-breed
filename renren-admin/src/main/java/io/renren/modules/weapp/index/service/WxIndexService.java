package io.renren.modules.weapp.index.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tangzc.mpe.bind.Binder;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.renren.common.page.Pager;
import io.renren.common.utils.ConvertUtils;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.framework.jwt.JwtMinappUserDto;
import io.renren.framework.login.WxLoginContextHolder;
import io.renren.modules.ext.dao.EntImageUploadDao;
import io.renren.modules.ext.dao.EntRevisionDataDao;
import io.renren.modules.ext.dao.EntUserManageDao;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.dto.EntImageUploadDTO;
import io.renren.modules.ext.dto.EntRevisionDataDTO;
import io.renren.modules.ext.dto.UserPlaceDTO;
import io.renren.modules.ext.entity.EntCameraEntity;
import io.renren.modules.ext.entity.EntImageUploadEntity;
import io.renren.modules.ext.entity.EntPlaceEntity;
import io.renren.modules.ext.entity.EntRevisionDataEntity;
import io.renren.modules.ext.enums.YesOrNoEnum;
import io.renren.modules.ext.service.EntCameraService;
import io.renren.modules.ext.service.EntDormTypeService;
import io.renren.modules.ext.service.EntPlaceService;
import io.renren.modules.weapp.index.dto.DormTypeCountDto;
import io.renren.modules.weapp.index.form.WxIndexImageQueryForm;
import io.renren.modules.weapp.index.form.WxIndexQueryForm;
import lombok.RequiredArgsConstructor;

/**
 * 首页盘点数据
 */
@Service
@RequiredArgsConstructor
public class WxIndexService {
	
	private final EntRevisionDataDao entRevisionDataDao ;
	
	private final EntDormTypeService entDormTypeService ;
	
	private final EntPlaceService entPlaceService ;
	
	private final EntUserManageDao entUserManageDao ;
	
	private final EntImageUploadDao entImageUploadDao ;
	
	private final EntCameraService entCameraService ;

	public Map<String,Object> indexData(WxIndexQueryForm form, Pager pager) {
		Long enterpriseId = form.getEnterpriseId(); 
		
		Map<String, Object> data = getUserPlaceData(form, pager) ; 
		enterpriseId = (Long) data.get("enterpriseId") ;
		
		if(Util.isEmpty(enterpriseId) || enterpriseId <= 0) {
			throw new IllegalArgumentException("未设置企业"); 
		}
		
		// 1 先按照企业、场地分组进行分页查询
		QueryWrapper<EntRevisionDataEntity> qw = new QueryWrapper<>();
		final Long entpId = enterpriseId ;
		qw.and(wrapper -> wrapper.eq("enterprise_id", entpId));
		
		Long entPlaceId = form.getPlaceId();  
		if(Util.isEmpty(entPlaceId) || entPlaceId <= 0) { 
			List<UserPlaceDTO> placeList = (List<UserPlaceDTO>) data.get("placeList");
			if(Util.isEmpty(placeList)) {
				throw new IllegalArgumentException("该企业无任何场地"); 
			}
			entPlaceId = placeList.get(0).getPlaceId() ;
		}
		
		final Long fPlaceId = entPlaceId ;
		qw.and(wrapper -> wrapper.eq("place_id", fPlaceId));
		
		EntPlaceEntity currentPlace = entPlaceService.selectEnabledById(fPlaceId); 
		if(Util.isEmpty(currentPlace)){
			throw new IllegalArgumentException("场地不存在");
		}
		
		Date date = form.getDate(); 
		if(Util.isEmpty(date)) {
			date = DateUtil.date() ;
		}
		
		DateTime beginOfDay = DateUtil.beginOfDay(date); 
		DateTime endOfDay = DateUtil.endOfDay(date); 
		qw.and(wrapper -> wrapper.gt("create_date", beginOfDay)); 
		qw.and(wrapper -> wrapper.le("create_date", endOfDay)); 
		
//		qw.groupBy("enterprise_id", "place_id","DATE_FORMAT(create_date, '%Y-%m-%d')");
		qw.orderByDesc("create_date");
		
		Page<EntRevisionDataEntity> page = new Page<>(pager.getPageNumber(), pager.getPageSize());
        page = entRevisionDataDao.selectPage(page, qw);
        
		// 2 查询栋舍详情数据并且设置到返回数据中
        Map<Long, EntDormTypeDTO> dormTypeMap = entDormTypeService.queryEnable2Map(); 
        List<EntDormTypeDTO> dormTypeList = new ArrayList<>() ;
		Set<Long> dormTypeIdSet = dormTypeMap.keySet(); 
		if(!Util.isEmpty(dormTypeIdSet)) {
			for(Long id : dormTypeIdSet) {
				EntDormTypeDTO type = dormTypeMap.get(id); 
				
				String keyWord = currentPlace.getKeyWord(); 
				if(!Util.isEmpty(keyWord)) {
					String typeName = type.getTypeName();  
					// 将栋舍类型的第二个字符替换为场地关键字
					if(!Util.isEmpty(typeName) && typeName.contains("公") && typeName.length() >= 2) {
						typeName = typeName.replaceAll("^(.{1}).(.*)$", "$1"+keyWord+"$2");
						type.setTypeName(typeName);
					}
				}
				
				dormTypeList.add(type) ;
			}
		}
		
		// 分别统计各栋舍类型的数量
		Map<String,DormTypeCountDto> tbleData = new HashMap<>() ;
		
        List<EntRevisionDataEntity> records = page.getRecords();
        if(!Util.isEmpty(records)) {
        	Set<Long> ids = records.stream().map(EntRevisionDataEntity::getEnterpriseId).collect(Collectors.toSet());   
        	
        	List<EntRevisionDataEntity> listData = entRevisionDataDao.selectList(Wrappers.lambdaQuery(EntRevisionDataEntity.class)
        			.gt(EntRevisionDataEntity::getCreateDate, beginOfDay).le(EntRevisionDataEntity::getCreateDate, endOfDay)
        			// 企业
        			.in(EntRevisionDataEntity::getEnterpriseId, ids)
        			// 场地
        			.eq(EntRevisionDataEntity::getPlaceId, fPlaceId));
        	if(!Util.isEmpty(listData)) {
        		for(EntRevisionDataEntity one : listData) {
        			Long typeId = one.getDormType().longValue(); 
        			Date createDate = one.getCreateDate(); 
        			String day = DateUtil.format(createDate, "yyyy-MM-dd") ;
        			String key = typeId + "-" + day; 
        			
        			Integer revisionCount = one.getRevisionCount(); 
        			if(Util.isEmpty(revisionCount)){
        				revisionCount = 0 ;
        			}
        			
        			Integer deliverCount = one.getDeliverCount(); 
        			if(Util.isEmpty(deliverCount)){
        				deliverCount = 0 ;
        			}
        			
        			DormTypeCountDto td = tbleData.get(key);  
        			if(null == td) {
        				EntDormTypeDTO dormType = dormTypeMap.get(typeId);
        				
        				td = new DormTypeCountDto() ;
        				td.setId(dormType.getId()); 
        				td.setName(dormType.getTypeName()); 
        				td.setDeliver(dormType.getDeliver()); 
        				td.setValue(revisionCount); 
        				
        				if(Util.eq(dormType.getDeliver(), YesOrNoEnum.YES.intKey())) {
        					td.setDeliverCnt(deliverCount); 
        				}
        				
        				tbleData.put(key, td) ;
        			}else {
        				td.setValue(td.getValue() + revisionCount);
        				
        				if(Util.eq(td.getDeliver(), YesOrNoEnum.YES.intKey())) {
        					td.setDeliverCnt(td.getDeliverCnt() + deliverCount);  
        				}
        			}
        		}
        		
        		// 补足不存在的栋舍类型数据 TODO 是否添加数量为0的项目
        		if(!Util.isEmpty(dormTypeList)) {
    				for(EntDormTypeDTO dt : dormTypeList) {
    					Long typeId = dt.getId(); 
    					String day = DateUtil.format(date, "yyyy-MM-dd") ;
            			String key = typeId + "-" + day; 
            			
            			DormTypeCountDto td = tbleData.get(key); 
            			if(null == td) {
            				td = new DormTypeCountDto() ; 
            				td.setId(dt.getId()); 
            				td.setName(dt.getTypeName()); 
            				td.setDeliver(dt.getDeliver()); 
            				td.setValue(0); 
            				
            				tbleData.put(key, td) ;
            			}
    				}
        		}
        	}
        }
        
        // 按量统计
        List<DormTypeCountDto> typeCount = new ArrayList<> () ;
        Set<String> keySet = tbleData.keySet() ;
        
        // 总数
        Integer allCount = 0 ;
        // 已分娩
        Integer deliverdCnt = 0 ;
        if(!Util.isEmpty(keySet)) {
        	for(String k : keySet) {
        		DormTypeCountDto dto = tbleData.get(k) ; 
        		Integer value = dto.getValue() ; 
        		
        		if(Util.eq(dto.getDeliver(), YesOrNoEnum.YES.intKey())) {
        			allCount += (null == value? 0 : value) ;
        			
        			Integer deliverCnt = dto.getDeliverCnt(); 
        			deliverdCnt += (null == deliverCnt? 0 : deliverCnt) ; 
        		}
        		
        		// 不添加0？
        		typeCount.add(dto) ;
        		if(null != value && value > 0) {
//        			typeCount.add(dto) ;
        		}
        	}
        }
        
        List<DormTypeCountDto> deliverCount = new ArrayList<> () ;
        DormTypeCountDto delivered = new DormTypeCountDto() ;
        delivered.setName("已分娩"); 
        delivered.setValue(deliverdCnt); 
        
        DormTypeCountDto undelivered = new DormTypeCountDto() ;
        undelivered.setName("未分娩");
        undelivered.setValue(allCount - deliverdCnt); 
        
        if(!Util.isEmpty(typeCount)) { 
        	deliverCount.add(delivered);
        	deliverCount.add(undelivered);
        }
        if(allCount > deliverdCnt) {
        	
        }
        
        data.put("placeId", entPlaceId);
        
        // 数量盘点
        if(!Util.isEmpty(typeCount) ) {
        	typeCount = typeCount.stream().filter(t -> !Util.eq(t.getDeliver(), YesOrNoEnum.YES.intKey())).collect(Collectors.toList());
        }       
		data.put("typeCount", typeCount);
        // 分娩盘点
        data.put("deliverCount", deliverCount);
		return data; 
	}
	
	/**
	 * 盘点详情
	 */
	public Map<String,Object> indexDetail(WxIndexQueryForm form) {
		Long enterpriseId = form.getEnterpriseId(); 
		if(Util.isEmpty(enterpriseId) || enterpriseId <= 0) {
			throw new IllegalArgumentException("请选择企业"); 
		}
		Long entPlaceId = form.getPlaceId();  
		if(Util.isEmpty(entPlaceId) || entPlaceId <= 0) {
			throw new IllegalArgumentException("请选择场地"); 
		}
		Date date = form.getDate(); 
		if(Util.isEmpty(date)) {
			date = DateUtil.date() ;
		}
		form.setDate(date); 
		
		// 当天
		List<EntRevisionDataDTO> currentList = queryRevisionData(form); 
		
        // 前一天
        Date beforeDay = DateUtil.offsetDay(date, -1) ;
        form.setDate(beforeDay); 
        List<EntRevisionDataDTO> beforeList = queryRevisionData(form);  
        Map<Long,EntRevisionDataDTO> beforeMap = new HashMap<>() ;
        if(!Util.isEmpty(beforeList)) {
        	beforeMap = beforeList.stream().collect(Collectors.toMap(EntRevisionDataDTO::getDormId, t -> t)) ; 
        }
        
        if(!Util.isEmpty(currentList)) {
        	for(EntRevisionDataDTO curr : currentList) { 
        		// 前一天的盘点数量
        		EntRevisionDataDTO before = beforeMap.get(curr.getDormId()) ;
        		Integer beforeRvCnt = 0 ; 
        		if(!Util.isEmpty(before)) { 
        			beforeRvCnt = before.getRevisionCount() ;
        		}
        		if(null == beforeRvCnt) {
        			beforeRvCnt = 0 ;
        		}
        		
        		// 当天的盘点数量
        		Integer currRvCnt = curr.getRevisionCount() ; 
        		if(null == currRvCnt) {
        			currRvCnt = 0 ;
        		}
        		
        		Integer deltaLastCount = currRvCnt - beforeRvCnt ;
        		curr.setDeltaLastCount(deltaLastCount); 
        	}
        }
        
        
        Map<String,Object> data = new HashMap<>() ;
        if(!Util.isEmpty(currentList) ) {
        	currentList = currentList.stream().filter(t -> !Util.eq(t.getDeliver(), YesOrNoEnum.YES.intKey())).collect(Collectors.toList());
        } 
        data.put("list", currentList);
		return data; 
	}
	
	/**
	 * 分娩详情
	 */
	public Map<String,Object> deliverDetail(WxIndexQueryForm form) {
		Long enterpriseId = form.getEnterpriseId(); 
		if(Util.isEmpty(enterpriseId) || enterpriseId <= 0) {
			throw new IllegalArgumentException("请选择企业"); 
		}
		Long entPlaceId = form.getPlaceId();  
		if(Util.isEmpty(entPlaceId) || entPlaceId <= 0) {
			throw new IllegalArgumentException("请选择场地"); 
		}
		Date date = form.getDate(); 
		if(Util.isEmpty(date)) {
			date = DateUtil.date() ;
		}
		form.setDate(date); 
		
		// 当天
		List<EntRevisionDataDTO> currentList = queryRevisionData(form); 
		
        // 前一天
        Date beforeDay = DateUtil.offsetDay(date, -1) ;
        form.setDate(beforeDay); 
        List<EntRevisionDataDTO> beforeList = queryRevisionData(form);  
        Map<Long,EntRevisionDataDTO> beforeMap = new HashMap<>() ;
        if(!Util.isEmpty(beforeList)) {
        	beforeMap = beforeList.stream().collect(Collectors.toMap(EntRevisionDataDTO::getDormId, t -> t)) ; 
        }
        
        List<EntRevisionDataDTO> deliverList = new ArrayList<>() ; 
        if(!Util.isEmpty(currentList)) {
        	for(EntRevisionDataDTO curr : currentList) { 
        		// 前一天的分娩数量
        		EntRevisionDataDTO before = beforeMap.get(curr.getDormId()) ;
        		Integer beforeDvCnt = 0 ; 
        		if(!Util.isEmpty(before)) {  
        			beforeDvCnt = before.getDeliverCount() ;
        		}
        		if(null == beforeDvCnt) {
        			beforeDvCnt = 0 ;
        		}
        		
        		// 当天的分娩数量
        		Integer currDvCnt = curr.getDeliverCount() ;   
        		if(null == currDvCnt) {
        			currDvCnt = 0 ;
        		}
        		
        		// 当天盘点数量
        		Integer currRvCnt = curr.getRevisionCount(); 
        		if(null == currRvCnt) {
        			currRvCnt = 0 ;
        		}
        		
        		// 前一天盘点数量
        		Integer beforeRvCnt = 0; 
        		if(!Util.isEmpty(before)) {  
        			beforeRvCnt = before.getRevisionCount() ;
        		}
        		if(null == beforeRvCnt) {
        			beforeRvCnt = 0 ;
        		}
        		// 分娩增量
        		Integer deltaLastCount = currDvCnt - beforeDvCnt ;
        		// 未分娩增量
        		Integer deltaLastCountUn = (currRvCnt - currDvCnt) - (beforeRvCnt - beforeDvCnt) ;
        		curr.setDeltaLastCount(deltaLastCount); 
        		curr.setDeltaLastCountUn(deltaLastCountUn); 
        	}
        	deliverList = currentList.stream().filter(dto -> {return Util.eq(dto.getDeliver(), YesOrNoEnum.YES.intKey()) ;}).collect(Collectors.toList()) ;
        }
        
        Map<String,Object> data = new HashMap<>() ;
        data.put("list", deliverList);
		return data; 
	}
	
	/**
	 * 查看图片
	 */
	public RR imageDetail(WxIndexImageQueryForm form) {
		Long enterpriseId = form.getEnterpriseId(); 
		if(Util.isEmpty(enterpriseId) || enterpriseId <= 0) {
			return RR.failed("请选择企业") ;
		}
		Long entPlaceId = form.getPlaceId();  
		if(Util.isEmpty(entPlaceId) || entPlaceId <= 0) {
			return RR.failed("请选择场地") ;
		}
		Long dormId = form.getDormId() ; 
		if(Util.isEmpty(dormId) || dormId <= 0) {
			return RR.failed("请选择栋舍") ;
		}
		
		QueryWrapper<EntImageUploadEntity> qw = new QueryWrapper<>();
		qw.and(wrapper -> wrapper.eq("enterprise_id", enterpriseId));
		qw.and(wrapper -> wrapper.eq("place_id", entPlaceId));
		qw.and(wrapper -> wrapper.eq("dorm_id", dormId));
		
		Date date = form.getDate(); 
		if(Util.isEmpty(date)) {
			date = DateUtil.date() ;
		}
		
		DateTime beginOfDay = DateUtil.beginOfDay(date); 
		DateTime endOfDay = DateUtil.endOfDay(date); 
		qw.and(wrapper -> wrapper.gt("create_date", beginOfDay)); 
		qw.and(wrapper -> wrapper.le("create_date", endOfDay)); 
		qw.orderByAsc("dorm_id");
		qw.orderByDesc("create_date");
        
        List<EntImageUploadEntity> selectList = entImageUploadDao.selectList(qw) ; 
        Binder.bindOn(selectList, EntImageUploadEntity::getCameraName);
        List<EntImageUploadDTO> imageList = ConvertUtils.sourceToTarget(selectList, EntImageUploadDTO.class); 
        
        if(!Util.isEmpty(imageList)) {
        	List<EntCameraEntity> cameraList = entCameraService.cameraList(dormId); 
        	if(!Util.isEmpty(cameraList)) {
        		Map<Long, EntCameraEntity> cmap = cameraList.stream().collect(Collectors.toMap(EntCameraEntity::getId, c -> c)); 
        		
        		for(EntImageUploadDTO img : imageList) {
        			Long cameraId = img.getCameraId(); 
        			EntCameraEntity camera = cmap.get(cameraId); 
        			if(null != camera) {
        				img.setVideoSn(camera.getVideoSn()); 
        				img.setChannel(camera.getChannel()); 
        			}
        		}
        		imageList = imageList.stream().sorted(Comparator
        				.comparing(EntImageUploadDTO::getCameraSn,Comparator.nullsLast(String::compareTo))
        				.thenComparing(EntImageUploadDTO::getChannel, Comparator.nullsLast(Integer::compareTo)) 
        				)
    			.collect(Collectors.toList());
        	}
        }
        
        Map<String,Object> data = new HashMap<>() ;
        data.put("list", imageList);
		return RR.ok(data); 
	}
	
	public RR enterpriseList() { 
		JwtMinappUserDto loginUser = WxLoginContextHolder.getLoginUser() ; 
		Long loginUserId = loginUser.getUserId() ; 
		
		List<UserPlaceDTO> userPlaceList = entUserManageDao.userPlaceList(loginUserId, null); 
		List<UserPlaceDTO> enterpriseList = new ArrayList<>() ;
		if(!Util.isEmpty(userPlaceList)) {
			enterpriseList = userPlaceList.stream().collect( 
					Collectors.collectingAndThen(
				    	        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(UserPlaceDTO::getEnterpriseId))), ArrayList::new)
				    	    );
		}
		
		Map<String,Object> data = new HashMap<>() ;
		data.put("enterpriseList", enterpriseList); 
		return RR.ok(data);  
	}
	
	public RR placeList(WxIndexQueryForm form, Pager pager) {
		Long enterpriseId = form.getEnterpriseId(); 
		if(Util.isEmpty(enterpriseId)) {
			return RR.failed("请选择企业") ;
		}
		
		Map<String, Object> map = getUserPlaceData(form, pager) ; 
		
		Map<String,Object> data = new HashMap<>() ;
		data.put("placeList", map.get("placeList")); 
		return RR.ok(data);  
	}
	
	private List<EntRevisionDataDTO> queryRevisionData(WxIndexQueryForm form) {
		QueryWrapper<EntRevisionDataEntity> qw = new QueryWrapper<>();
		final Long entpId = form.getEnterpriseId() ;
		qw.and(wrapper -> wrapper.eq("enterprise_id", entpId));
		
		final Long fPlaceId = form.getPlaceId() ;
		qw.and(wrapper -> wrapper.eq("place_id", fPlaceId));
		
		Date date = form.getDate(); 
		if(Util.isEmpty(date)) {
			date = DateUtil.date() ;
		}
		
		DateTime beginOfDay = DateUtil.beginOfDay(date); 
		DateTime endOfDay = DateUtil.endOfDay(date); 
		qw.and(wrapper -> wrapper.gt("create_date", beginOfDay)); 
		qw.and(wrapper -> wrapper.le("create_date", endOfDay)); 
		qw.orderByAsc("dorm_id");
		qw.orderByDesc("create_date");
		
		List<EntRevisionDataEntity> list = entRevisionDataDao.selectList(qw);  
		return ConvertUtils.sourceToTarget(list, EntRevisionDataDTO.class);
	}
	
	private Map<String,Object> getUserPlaceData(WxIndexQueryForm form, Pager pager) {
		JwtMinappUserDto loginUser = WxLoginContextHolder.getLoginUser() ; 
		Long loginUserId = loginUser.getUserId() ; 
		Long enterpriseId = form.getEnterpriseId(); 
		
		List<UserPlaceDTO> userPlaceList = entUserManageDao.userPlaceList(loginUserId, null); 
		List<UserPlaceDTO> enterpriseList = new ArrayList<>() ;
		List<UserPlaceDTO> placeList = new ArrayList<>() ;
		if(!Util.isEmpty(userPlaceList)) {
			enterpriseList = userPlaceList.stream().collect( 
					Collectors.collectingAndThen(
				    	        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(UserPlaceDTO::getEnterpriseId))), ArrayList::new)
				    	    );
			
			if(Util.isEmpty(enterpriseId) && !Util.isEmpty(enterpriseList)) {
				enterpriseId = enterpriseList.get(0).getEnterpriseId() ;
			}
			
			final Long entId = enterpriseId ;
			placeList = userPlaceList.stream().filter(up -> {return Util.eq(entId, up.getEnterpriseId());}).collect(Collectors.toList()) ; 
		}
        
        Map<String,Object> data = new HashMap<>() ;
        data.put("enterpriseId", enterpriseId);
        data.put("enterpriseList", enterpriseList);
        data.put("placeList", placeList);
        return data ;
	}
}