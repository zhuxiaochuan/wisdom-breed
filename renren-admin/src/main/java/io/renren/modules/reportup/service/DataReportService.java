package io.renren.modules.reportup.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntCameraDao;
import io.renren.modules.ext.dao.EntImageUploadDao;
import io.renren.modules.ext.dao.EntIpcDao;
import io.renren.modules.ext.dao.EntRevisionTaskCommitDao;
import io.renren.modules.ext.dao.EntRevisionTaskDao;
import io.renren.modules.ext.dao.EntUserEnterpriseDao;
import io.renren.modules.ext.dao.TodayTaskDao;
import io.renren.modules.ext.entity.EntCameraEntity;
import io.renren.modules.ext.entity.EntImageUploadEntity;
import io.renren.modules.ext.entity.EntIpcEntity;
import io.renren.modules.ext.entity.EntRevisionTaskCommitEntity;
import io.renren.modules.ext.entity.EntRevisionTaskEntity;
import io.renren.modules.ext.entity.EntUserEnterpriseEntity;
import io.renren.modules.ext.enums.DeviceTypeEnum;
import io.renren.modules.ext.enums.ImageReportTypeEnum;
import io.renren.modules.ext.enums.TaskCommitStatusEnum;
import io.renren.modules.ext.enums.YesOrNoEnum;
import io.renren.modules.ext.service.EntDormService;
import io.renren.modules.ext.service.EntImageUploadService;
import io.renren.modules.reportup.form.DeviceRptForm;
import io.renren.modules.reportup.form.ImageRptForm;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DataReportService {
	
	protected Logger logger  = LoggerFactory.getLogger(DataReportService.class) ; 
	
	private final EntCameraDao entCameraDao ;
	
	private final EntIpcDao entIpcDao ;
	
	private final EntImageUploadDao entImageUploadDao ;
	
	private final EntUserEnterpriseDao entUserEnterpriseDao ;
	private final EntRevisionTaskDao entRevisionTaskDao ;
	private final TodayTaskDao todayTaskDao ;
	
	private final EntImageUploadService entImageUploadService ;
	private final EntDormService entDormService ;
	
	private final EntRevisionTaskCommitDao entRevisionTaskCommitDao ;
	
	@Autowired
	DataSourceTransactionManager dataSourceTransactionManager;
	@Autowired
	TransactionDefinition transactionDefinition;
	
	// 改成萤石云后无需此业务
	@Deprecated
	public RR reportDevice(final DeviceRptForm form) {
		Integer deviceType = form.getDeviceType() ; 
		String deviceNo = form.getDeviceNo(); 
		
		LambdaQueryWrapper<EntIpcEntity> ipcwrapper = Wrappers.lambdaQuery(EntIpcEntity.class).eq(EntIpcEntity::getVideoSn, deviceNo);
		EntIpcEntity ipc = entIpcDao.selectOne(ipcwrapper);  
		if(Util.isEmpty(ipc)) {
			return RR.failed("硬盘录像机不存在");
		}
		
		if(Util.eq(DeviceTypeEnum.CAMERA.intKey(), deviceType)) { 
			String ip = form.getIp(); 
			if(Util.isEmpty(ip)) {
				return RR.failed("摄像头ip不能为空"); 
			}
			
			// 通过工控机ID以及摄像头IP查询摄像头
			LambdaQueryWrapper<EntCameraEntity> qw = Wrappers.lambdaQuery(EntCameraEntity.class).eq(EntCameraEntity::getIpcId, ipc.getId()).eq(EntCameraEntity::getIp, ip);
			EntCameraEntity camera = entCameraDao.selectOne(qw); 
			if(Util.isEmpty(camera)) {
				return RR.failed("摄像头不存在"); 
			}else {
				LambdaUpdateWrapper<EntCameraEntity> uw = Wrappers.lambdaUpdate(EntCameraEntity.class).set(EntCameraEntity::getReportTime, DateUtil.date()).eq(EntCameraEntity::getId, camera.getId());
				entCameraDao.update(null, uw) ;
				return RR.ok() ;
			}
		}else if(Util.eq(DeviceTypeEnum.IPC.intKey(), deviceType)) {
			LambdaUpdateWrapper<EntIpcEntity> uw = Wrappers.lambdaUpdate(EntIpcEntity.class).set(EntIpcEntity::getReportTime, DateUtil.date()).eq(EntIpcEntity::getVideoSn, deviceNo);
			entIpcDao.update(null, uw) ;
			return RR.ok() ;
		}else {
			return RR.failed("设备类型错误"); 
		}
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public RR reportImage(final ImageRptForm form) {
		synchronized (DataReportService.class) {
			Integer type = form.getType() ;  
			String videoSn = form.getVideoSn();    
			
			LambdaQueryWrapper<EntIpcEntity> ipcwrapper = Wrappers.lambdaQuery(EntIpcEntity.class).eq(EntIpcEntity::getVideoSn, videoSn);
			EntIpcEntity ipc = entIpcDao.selectOne(ipcwrapper);  
			if(Util.isEmpty(ipc)) {
				return RR.failed("硬盘录像机不存在");
			}
			
			Integer channel = form.getChannel();   
			// 通过录像机SN以及摄像头通道号查询摄像头
			LambdaQueryWrapper<EntCameraEntity> qw = Wrappers.lambdaQuery(EntCameraEntity.class).eq(EntCameraEntity::getIpcId, ipc.getId()).eq(EntCameraEntity::getChannel, channel);
			EntCameraEntity camera = entCameraDao.selectOne(qw); 
			if(Util.isEmpty(camera)) {
				return RR.failed("摄像头不存在"); 
			}
			
			// 查找任务负责人
			Long operator = -1l ;
			Long enterpriseId = camera.getEnterpriseId();  
			EntUserEnterpriseEntity ue = entUserEnterpriseDao.selectOne(Wrappers.lambdaQuery(EntUserEnterpriseEntity.class).eq(EntUserEnterpriseEntity::getEnterpriseId, enterpriseId)) ;
			if(Util.isEmpty(ue)) {
				logger.warn("警告!!!摄像头所在企业还未设置负责人,SN:" + videoSn); 
			}else {
				operator = ue.getUserId() ;
			}
			
			DateTime now = DateUtil.date(); 
			DateTime dayBegin = DateUtil.beginOfDay(now); 
			DateTime dayEnd = DateUtil.endOfDay(now); 
			
			/**
			 * 每个摄像头每天执行一次
			 */
			Long cameraId = camera.getId(); 
			Long dormId = camera.getDormId(); 
			
			// 摄像头所在栋舍的当日任务是否已经创建 
			EntRevisionTaskEntity task = entRevisionTaskDao.selectOne(Wrappers.lambdaQuery(EntRevisionTaskEntity.class)
					.eq(EntRevisionTaskEntity::getDormId, dormId)
					.between(EntRevisionTaskEntity::getCreateDate,dayBegin, dayEnd)) ;
			if(Util.isEmpty(task)) {
				List<EntRevisionTaskEntity> taskLst = todayTaskDao.todayTaskForCamera(cameraId) ;  
				if(Util.isEmpty(taskLst)) { 
					return RR.failed("摄像头所在栋舍不存在或未分配责任人，摄像头id:" + cameraId) ;
				}
				
				// 如果企业今日还不存在提交记录，则添加
				task = taskLst.get(0);  
				Long userId = task.getUserId(); 
				EntRevisionTaskCommitEntity commit = entRevisionTaskCommitDao.selectOne(Wrappers.lambdaQuery(EntRevisionTaskCommitEntity.class)
						.eq(EntRevisionTaskCommitEntity::getEnterpriseId, enterpriseId)
						.eq(EntRevisionTaskCommitEntity::getUserId, userId)
						.gt(EntRevisionTaskCommitEntity::getCreateDate, dayBegin)
						.le(EntRevisionTaskCommitEntity::getCreateDate, dayEnd)) ; 
				
				if(Util.isEmpty(commit)) { 
					commit = new EntRevisionTaskCommitEntity() ; 
					commit.setEnterpriseId(enterpriseId); 
					commit.setUserId(userId); 
					commit.setCommitStatus(TaskCommitStatusEnum.UN_COMMIT.intKey()); 
					entRevisionTaskCommitDao.insert(commit) ;
				}
				
				task.setCommitId(commit.getId());
				task.setCreator(userId);  
				entRevisionTaskDao.insert(task) ;
			}
			
			LambdaQueryWrapper<EntImageUploadEntity> lw = Wrappers.lambdaQuery(EntImageUploadEntity.class).eq(EntImageUploadEntity::getCameraId, cameraId)
			.between(EntImageUploadEntity::getCreateDate,dayBegin, dayEnd);  
			EntImageUploadEntity imgUp = entImageUploadDao.selectOne(lw);    
			if(Util.isEmpty(imgUp)) {
				imgUp = new EntImageUploadEntity() ;
				// add
				imgUp.setTaskId(task.getId()); 
				
				imgUp.setCreator(operator); 
				imgUp.setEnterpriseId(camera.getEnterpriseId()); 
				imgUp.setPlaceId(camera.getPlaceId()); 
				imgUp.setDormId(camera.getDormId()); 
				imgUp.setCameraId(cameraId); 
				
				imgUp.setCatchStatus(YesOrNoEnum.NO.intKey()); 
				imgUp.setUploadStatus(YesOrNoEnum.NO.intKey()); 
				entImageUploadDao.insert(imgUp) ; 
			}
			
			Long dormCameraCount = entDormService.dormCameraCount(dormId) ; 
			
			if(Util.eq(ImageReportTypeEnum.CATCH.intKey(), type)) { 
				/**
				 *  抓取上报已废除，暂不考虑
				 */
				// 上报抓取
	//			if(Util.eq(imgUp.getCatchStatus(), YesOrNoEnum.YES.intKey())) {  
	//				return RR.ok() ;
	//			}
	//			LambdaUpdateWrapper<EntImageUploadEntity> uw = Wrappers.lambdaUpdate(EntImageUploadEntity.class)
	//					.set(EntImageUploadEntity::getCatchStatus, YesOrNoEnum.YES.intKey())
	//					.set(EntImageUploadEntity::getCatchDate, now)
	//					// 当天、cameraId
	//					.eq(EntImageUploadEntity::getCameraId, cameraId)
	//					.between(EntImageUploadEntity::getCreateDate,dayBegin, dayEnd);
	//			entImageUploadDao.update(null, uw) ;
	//			
	//			// 如果1个栋舍的所有摄像头都完成抓取，则更新栋舍的抓取状态 
	//			checkDormCatchStatus(now, dormId, dormCameraCount);
				
				return RR.ok() ;
			}else if(Util.eq(ImageReportTypeEnum.UPLOAD.intKey(), type)) {
				// 上报图片
				String imageUrl = form.getImageUrl(); 
				if(Util.isEmpty(imageUrl)) {
					return RR.failed("图片地址不能为空"); 
				}
				if(Util.eq(imgUp.getUploadStatus(), YesOrNoEnum.YES.intKey())) {  
					return RR.ok() ;
				}
				
				LambdaUpdateWrapper<EntImageUploadEntity> uw = Wrappers.lambdaUpdate(EntImageUploadEntity.class)
						.set(EntImageUploadEntity::getImageUrl, imageUrl)
						.set(EntImageUploadEntity::getCatchStatus, YesOrNoEnum.YES.intKey())
						.set(EntImageUploadEntity::getUploadStatus, YesOrNoEnum.YES.intKey())
						.set(EntImageUploadEntity::getCatchDate, now)
						.set(EntImageUploadEntity::getUploadDate, now)
						// 当日
						.eq(EntImageUploadEntity::getCameraId, cameraId)
						.between(EntImageUploadEntity::getCreateDate, dayBegin, dayEnd);
				entImageUploadDao.update(null, uw) ;
				
				// 如果1个栋舍的所有摄像头都完成抓取，则更新栋舍的抓取状态
				// 改用萤石云后不关心抓取状态，无需处理
				// checkDormCatchStatus(now, dormId, dormCameraCount);
				
				// 如果1个栋舍的所有摄像头都完成上传，则更新栋舍的上传状态
				Long uploadCompleteCnt = entImageUploadService.dormUploadCompleteCnt(dormId,now) ; 
				if(Util.eq(dormCameraCount, uploadCompleteCnt)) {
					entRevisionTaskDao.update(null, Wrappers.lambdaUpdate(EntRevisionTaskEntity.class)
							// 上传完成
							.set(EntRevisionTaskEntity::getUploadStatus, YesOrNoEnum.YES.intKey()) 
							// 栋舍
							.eq(EntRevisionTaskEntity::getDormId, dormId)
							// 当日
							.between(EntRevisionTaskEntity::getCreateDate, DateUtil.beginOfDay(now), DateUtil.endOfDay(now)));
				}
				return RR.ok() ;
			}else {
				return RR.failed("上报类型错误"); 
			}
		}
	}

	private void checkDormCatchStatus(DateTime now, Long dormId, Long dormCameraCount) {
		DateTime dayBegin = DateUtil.beginOfDay(now); 
		DateTime dayEnd = DateUtil.endOfDay(now);
		Long catchCompleteCnt = entImageUploadService.dormCatchCompleteCnt(dormId,now) ; 
		if(Util.eq(dormCameraCount, catchCompleteCnt)) {
			entRevisionTaskDao.update(null, Wrappers.lambdaUpdate(EntRevisionTaskEntity.class)
					// 更新为抓取已完成
					.set(EntRevisionTaskEntity::getCatchStatus, YesOrNoEnum.YES.intKey()) 
					// 栋舍
					.eq(EntRevisionTaskEntity::getDormId, dormId)
					// 当日
					.between(EntRevisionTaskEntity::getCreateDate, dayBegin, dayEnd));
		}
	}

}