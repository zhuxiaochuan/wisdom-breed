package io.renren.framework.login;

import org.springframework.util.Assert;

import io.renren.framework.jwt.JwtMinappUserDto;

public class WxLoginContextHolder {
	
	private static final ThreadLocal<JwtMinappUserDto> contextHolder = new InheritableThreadLocal<>();
	
	private WxLoginContextHolder() {}

	public static void clearContext() {
		contextHolder.remove();
	}
	
	/**
	 * 获取登录用户
	 */
	public static JwtMinappUserDto getLoginUser() {
		return contextHolder.get();
	}

	public static void setContext(JwtMinappUserDto context) {
		Assert.notNull(context, "Only non-null user instances are permitted");
		contextHolder.set(context);
	}
}
