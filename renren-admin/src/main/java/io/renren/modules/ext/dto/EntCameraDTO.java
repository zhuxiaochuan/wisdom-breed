package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 摄像头
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@ApiModel(value = "摄像头")
public class EntCameraDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    
	@ApiModelProperty(value = "")
	private Long id;

	@ApiModelProperty(value = "企业id")
	private Long enterpriseId;

	@ApiModelProperty(value = "场地id")
	private Long placeId;

	@ApiModelProperty(value = "栋舍id")
	private Long dormId;
	
	@ApiModelProperty(value = "工控机id")
	private Long ipcId;

	@ApiModelProperty(value = "名称")
	private String cameraName;

	@ApiModelProperty(value = "SN号")
	private String snNumber;

	@ApiModelProperty(value = "IP地址")
	private String ip;
	
	@ApiModelProperty(value = "用户名")
	private String cuserName;
	
	@ApiModelProperty(value = "密码")
	private String password;

	@ApiModelProperty(value = "通道号")
	private Integer channel;

	@ApiModelProperty(value = "在线状态")
	private Integer onlineStatus;

	@ApiModelProperty(value = "创建者")
	private Long creator;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;

	@ApiModelProperty(value = "更新者")
	private Long updater;

	@ApiModelProperty(value = "更新时间")
	private Date updateDate;
	
	@ApiModelProperty(value = "上报时间")
	private Date reportTime;
	
	/**
	 * 状态
	 */
	private Integer status;
	
	private String enterpriseName;
	private String placeName;
	private String dormName;
	private String ipcNo;
	private Integer accessType;
	private String creatorName;
	private String creatorMobile;
	private String updaterName;
	private String updaterMobile;

}