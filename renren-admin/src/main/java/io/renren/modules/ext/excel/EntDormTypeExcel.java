package io.renren.modules.ext.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 栋舍类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
public class EntDormTypeExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "类型名称")
    private String typeName;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;

}