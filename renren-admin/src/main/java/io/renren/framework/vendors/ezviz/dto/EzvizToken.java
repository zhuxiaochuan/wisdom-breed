package io.renren.framework.vendors.ezviz.dto;

import lombok.Data;

@Data
public class EzvizToken {
	
	private String accessToken ;
	
	private Long expireTime ;

}
