package io.renren.modules.ext.form.dorm;

import java.util.Date;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

@Data
public class DormListQueryForm {
	
	// 企业
	private String enterpriseName;
		
	// 场地
	private String placeName;
	
	// 关键字
	private String keyword;
	
	// 栋舍类型
	private Long dormType;
	
	/*创建时间*/
	private Date beginTime ;
	private Date endTime ;
	
	
	public Date getBeginTime() {
		if(!Util.isEmpty(beginTime)) {
			beginTime = DateUtil.beginOfDay(beginTime);
		}
		return beginTime;
	}
	
	public Date getEndTime() {
		if(!Util.isEmpty(endTime)) {
			endTime = DateUtil.endOfDay(endTime); 
		}
		return endTime ;
	}
}
