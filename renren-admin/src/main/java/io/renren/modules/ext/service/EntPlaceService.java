package io.renren.modules.ext.service;

import java.io.Serializable;
import java.util.Map;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.common.utils.RR;
import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.entity.EntPlaceEntity;
import io.renren.modules.ext.form.place.PlaceListQueryForm;

/**
 * 场地
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
public interface EntPlaceService extends CrudService<EntPlaceEntity, EntPlaceDTO> {
	
	/**
	 * 场地列表
	 */
	public PageData<EntPlaceDTO> pageData(final PlaceListQueryForm form,Pager pager);
	/**
	 * 查询启用中的
	 */
	public PageData<EntPlaceDTO> findEnableList(Map<String, Object> params,Pager pager);
	public EntPlaceEntity selectEnabledById(Serializable id);
	
	public RR enable(final Long id);
	
	public RR disable(final Long id);
	
	public EntPlaceDTO placeDetail(Long id);

}