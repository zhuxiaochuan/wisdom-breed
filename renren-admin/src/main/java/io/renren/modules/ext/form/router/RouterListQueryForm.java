package io.renren.modules.ext.form.router;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

import java.util.Date;

@Data
public class RouterListQueryForm {
		
	// 厂家
	private String producer;

	// SIM卡卡号
	private String simNumber;

	// 所属企业
	private String enterprise;

	// 所属场地
	private String place;
	
	/*创建时间*/
	private Date beginTime ;
	private Date endTime ;
	
	
	public Date getBeginTime() {
		if(!Util.isEmpty(beginTime)) {
			beginTime = DateUtil.beginOfDay(beginTime);
		}
		return beginTime;
	}
	
	public Date getEndTime() {
		if(!Util.isEmpty(endTime)) {
			endTime = DateUtil.endOfDay(endTime); 
		}
		return endTime ;
	}
}
