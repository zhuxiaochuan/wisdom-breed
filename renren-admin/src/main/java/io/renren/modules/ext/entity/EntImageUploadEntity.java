package io.renren.modules.ext.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tangzc.mpe.bind.metadata.annotation.BindField;
import com.tangzc.mpe.bind.metadata.annotation.JoinCondition;

import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 图片上传表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-23
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_image_upload")
public class EntImageUploadEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
    /**
     * 任务id
     */
	private Long taskId;
    /**
     * 企业id
     */
	private Long enterpriseId;
    /**
     * 场地id
     */
	private Long placeId;
    /**
     * 栋舍id
     */
	private Long dormId;
    /**
     * 摄像头id
     */
	private Long cameraId;
    /**
     * 抓取状态
     */
	private Integer catchStatus;
    /**
     * 上传状态
     */
	private Integer uploadStatus;
    /**
     * 图片url
     */
	private String imageUrl;
    /**
     * 抓取时间
     */
	private Date catchDate;
    /**
     * 上传时间
     */
	private Date uploadDate;
	
	@BindField(entity = EntCameraEntity.class,field = "cameraName",conditions = { @JoinCondition(selfField = "cameraId",joinField = "id") })
	@TableField(exist = false)
	private String cameraName;
	
}