package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.Data;


/**
 * 后台用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Data
public class AdminUserManageDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	//@ApiModelProperty(value = "")
	private Long id;
	
	private Long oldId;

	//@ApiModelProperty(value = "用户名")
	private String username;

	//@ApiModelProperty(value = "姓名")
	private String realName;

	//@ApiModelProperty(value = "手机号")
	private String mobile;

	//@ApiModelProperty(value = "负责企业")
	private String dutyEnterprise;

	//@ApiModelProperty(value = "状态")
	private Integer status;

	//@ApiModelProperty(value = "状态名称")
	private String statusName;

	//@ApiModelProperty(value = "更新时间")
	private Date createDate;

	//@ApiModelProperty(value = "操作人")
	private String creatorName;
	
	private List<Long> entList = new ArrayList<>();


}