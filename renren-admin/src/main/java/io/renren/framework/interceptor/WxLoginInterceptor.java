package io.renren.framework.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.auth0.jwt.exceptions.TokenExpiredException;

import cn.hutool.http.HttpStatus;
import io.renren.common.utils.HttpResponseUtil;
import io.renren.common.utils.Util;
import io.renren.framework.jwt.JwtMinappUserDto;
import io.renren.framework.jwt.UserTokenManager;
import io.renren.framework.login.WxLoginContextHolder;

public class WxLoginInterceptor implements  HandlerInterceptor{
	
	/**
     * 请求处理之前
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
    	
    	String token = request.getHeader(UserTokenManager.MINAPP_TOKEN_KEY);
		if (token == null || token.isEmpty()) {
			// token 为空 -> 401未登录
			HttpResponseUtil.jsonStatusView(response,HttpStatus.HTTP_UNAUTHORIZED);
			return false;
		} else {
			JwtMinappUserDto loginUser = null; 
			try {
				loginUser = UserTokenManager.getUser4Minapp(token);
			} catch (TokenExpiredException e) {
				// token 非法 -> 403
				HttpResponseUtil.jsonStatusView(response,HttpStatus.HTTP_FORBIDDEN);
				return false;
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (Util.isEmpty(loginUser)) {
				// token -> user null 400
				HttpResponseUtil.jsonStatusView(response,HttpStatus.HTTP_BAD_REQUEST);
				return false;
			} else {
				String tel = loginUser.getTel();
				if (Util.isEmpty(tel)) {
					// 未设置手机号 406
					HttpResponseUtil.jsonStatusView(response,HttpStatus.HTTP_NOT_ACCEPTABLE);
					return false;
				}
				// 每个请求都保存到ThreadLocal
				WxLoginContextHolder.setContext(loginUser); 
				return true ;
			}
		}
    }
 
    /**
     * 请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }
 
    /**
     * 在整个请求结束之后被调用，也就是在DispatcherServlet 渲染了对应的视图之后执行（主要是用于进行资源清理工作）
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    	// 请求结束后清除用户信息
    	WxLoginContextHolder.clearContext();
    }
}
