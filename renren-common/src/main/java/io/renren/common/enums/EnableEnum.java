package io.renren.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum EnableEnum implements IEnum {
	
	ENABLE(1,"启用"),DISABLE(2,"禁用");

	@EnumValue
	private int key;

    private String value;
    
	EnableEnum(Integer key, String value){
        this.key = key;
        this.value = value;
    }

    @Override
	public String key() {
		return String.valueOf(key);
	}

	@Override
	public String value() {
		return value;
	}

	public int intKey() {
		return key;
	}


}
