package io.renren.modules.sys.controller;

import io.renren.common.utils.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页提示
 *
 * @author 
 */
@RestController
public class IndexController {

    @GetMapping("/")
    public Result<String> index(){
        String tips = "你好，后台已启动，请启动ui前端，才能访问页面！";
        return new Result<String>().ok(tips);
    }
}
