package io.renren.modules.ext.form.ipc;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class DeviceQueryForm {
	
	// 序列号集合
	private List<String> deviceSerialList;

}
