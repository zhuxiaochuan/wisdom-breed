package io.renren.framework.vendors.ezviz.form;

import io.renren.framework.vendors.ezviz.enums.EzvizQualityEnum;
import lombok.Data;

@Data
public class EzvizLiveForm {
	
	// 设备序列号,逗号分割，最多50个
	private String deviceSerial ;
	
	// 通道号
	private String channelNo ;
	
	// ezopen协议地址的设备的视频加密密码
	private String code ;
	
	// 流播放协议，1-ezopen、2-hls、3-rtmp、4-flv
	private int protocol = 2 ;
	
	// 视频清晰度，1-高清（主码流）、2-流畅（子码流）
	private int quality = EzvizQualityEnum.FLUENT.intKey() ;

}
