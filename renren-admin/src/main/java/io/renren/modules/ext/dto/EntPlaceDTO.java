package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import io.renren.common.dto.EnableDto;
import lombok.Data;


/**
 * 场地
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
public class EntPlaceDTO extends EnableDto implements Serializable {
    private static final long serialVersionUID = 1L;

	//@ApiModelProperty(value = "id")
	private Long id;
	
	private Long enterpriseId;

	//@ApiModelProperty(value = "场地类型")
	private Long placeType;

	//@ApiModelProperty(value = "场地名称")
	private String placeName;

	//@ApiModelProperty(value = "创建者")
	private Long creator;

	//@ApiModelProperty(value = "创建时间")
	private Date createDate;

	//@ApiModelProperty(value = "更新者")
	private Long updater;

	//@ApiModelProperty(value = "更新时间")
	private Date updateDate;

	// 需要关联查询
	//@ApiModelProperty(value = "场地类型名称")
	private String placeTypeName;
	
	//企业简称
	private String enterpriseName;
	
	private String creatorName;
	private String creatorMobile;
	private String updaterName;
	private String updaterMobile;
	
	 /**
     * 地区代码
     */
	private String areaCode;
    /**
     * 详细地址
     */
	private String address;
	 /**
     * 联系人
     */
	private String contactPerson;
    /**
     * 联系电话
     */
	private String contactNumber;
	/**
     * 栋舍数量
     */
	private Integer dormCount;
	/**
     * 工控机数量
     */
	private Integer ipcCount;
	/**
     * 摄像头数量
     */
	private Integer cameraCount;
	
	private Integer status;
	
	
}