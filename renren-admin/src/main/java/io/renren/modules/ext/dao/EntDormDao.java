package io.renren.modules.ext.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.EntDormDTO;
import io.renren.modules.ext.entity.EntDormEntity;
import io.renren.modules.ext.form.dorm.DormListQueryForm;

/**
 * 栋舍
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Mapper
public interface EntDormDao extends BaseDao<EntDormEntity> {
	
	public List<EntDormDTO> listData(@Param("form") final DormListQueryForm form, @Param("pager") Pager pager);
	
	public int listDataCount(@Param("form") final DormListQueryForm form);
	
}