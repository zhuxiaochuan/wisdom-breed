package io.renren.modules.ext.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.RR;
import io.renren.common.utils.Result;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.ext.dto.EntCameraDTO;
import io.renren.modules.ext.entity.EntIpcEntity;
import io.renren.modules.ext.form.camera.CameraListQueryForm;
import io.renren.modules.ext.form.camera.CameraPlayForm;
import io.renren.modules.ext.service.EntCameraService;
import io.renren.modules.ext.service.EntIpcService;
import io.swagger.annotations.Api;


/**
 * 摄像头
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@RestController
@RequestMapping("ext/entcamera")
@Api(tags="摄像头")
public class EntCameraController {
    @Autowired
    private EntCameraService entCameraService;
    
    @Autowired
    private EntIpcService entIpcService;

    @GetMapping("page")
    @RequiresPermissions("ext:entcamera:page")
    public Result<PageData<EntCameraDTO>> page(final CameraListQueryForm form,final Pager pager){
        PageData<EntCameraDTO> page = entCameraService.pageData(form, pager);

        return new Result<PageData<EntCameraDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @RequiresPermissions("ext:entcamera:info")
    public Result<EntCameraDTO> get(@PathVariable("id") Long id){
        EntCameraDTO data = entCameraService.detail(id); 

        return new Result<EntCameraDTO>().ok(data);
    }

    @PostMapping
    @LogOperation("保存")
    @RequiresPermissions("ext:entcamera:save")
    public Result save(@RequestBody EntCameraDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        entCameraService.save(dto);

        return new Result();
    }

    @PutMapping
    @LogOperation("修改")
    @RequiresPermissions("ext:entcamera:update")
    public Result update(@RequestBody EntCameraDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        entCameraService.update(dto);

        return new Result();
    }

    @RequestMapping(value = { "/enable" })
    @LogOperation("启用")
    public RR enable(@RequestBody final EntCameraDTO dto) {
		return entCameraService.enable(dto.getId());   
	}
    
    @RequestMapping(value = { "/disable" })
    @LogOperation("禁用")
    public RR disable(@RequestBody final EntCameraDTO dto) {
		return entCameraService.disable(dto.getId());   
	}
    
    @GetMapping("/ipcList/{placeId}")
    @RequiresPermissions("ext:entcamera:info")
    public RR ipcList(@PathVariable("placeId") Long placeId){
    	List<EntIpcEntity> list = entIpcService.findByPlace(placeId) ; 
        return RR.ok(list);
    }
    
    /**
     * 查询直播地址
     */
    @GetMapping("/playurl")
    @RequiresPermissions("ext:entcamera:info")
    public RR playurl(final CameraPlayForm pf){
    	RR r = entCameraService.playurl(pf) ; 
        return r;
    }

}