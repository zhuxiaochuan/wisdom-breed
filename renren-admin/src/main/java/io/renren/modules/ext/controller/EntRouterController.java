package io.renren.modules.ext.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.ext.dto.EntPlaceTypeDTO;
import io.renren.modules.ext.dto.EntRouterDTO;
import io.renren.modules.ext.excel.EntRouterExcel;
import io.renren.modules.ext.form.placeType.PlaceTypeListQueryForm;
import io.renren.modules.ext.form.router.RouterListQueryForm;
import io.renren.modules.ext.service.EntRouterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 路由器
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@RestController
@RequestMapping("ext/entrouter")
@Api(tags="路由器")
public class EntRouterController {
    @Autowired
    private EntRouterService entRouterService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("ext:entrouter:page")
    public Result<PageData<EntRouterDTO>> page(final RouterListQueryForm form, final Pager pager){
        PageData<EntRouterDTO> page = entRouterService.pageData(form,pager);
        return new Result<PageData<EntRouterDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("ext:entrouter:info")
    public Result<EntRouterDTO> get(@PathVariable("id") Long id){
        EntRouterDTO data = entRouterService.get(id);

        return new Result<EntRouterDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("ext:entrouter:save")
    public Result save(@RequestBody EntRouterDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        entRouterService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("ext:entrouter:update")
    public Result update(@RequestBody EntRouterDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        entRouterService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("ext:entrouter:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        entRouterService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("ext:entrouter:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<EntRouterDTO> list = entRouterService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, EntRouterExcel.class);
    }

}