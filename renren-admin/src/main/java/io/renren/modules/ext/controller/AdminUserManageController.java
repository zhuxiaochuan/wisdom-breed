package io.renren.modules.ext.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.modules.ext.dto.AdminUserManageDTO;
import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.form.adminuser.AdminUserListQueryForm;
import io.renren.modules.ext.service.AdminUserManageService;
import io.swagger.annotations.Api;


/**
 * 后台用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@RestController
@RequestMapping("ext/adminusermanage")
@Api(tags="后台用户管理")
public class AdminUserManageController {
    @Autowired
    private AdminUserManageService adminUserManageService;

    @GetMapping("page")
    @RequiresPermissions("ext:adminusermanage:page")
    public Result<PageData<AdminUserManageDTO>> page(final AdminUserListQueryForm form,final Pager pager){
    	
        PageData<AdminUserManageDTO> page = adminUserManageService.pageData(form,pager);

        return new Result<PageData<AdminUserManageDTO>>().ok(page);
    }
    
    @DeleteMapping
    @LogOperation("删除")
    @RequiresPermissions("ext:adminusermanage:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        adminUserManageService.delete(ids);

        return new Result();
    }
    
    @GetMapping("{id}")
    @RequiresPermissions("ext:adminusermanage:info")
    public Result<AdminUserManageDTO> get(@PathVariable("id") Long id){
    	AdminUserManageDTO data = adminUserManageService.userDetail(id);

        return new Result<AdminUserManageDTO>().ok(data);
    }
    
    @PostMapping
    @LogOperation("保存")
    @RequiresPermissions("ext:adminusermanage:save")
    public Result save(@RequestBody AdminUserManageDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        adminUserManageService.save(dto);

        return new Result();
    }

}