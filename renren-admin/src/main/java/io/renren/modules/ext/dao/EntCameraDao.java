package io.renren.modules.ext.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.EntCameraDTO;
import io.renren.modules.ext.entity.EntCameraEntity;
import io.renren.modules.ext.form.camera.CameraListQueryForm;

/**
 * 摄像头
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Mapper
public interface EntCameraDao extends BaseDao<EntCameraEntity> {
	
	public List<EntCameraDTO> listData(@Param("form") final CameraListQueryForm form, @Param("pager") Pager pager);
	
	public int listDataCount(@Param("form") final CameraListQueryForm form);
	
}