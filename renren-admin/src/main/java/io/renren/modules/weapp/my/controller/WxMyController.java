package io.renren.modules.weapp.my.controller;


import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.RR;
import io.renren.modules.weapp.my.form.WxUpdatePasswordForm;
import io.renren.modules.weapp.my.service.WxMyService;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/my")
public class WxMyController {
	
	private final WxMyService wxMyService ;

    @PostMapping("updatePassword")
    public RR updatePassword(@Validated final WxUpdatePasswordForm form){
        return wxMyService.updatePassword(form); 
    }
    
}