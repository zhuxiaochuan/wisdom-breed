package io.renren.modules.ext.form.revision;

import java.util.Date;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

@Data
public class EnterpriseRevisionTaskQueryForm {
	
	private Long id;
	
	// 企业
	private String enterpriseName;
		
	// 场地
	private String placeName;
	
	// 栋舍
	private Long dormId;
	private Integer dormType;
	private String dormName;
	
	// 关键字
	private String keyword;
	
	// 状态
	private Integer uploadStatus;
	
	// 用户id
	private Long userId;
	
	/*日期*/
	private Date date ;
	
	private Boolean today = false ;
	
	public Date getBeginTime() {
		if(!Util.isEmpty(date)) {
			return DateUtil.beginOfDay(date);
		}
		return null;
	}
	
	public Date getEndTime() {
		if(!Util.isEmpty(date)) {
			return DateUtil.endOfDay(date); 
		}
		return null;
	}
}
