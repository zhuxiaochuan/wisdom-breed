package io.renren.modules.weapp.login.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.IpUtils;
import io.renren.common.utils.RR;
import io.renren.common.utils.Result;
import io.renren.framework.jwt.JwtMinappUserDto;
import io.renren.framework.login.WxLoginContextHolder;
import io.renren.modules.log.entity.SysLogLoginEntity;
import io.renren.modules.log.enums.LoginOperationEnum;
import io.renren.modules.log.enums.LoginStatusEnum;
import io.renren.modules.log.service.SysLogLoginService;
import io.renren.modules.weapp.login.form.WxLoginForm;
import io.renren.modules.weapp.login.service.WxLoginService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;


/**
 * 手机端登录
 */
@RestController
@RequestMapping("/wx/auth")
@RequiredArgsConstructor
public class WxLoginController {
	
    private final WxLoginService wxLoginService;
    
    private final SysLogLoginService sysLogLoginService;

    // 登录
    @PostMapping("login")
    @ApiOperation(value = "登录")
 	public Object login(HttpServletRequest request,@Validated @RequestBody WxLoginForm form) {
 		try {
 			return wxLoginService.login(request,form);
 		} catch (Exception e) {
 			e.printStackTrace();
 			return new Result().error("接口调用失败");
 		}
 	}
    
    /**
	 * 登录检查
	 */
    @PostMapping("loginCheck")
	public Object loginCheck() {
		return RR.ok();
	}
    
    /**
	 * 退出登录
	 */
	@GetMapping("logout")
	public Object logout(HttpServletRequest request) {
		JwtMinappUserDto user = WxLoginContextHolder.getLoginUser() ; 
		Long userId = user.getUserId(); 

		// 日志
		SysLogLoginEntity log = new SysLogLoginEntity();
		log.setOperation(LoginOperationEnum.LOGOUT.value());
		log.setIp(IpUtils.getIpAddr(request));
		log.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));
		log.setIp(IpUtils.getIpAddr(request));
		log.setStatus(LoginStatusEnum.SUCCESS.value());
		log.setCreator(userId);
		log.setCreatorName(user.getRealName());
		log.setCreateDate(new Date());
		sysLogLoginService.save(log);
		
		return RR.ok(true);
	}
}