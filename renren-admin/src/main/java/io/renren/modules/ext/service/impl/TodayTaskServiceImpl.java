package io.renren.modules.ext.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntRevisionTaskCommitDao;
import io.renren.modules.ext.dao.EntRevisionTaskDao;
import io.renren.modules.ext.dao.TodayTaskDao;
import io.renren.modules.ext.dto.EntRevisionTaskDTO;
import io.renren.modules.ext.entity.EntRevisionTaskCommitEntity;
import io.renren.modules.ext.entity.EntRevisionTaskEntity;
import io.renren.modules.ext.enums.TaskCommitStatusEnum;
import io.renren.modules.ext.form.revision.EnterpriseRevisionTaskQueryForm;
import io.renren.modules.ext.service.TodayTaskService;
import io.renren.modules.security.user.SecurityUser;
import lombok.RequiredArgsConstructor;

/**
 * 今日任务
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Service
@RequiredArgsConstructor
public class TodayTaskServiceImpl extends CrudServiceImpl<EntRevisionTaskDao, EntRevisionTaskEntity, EntRevisionTaskDTO> implements TodayTaskService {
	
	private final TodayTaskDao todayTaskDao ;
	
	private final EntRevisionTaskDao entRevisionTaskDao ;
	
	private final EntRevisionTaskCommitDao entRevisionTaskCommitDao ;
	
	/**
	 * 如果今日任务还不存在，则新增
	 */
	@Transactional
	@Override
	public PageData<EntRevisionTaskDTO> todayTask(EnterpriseRevisionTaskQueryForm form, Pager pager) {
		// 今日
		Long loginUserId = SecurityUser.getUserId(); 
		DateTime now = DateUtil.date();
		
		// 今日提交状态
		Boolean commitStatus = false ;
		
		// 1 目前已添加的任务
		DateTime beginOfDay = DateUtil.beginOfDay(now);  
		DateTime endOfDay = DateUtil.endOfDay(now);
		LambdaQueryWrapper<EntRevisionTaskEntity> lw = Wrappers.lambdaQuery(EntRevisionTaskEntity.class).eq(EntRevisionTaskEntity::getCreator, loginUserId).gt(EntRevisionTaskEntity::getCreateDate, beginOfDay).le(EntRevisionTaskEntity::getCreateDate, endOfDay);
		List<EntRevisionTaskEntity> existsList = baseDao.selectList(lw) ; 
		
		// 2 应该执行的任务
		List<EntRevisionTaskEntity> allList = todayTaskForUser(loginUserId); 
		if(!Util.isEmpty(allList)) {
			// 企业 -> 提交记录
			Map<Long,Long> commitMap = new HashMap<>() ;
			List<EntRevisionTaskEntity> toAdd = allList.stream().filter(task -> {
				task.setCreator(loginUserId); 
				Long entId = task.getEnterpriseId(); 
				commitMap.put(entId, Constant.NON_EXIST_ID) ;
				
				if(Util.isEmpty(existsList)) {
					return true ;
				}else {
					return !existsList.contains(task) ; 
				}
			}).collect(Collectors.toList()) ; 
			
			// 如果企业今日还不存在提交记录，则添加
			Set<Long> entIdSet = commitMap.keySet();
			for(Long enterpriseId:entIdSet) {
				EntRevisionTaskCommitEntity commit = entRevisionTaskCommitDao.selectOne(Wrappers.lambdaQuery(EntRevisionTaskCommitEntity.class)
						.eq(EntRevisionTaskCommitEntity::getEnterpriseId, enterpriseId)
						.eq(EntRevisionTaskCommitEntity::getUserId, loginUserId)
						.gt(EntRevisionTaskCommitEntity::getCreateDate, beginOfDay)
						.le(EntRevisionTaskCommitEntity::getCreateDate, endOfDay)) ; 
				
				if(Util.isEmpty(commit)) { 
					commit = new EntRevisionTaskCommitEntity() ; 
					commit.setEnterpriseId(enterpriseId); 
					commit.setUserId(loginUserId); 
					commit.setCommitStatus(TaskCommitStatusEnum.UN_COMMIT.intKey()); 
					entRevisionTaskCommitDao.insert(commit) ;
				}else {
					if(!Util.eq(TaskCommitStatusEnum.UN_COMMIT.intKey(), commit.getCommitStatus())) {
						commitStatus = true ;
					}
				}
				commitMap.put(enterpriseId, commit.getId()) ;
			}
			 
			if(!Util.isEmpty(toAdd)) {
				for(EntRevisionTaskEntity task : toAdd) {
					Long enterpriseId = task.getEnterpriseId(); 
					Long commitId = commitMap.get(enterpriseId) ; 
					task.setCommitId(commitId) ;
				}
				entRevisionTaskDao.insertBatch(toAdd); 
			}
			
			// 已存在的元素才需要更新
			if(!Util.isEmpty(existsList)) {
				List<EntRevisionTaskEntity> toUpdate= existsList.stream().filter(exist -> { 
					exist.setCreator(loginUserId); 
					
					// 前面新增时allList已经设置了creator
					int index = allList.indexOf(exist) ;  
					if(index >= 0) {
						EntRevisionTaskEntity target = allList.get(index) ;  
						// 更新摄像头数量
						exist.setCameraCount(target.getCameraCount()); 
						
						entRevisionTaskDao.update(null, Wrappers.lambdaUpdate(EntRevisionTaskEntity.class).set(EntRevisionTaskEntity::getCameraCount, exist.getCameraCount()).eq(EntRevisionTaskEntity::getId, exist.getId())) ; 
						
						return true ;
					}else {
						return false ;
					}
				}).collect(Collectors.toList()) ;
				
				// 为防止sql注入，mysql是默认不支持批量更新的，需要配置开启
//				if(!Util.isEmpty(toUpdate)) {
//					entRevisionTaskDao.updateBatch(toUpdate);  
//				}
			}
		}
		
		// 3 查询
		form.setUserId(loginUserId);
		form.setToday(true);
		form.setDate(now); 
		List<EntRevisionTaskEntity> list = entRevisionTaskDao.listData(form, pager) ;   
		int total = entRevisionTaskDao.listDataCount(form);  
		
		PageData<EntRevisionTaskDTO> pageData = getPageData(list, total, EntRevisionTaskDTO.class);
		
		Map<String,Object> extra = new HashMap<>() ;
		extra.put("commitStatus", commitStatus) ;
		pageData.setExtra(extra);
		return pageData;
	}

	@Override
	public QueryWrapper<EntRevisionTaskEntity> getWrapper(Map<String, Object> params) {
		// do nothing
		return null;
	}

	@Override
	public List<EntRevisionTaskEntity> todayTaskForUser(Long userId) {
		return todayTaskDao.todayTaskForUser(userId); 
	}
	
	@Transactional
	@Override
	public RR commit() {
		Long loginUserId = SecurityUser.getUserId(); 
		DateTime now = DateUtil.date();
		
		DateTime beginOfDay = DateUtil.beginOfDay(now);  
		DateTime endOfDay = DateUtil.endOfDay(now);
		
		List<EntRevisionTaskCommitEntity> records = entRevisionTaskCommitDao.selectList(Wrappers.lambdaQuery(EntRevisionTaskCommitEntity.class)
				.eq(EntRevisionTaskCommitEntity::getUserId, loginUserId)
				.gt(EntRevisionTaskCommitEntity::getCreateDate, beginOfDay) 
				.le(EntRevisionTaskCommitEntity::getCreateDate, endOfDay)) ;
		if(Util.isEmpty(records)) {
			return RR.failed("任务还未创建,提交失败") ;
		}else {
			records.forEach(tc -> {
				Integer commitStatus = tc.getCommitStatus(); 
				if(Util.eq(commitStatus, TaskCommitStatusEnum.UN_COMMIT.intKey())) { 
					entRevisionTaskCommitDao.update(null, Wrappers.lambdaUpdate(EntRevisionTaskCommitEntity.class)
							.set(EntRevisionTaskCommitEntity::getCommitStatus, TaskCommitStatusEnum.COMMITED.intKey())
							.set(EntRevisionTaskCommitEntity::getCommitDate, DateUtil.date()) 
							.eq(EntRevisionTaskCommitEntity::getId, tc.getId()));
				}
			});
		}
		return RR.ok();
	}

}