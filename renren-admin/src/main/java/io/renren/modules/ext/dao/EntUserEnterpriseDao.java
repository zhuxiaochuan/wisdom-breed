package io.renren.modules.ext.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.ext.entity.EntUserEnterpriseEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 责任分配表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-24
 */
@Mapper
public interface EntUserEnterpriseDao extends BaseDao<EntUserEnterpriseEntity> {
	
}