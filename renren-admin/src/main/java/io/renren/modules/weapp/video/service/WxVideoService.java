package io.renren.modules.weapp.video.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import io.renren.common.utils.RR;
import io.renren.modules.ext.entity.EntCameraEntity;
import io.renren.modules.ext.entity.EntDormEntity;
import io.renren.modules.ext.service.EntCameraService;
import io.renren.modules.ext.service.EntDormService;
import io.renren.modules.weapp.video.form.WxCameraForm;
import io.renren.modules.weapp.video.form.WxDormForm;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class WxVideoService {
	
	private final EntDormService entDormService ;
	
	private final EntCameraService entCameraService ;
	
	public RR dormList(@Validated final WxDormForm form) {
		Long placeId = form.getPlaceId() ; 
		List<EntDormEntity> dormList = entDormService.dormList(placeId) ; 
		return RR.ok(dormList) ;
	}
	
	public RR cameraList(@Validated final WxCameraForm form) {
		Long dormId = form.getDormId() ;
		List<EntCameraEntity> cameraList = entCameraService.cameraList(dormId); 
		return RR.ok(cameraList) ;
	}
}