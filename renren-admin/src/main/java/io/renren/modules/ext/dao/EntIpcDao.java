package io.renren.modules.ext.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.EntIpcDTO;
import io.renren.modules.ext.entity.EntIpcEntity;
import io.renren.modules.ext.form.ipc.IPCListQueryForm;

/**
 * 工控机
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Mapper
public interface EntIpcDao extends BaseDao<EntIpcEntity> {
	
	public List<EntIpcDTO> listData(@Param("form") final IPCListQueryForm form, @Param("pager") Pager pager);
	
	public int listDataCount(@Param("form") final IPCListQueryForm form);
	
}