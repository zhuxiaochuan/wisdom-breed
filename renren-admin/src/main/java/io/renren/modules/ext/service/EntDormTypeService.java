package io.renren.modules.ext.service;

import java.util.Map;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.modules.ext.dto.EntDormDTO;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.entity.EntDormTypeEntity;
import io.renren.modules.ext.form.dorm.DormListQueryForm;
import io.renren.modules.ext.form.dormType.DormTypeListQueryForm;

/**
 * 栋舍类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
public interface EntDormTypeService extends CrudService<EntDormTypeEntity, EntDormTypeDTO> {
	
	// 查询全部返回map
	public Map<Long,EntDormTypeDTO> queryEnable2Map() ;

	public PageData<EntDormTypeDTO> pageData(final DormTypeListQueryForm form, Pager pager);
	
	public PageData<EntDormTypeDTO> findEnableList(Map<String, Object> params,Pager pager);

}