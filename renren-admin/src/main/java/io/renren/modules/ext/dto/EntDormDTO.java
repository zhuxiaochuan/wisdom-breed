package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;


/**
 * 栋舍
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Data
public class EntDormDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	private Long id;

	private Long enterpriseId;

	private Long placeId;
	
	private Long dormType;

	private String dormName;

	private String keyword;

	private String dormSize;

	private Integer fieldRow;

	private Integer fieldColumn;

	private String fieldSize;

	private Long creator;

	private Date createDate;

	private Long updater;

	private Date updateDate;
	
	private String enterpriseName;
	private String placeName;
	private String dormTypeName;
	private String creatorName;
	private String creatorMobile;
	private String updaterName;
	private String updaterMobile;
	private Integer ipcCount;
	private Integer cameraCount;
	private Integer status;

}