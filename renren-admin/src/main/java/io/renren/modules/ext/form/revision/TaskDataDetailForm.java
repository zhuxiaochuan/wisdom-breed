package io.renren.modules.ext.form.revision;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class TaskDataDetailForm {
	
	// 任务id
	@NotNull(message="请选择任务")
	private Long taskId;
	
}
