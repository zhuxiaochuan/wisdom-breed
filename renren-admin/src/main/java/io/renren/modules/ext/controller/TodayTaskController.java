package io.renren.modules.ext.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.RR;
import io.renren.common.utils.Result;
import io.renren.modules.ext.dto.EntRevisionTaskDTO;
import io.renren.modules.ext.form.revision.EnterpriseRevisionTaskQueryForm;
import io.renren.modules.ext.form.revision.SubmitTaskDataForm;
import io.renren.modules.ext.form.revision.TaskDataDetailForm;
import io.renren.modules.ext.service.EntRevisionTaskService;
import io.renren.modules.ext.service.TodayTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;


/**
 * 今日任务
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@RestController
@RequestMapping("ext/todaytask")
@Api(tags="今日任务")
public class TodayTaskController {
    @Autowired
    private TodayTaskService todayTaskService;
    
    @Autowired
    private EntRevisionTaskService entRevisionTaskService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("ext:todaytask:page")
    public Result<PageData<EntRevisionTaskDTO>> page(final EnterpriseRevisionTaskQueryForm form,Pager pager){
        PageData<EntRevisionTaskDTO> page = todayTaskService.todayTask(form, pager); 

        return new Result<PageData<EntRevisionTaskDTO>>().ok(page);
    }
    
    /**
     * 查询任务执行详情(任务摄像头盘点数据)
     */
    @GetMapping("/taskDataDetail")
    @RequiresPermissions("ext:todaytask:page")
    public RR taskDataDetail(@Validated TaskDataDetailForm form){
    	RR r = entRevisionTaskService.taskDataDetail(form);
        return r;
    }
    
    @RequestMapping(value = { "/submitTaskData" })
    @LogOperation("提交任务数据")
    @RequiresPermissions("ext:todaytask:page")
    public RR submitTaskData(@RequestBody @Validated SubmitTaskDataForm form) {
		return entRevisionTaskService.submitTaskData(form);   
	}
    
    @RequestMapping(value = { "/commit" })
    @LogOperation("提交今日任务")
    @RequiresPermissions("ext:todaytask:page")
    public RR enable() {
		return todayTaskService.commit();   
	}

}