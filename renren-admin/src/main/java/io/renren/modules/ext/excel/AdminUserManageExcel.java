package io.renren.modules.ext.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 后台用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Data
public class AdminUserManageExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "用户名")
    private String username;
    @Excel(name = "姓名")
    private String realName;
    @Excel(name = "手机号")
    private String mobile;
    @Excel(name = "负责企业")
    private String dutyEnterprise;
    @Excel(name = "状态")
    private Integer status;
    @Excel(name = "状态名称")
    private String statusName;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "操作人")
    private String updaterName;

}