package io.renren.modules.ext.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.renren.common.exception.RenExceptionHandler;
import io.renren.framework.vendors.ezviz.dto.EzvizDeviceInfoDto;
import io.renren.framework.vendors.ezviz.enums.EzvizOnlineStatusEnum;
import io.renren.framework.vendors.ezviz.service.EzvizCloudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tangzc.mpe.bind.Binder;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.framework.config.WisdomConfig;
import io.renren.modules.ext.dao.EntIpcDao;
import io.renren.modules.ext.dto.EntIpcDTO;
import io.renren.modules.ext.entity.EntIpcEntity;
import io.renren.modules.ext.form.ipc.IPCListQueryForm;
import io.renren.modules.ext.service.EntIpcService;
import lombok.RequiredArgsConstructor;

/**
 * 工控机
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Service
@RequiredArgsConstructor
public class EntIpcServiceImpl extends CrudServiceImpl<EntIpcDao, EntIpcEntity, EntIpcDTO> implements EntIpcService {

	private static final Logger logger = LoggerFactory.getLogger(EntIpcServiceImpl.class);

	private final EntIpcDao entIpcDao ; 
	
	private final WisdomConfig wisdomConfig ;

	@Autowired
	private EzvizCloudService ezvizCloudService;


	
	// 单表分页查询的条件
    @Override
    public QueryWrapper<EntIpcEntity> getWrapper(Map<String, Object> params){
    	QueryWrapper<EntIpcEntity> wrapper = new QueryWrapper<>();
    	
        return wrapper;
    }
    
    @Override
	public PageData<EntIpcDTO> pageData(IPCListQueryForm form, Pager pager) {
    	form.setOnlineThreshold(wisdomConfig.getOnlineThreshold()); 
    	
		List<EntIpcDTO> list = entIpcDao.listData(form, pager) ;
		// 查询设备状态
		for (EntIpcDTO entIpcDTO :list) {
			try{
				EzvizDeviceInfoDto ezvizDeviceInfoDto = ezvizCloudService.deviceInfo(entIpcDTO.getVideoSn());
				entIpcDTO.setOnlineStatus(ezvizDeviceInfoDto.getStatus());
			} catch (Exception e) {
				entIpcDTO.setOnlineStatus(EzvizOnlineStatusEnum.OFFLINE.intKey());
			}
		}
		int total = entIpcDao.listDataCount(form);  
		return getPageData(list, total, EntIpcDTO.class); 
	}
    
    // 单表page
    @Override
    public PageData<EntIpcDTO> page(Map<String, Object> params) {
        IPage<EntIpcEntity> page = baseDao.selectPage(
                getPage(params, null, false),
                getWrapper(params)
        );

        // 查询填充关联字段
        Binder.bind(page);

        return getPageData(page, currentDtoClass());
    }

	@Override
	public List<EzvizDeviceInfoDto> getDeviceInfo(List<String> deviceSerialList) {
		List<EzvizDeviceInfoDto> ezvizDeviceInfoDtoList = new ArrayList<>();
    	for (String deviceSerial:deviceSerialList) {
    		try{
				EzvizDeviceInfoDto ezvizDeviceInfoDto = ezvizCloudService.deviceInfo(deviceSerial);
				ezvizDeviceInfoDtoList.add(ezvizDeviceInfoDto);
			} catch (Exception e) {
				logger.info(deviceSerial+"查询失败。。。。。");
				EzvizDeviceInfoDto ezvizDeviceInfoDto = new EzvizDeviceInfoDto();
				ezvizDeviceInfoDto.setDeviceSerial(deviceSerial);
				ezvizDeviceInfoDtoList.add(ezvizDeviceInfoDto);
			}
		}
		return ezvizDeviceInfoDtoList;
	}

	@Override
	public List<EntIpcEntity> findByPlace(Long placeId) {
		return entIpcDao.selectList(Wrappers.lambdaQuery(EntIpcEntity.class).eq(EntIpcEntity::getPlaceId, placeId));
	}
}