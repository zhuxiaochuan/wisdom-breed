package io.renren.framework.vendors.ezviz.config;

import java.io.Serializable;

import lombok.Data;

@Data
public class EzvizCloudConfig implements Serializable {
	
    private static final long serialVersionUID = 1L;

    private String appKey;

    private String appSecret;

}