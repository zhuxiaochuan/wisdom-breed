package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import io.renren.common.dto.EnableDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 栋舍类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
public class EntDormTypeDTO extends EnableDto implements Serializable {
    private static final long serialVersionUID = 1L;

	// @ApiModelProperty(value = "id")
	private Long id;

	// @ApiModelProperty(value = "类型名称")
	private String typeName;

	// @ApiModelProperty(value = "是否分娩舍")
	private Integer deliver ;

	// @ApiModelProperty(value = "创建者")
	private Long creator;

	// @ApiModelProperty(value = "创建时间")
	private Date createDate;

	// @ApiModelProperty(value = "更新者")
	private Long updater;

	// @ApiModelProperty(value = "更新时间")
	private Date updateDate;

	private String updaterName;

}