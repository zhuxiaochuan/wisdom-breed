package io.renren.modules.weapp.revision.dto;

import java.io.Serializable;

import lombok.Data;


@Data
public class DataSheetDto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    // 日期
    private String day;
    // 类型id
    private Long id ;
    
    private Integer deliver ;

    // 类型名称
	private String name;
	
	// 数量
	private Integer value = 0; 
	
	public void setValue(Integer value) { 
		this.value = (null == value ? 0 : value) ;
	}

}