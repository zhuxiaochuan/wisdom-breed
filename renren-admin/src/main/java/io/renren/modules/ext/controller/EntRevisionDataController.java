package io.renren.modules.ext.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.modules.ext.dto.EntRevisionDataDTO;
import io.renren.modules.ext.excel.EntRevisionDataExcel;
import io.renren.modules.ext.form.revision.EnterpriseRevisionQueryForm;
import io.renren.modules.ext.service.EntRevisionDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 企业盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@RestController
@RequestMapping("ext/entrevisiondata")
@Api(tags="企业盘点")
public class EntRevisionDataController {
    @Autowired
    private EntRevisionDataService entRevisionDataService;

    @GetMapping("page")
    @RequiresPermissions("ext:entrevisiondata:page")
    public Result<PageData<EntRevisionDataDTO>> page(final EnterpriseRevisionQueryForm form,Pager pager){
        PageData<EntRevisionDataDTO> page = entRevisionDataService.pageData(form, pager);

        return new Result<PageData<EntRevisionDataDTO>>().ok(page);
    }

    //@GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("ext:entrevisiondata:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<EntRevisionDataDTO> list = entRevisionDataService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, EntRevisionDataExcel.class);
    }

}