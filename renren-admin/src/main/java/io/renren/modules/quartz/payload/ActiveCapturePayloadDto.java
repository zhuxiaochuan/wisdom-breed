package io.renren.modules.quartz.payload;

import java.io.Serializable;

import lombok.Data;

/**
 */
@Data
public class ActiveCapturePayloadDto implements Serializable{ 
	
	private static final long serialVersionUID = 1L;
	
	private String taskId ;

}
