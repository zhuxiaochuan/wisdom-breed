package io.renren.modules.ext.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 任务提交表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-27
 */
@Data
@ApiModel(value = "任务提交表")
public class EntRevisionTaskCommitDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Long id;

	@ApiModelProperty(value = "企业id")
	private Long enterpriseId;

	@ApiModelProperty(value = "负责人id")
	private Long userId;

	@ApiModelProperty(value = "提交状态")
	private Integer commitStatus;
	private Date commitDate;

	@ApiModelProperty(value = "发布者")
	private Long postUser;

	@ApiModelProperty(value = "发布时间")
	private Date postDate;

	@ApiModelProperty(value = "创建者")
	private Long creator;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;


}