package io.renren.modules.reportup.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.RR;
import io.renren.modules.ext.service.EntCameraService;
import io.renren.modules.reportup.form.DeviceRptForm;
import io.renren.modules.reportup.form.ImageRptForm;
import io.renren.modules.reportup.service.DataReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

/**
 * 数据上报接口
 * 
 * @author 
 */
@RestController
@RequestMapping("/reportup")
@Api(tags="数据上报")
@RequiredArgsConstructor
public class DataReportController {
	
	private final DataReportService dataReportService ;
	private final EntCameraService entCameraService ;

//	@RequestMapping("/device")
	@ApiOperation(value = "设备状态上报")
	public RR reportDevice(@Validated final DeviceRptForm form) {
		try {
			return dataReportService.reportDevice(form); 
 		} catch (Exception e) {
 			e.printStackTrace();
 			return RR.failed("接口调用失败"); 
 		}
	}
	
	@RequestMapping("/image")
	@ApiOperation(value = "图片上报")
	public RR image(@Validated final ImageRptForm form) {
		try {
			return dataReportService.reportImage(form); 
 		} catch (Exception e) {
 			e.printStackTrace();
 			return RR.failed("接口调用失败"); 
 		}
	}
	
}