package io.renren.modules.ext.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.renren.common.enums.IEnum;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ImageReportTypeEnum implements IEnum {
	
	CATCH(1,"抓取"),UPLOAD(2,"上传");

	@EnumValue
	private int key;

    private String value;
    
	ImageReportTypeEnum(Integer key, String value){
        this.key = key;
        this.value = value;
    }

    @Override
	public String key() {
		return String.valueOf(key);
	}

	@Override
	public String value() {
		return value;
	}

	public int intKey() {
		return key;
	}


}
