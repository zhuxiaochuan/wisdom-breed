package io.renren.modules.weapp.login.form;

import javax.validation.constraints.NotEmpty;

import lombok.Data;

/**
 * 小程序登录参数
 * <p>
 * @author   朱晓川
 */
@Data
public class WxLoginForm {

	// 手机号
	@NotEmpty(message="手机号不能为空")
	private String tel;
	
	// 登录密码
	@NotEmpty(message="登录密码不能为空")
	private String password;

}
