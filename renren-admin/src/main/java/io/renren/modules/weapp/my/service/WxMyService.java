package io.renren.modules.weapp.my.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.renren.common.exception.ErrorCode;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.framework.jwt.JwtMinappUserDto;
import io.renren.framework.login.WxLoginContextHolder;
import io.renren.modules.security.password.PasswordUtils;
import io.renren.modules.sys.dto.SysUserDTO;
import io.renren.modules.sys.service.SysUserService;
import io.renren.modules.weapp.my.form.WxUpdatePasswordForm;
import lombok.RequiredArgsConstructor;

/**
 * 我的
 */
@Service
@RequiredArgsConstructor
public class WxMyService {
	
	@Autowired
	private SysUserService sysUserService;
	
	public RR updatePassword(final WxUpdatePasswordForm form) {
		JwtMinappUserDto loginUser = WxLoginContextHolder.getLoginUser() ; 
		Long userId = loginUser.getUserId(); 
		
		String newPassword = form.getNewPassword(); 
		String confirmPassword = form.getConfirmPassword() ; 
		if(!Util.eq(newPassword, confirmPassword)){ 
			return RR.failed("确认密码不一致") ;
		}
		
		SysUserDTO existUser = sysUserService.get(userId); 
		String oldPassword = existUser.getPassword(); 
		
		//原密码不正确
		if(!PasswordUtils.matches(form.getOriginPassword(),oldPassword)){
			return RR.failed("原密码不正确"); 
		}

		sysUserService.updatePassword(userId, newPassword);
		return RR.ok() ;
	}
	
	
}