package io.renren.modules.ext.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.renren.common.enums.IEnum;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum NetworkAccessTypeEnum implements IEnum {

	WIRED_BROADBAND(1,"有线宽带"), ROUTING(2,"4G路由");

	@EnumValue
	private int key;

    private String value;

	NetworkAccessTypeEnum(Integer key, String value){
        this.key = key;
        this.value = value;
    }

    @Override
	public String key() {
		return String.valueOf(key);
	}

	@Override
	public String value() {
		return value;
	}

	public int intKey() {
		return key;
	}


}
