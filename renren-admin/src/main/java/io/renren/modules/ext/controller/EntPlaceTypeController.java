package io.renren.modules.ext.controller;

import java.util.Map;

import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.form.place.PlaceListQueryForm;
import io.renren.modules.ext.form.placeType.PlaceTypeListQueryForm;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.ext.dto.EntPlaceTypeDTO;
import io.renren.modules.ext.service.EntPlaceTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 场地类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@RestController
@RequestMapping("ext/entplacetype")
@Api(tags="场地类型")
public class EntPlaceTypeController {
    @Autowired
    private EntPlaceTypeService entPlaceTypeService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("ext:entplacetype:page")
    public Result<PageData<EntPlaceTypeDTO>> page(final PlaceTypeListQueryForm form, final Pager pager){
        PageData<EntPlaceTypeDTO> page = entPlaceTypeService.pageData(form,pager);
        return new Result<PageData<EntPlaceTypeDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("ext:entplacetype:info")
    public Result<EntPlaceTypeDTO> get(@PathVariable("id") Long id){
        EntPlaceTypeDTO data = entPlaceTypeService.get(id);

        return new Result<EntPlaceTypeDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("ext:entplacetype:save")
    public Result save(@RequestBody EntPlaceTypeDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        entPlaceTypeService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("ext:entplacetype:update")
    public Result update(@RequestBody EntPlaceTypeDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        entPlaceTypeService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("ext:entplacetype:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        entPlaceTypeService.delete(ids);

        return new Result();
    }

    @GetMapping("listEnable")
    public Result<PageData<EntPlaceTypeDTO>> listEnable(@RequestParam Map<String, Object> params,final Pager pager){
    	PageData<EntPlaceTypeDTO> data = entPlaceTypeService.findEnableList(params,pager);   

        return new Result<PageData<EntPlaceTypeDTO>>().ok(data);
    }

}