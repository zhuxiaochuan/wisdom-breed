package io.renren.modules.ext.service.impl;

import java.util.List;
import java.util.Map;

import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.form.placeType.PlaceTypeListQueryForm;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.renren.common.enums.EnableEnum;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntPlaceTypeDao;
import io.renren.modules.ext.dto.EntPlaceTypeDTO;
import io.renren.modules.ext.entity.EntPlaceTypeEntity;
import io.renren.modules.ext.service.EntPlaceTypeService;

/**
 * 场地类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Service
@RequiredArgsConstructor
public class EntPlaceTypeServiceImpl extends CrudServiceImpl<EntPlaceTypeDao, EntPlaceTypeEntity, EntPlaceTypeDTO> implements EntPlaceTypeService {

	private final EntPlaceTypeDao entPlaceTypeDao ;

    @Override
    public QueryWrapper<EntPlaceTypeEntity> getWrapper(Map<String, Object> params){
    	String status = (String)params.get("status");

        QueryWrapper<EntPlaceTypeEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(status), "status", status);

        return wrapper;
    }

	@Override
	public PageData<EntPlaceTypeDTO> pageData(PlaceTypeListQueryForm form, Pager pager) {
		List<EntPlaceTypeDTO> list = entPlaceTypeDao.listData(form, pager) ;
		int total = entPlaceTypeDao.listDataCount(form);
		return getPageData(list, total, EntPlaceTypeDTO.class);
	}

	@Override
	public PageData<EntPlaceTypeDTO> findEnableList(Map<String, Object> params, Pager pager) {
		String typeName = (String) params.get("name") ;
    	
    	LambdaQueryWrapper<EntPlaceTypeEntity> lw = Wrappers.lambdaQuery(EntPlaceTypeEntity.class);
		if(!Util.isEmpty(typeName)) { 
			lw.like(EntPlaceTypeEntity::getTypeName, typeName);
		}
		lw.eq(EntPlaceTypeEntity::getStatus, EnableEnum.ENABLE.intKey());
		
		Page<EntPlaceTypeEntity> page = new Page<>(pager.getPageNumber(), pager.getPageSize());
		page = baseDao.selectPage(page, lw);
		return getPageData(page, currentDtoClass());
	}


}