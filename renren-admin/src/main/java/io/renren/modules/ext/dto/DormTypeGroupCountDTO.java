package io.renren.modules.ext.dto;

import java.io.Serializable;

import lombok.Data;


/**
 * 栋舍类型 - count
 */
@Data
public class DormTypeGroupCountDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    // 类型名称
	private String Label;
	
	// 数量
	private Integer count = 0;
	
	public void setCount(Integer count) {
		this.count = (null == count ? 0 : count) ;
	}

}