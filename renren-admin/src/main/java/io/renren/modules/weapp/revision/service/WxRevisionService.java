package io.renren.modules.weapp.revision.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.base.Splitter;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import io.renren.framework.jwt.JwtMinappUserDto;
import io.renren.framework.login.WxLoginContextHolder;
import io.renren.modules.ext.dao.EntRevisionDataDao;
import io.renren.modules.ext.dao.EntUserManageDao;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.dto.UserPlaceDTO;
import io.renren.modules.ext.entity.EntPlaceEntity;
import io.renren.modules.ext.entity.EntRevisionDataEntity;
import io.renren.modules.ext.enums.YesOrNoEnum;
import io.renren.modules.ext.service.EntDormTypeService;
import io.renren.modules.ext.service.EntPlaceService;
import io.renren.modules.weapp.revision.dto.DataSheetDto;
import io.renren.modules.weapp.revision.dto.DataSheetRowDto;
import io.renren.modules.weapp.revision.dto.DeliverSheetDto;
import io.renren.modules.weapp.revision.enums.QueryDateTypeEnum;
import io.renren.modules.weapp.revision.form.WxRevisionQueryForm;
import lombok.RequiredArgsConstructor;

/**
 * 盘点报表
 */
@Service
@RequiredArgsConstructor
public class WxRevisionService {
	
	private final EntDormTypeService entDormTypeService ;
	
	private final EntRevisionDataDao entRevisionDataDao ;
	
	private final EntUserManageDao entUserManageDao ;
	
	private final EntPlaceService entPlaceService ;
	
	private String DAY_FORMAT = "yyyy-MM-dd" ;
	
	private String KEY_SPLITTER = "|" ;
	
	public Map<String,Object> data(WxRevisionQueryForm form) {
		Long enterpriseId = form.getEnterpriseId(); 
		if(Util.isEmpty(enterpriseId) || enterpriseId <= 0) {
			throw new IllegalArgumentException("未设置企业"); 
		}
		
		// 1 先按照企业、场地分组进行分页查询
		QueryWrapper<EntRevisionDataEntity> qw = new QueryWrapper<>();
		final Long entpId = enterpriseId ;
		qw.and(wrapper -> wrapper.eq("enterprise_id", entpId));
		
		Long entPlaceId = form.getPlaceId();  
		if(Util.isEmpty(entPlaceId) || entPlaceId <= 0) { 
			List<UserPlaceDTO> placeList = userPlaceList(form) ;
			if(Util.isEmpty(placeList)) {
				throw new IllegalArgumentException("该企业无任何场地"); 
			}
			entPlaceId = placeList.get(0).getPlaceId() ;
		}
		
		final Long fPlaceId = entPlaceId ;
		qw.and(wrapper -> wrapper.eq("place_id", fPlaceId));
		EntPlaceEntity currentPlace = entPlaceService.selectEnabledById(fPlaceId); 
		if(Util.isEmpty(currentPlace)){
			throw new IllegalArgumentException("场地不存在");
		}
		
		Integer queryType = form.getQueryType(); 
		Date now = DateUtil.date(); 
		Date begin = now ;
		if(Util.eq(queryType, QueryDateTypeEnum.WEEK.intKey())) {
			begin = DateUtil.offsetDay(now, -7) ;
		}
		else if(Util.eq(queryType, QueryDateTypeEnum.MONTH.intKey())) {
			begin = DateUtil.offsetMonth(now, -1) ; 
		}
		else if(Util.eq(queryType, QueryDateTypeEnum.SEASON.intKey())) {
			begin = DateUtil.offsetMonth(now, -3) ;	
		}
		else if(Util.eq(queryType, QueryDateTypeEnum.HALF_YEAR.intKey())) {
			begin = DateUtil.offsetMonth(now, -6) ;
		}
		else if(Util.eq(queryType, QueryDateTypeEnum.YEAR.intKey())) {
			begin = DateUtil.offsetMonth(now, -12) ; 
		}
		
		DateTime beginOfDay = DateUtil.beginOfDay(begin); 
		DateTime endOfDay = DateUtil.endOfDay(now); 
		qw.and(wrapper -> wrapper.gt("create_date", beginOfDay)); 
		qw.and(wrapper -> wrapper.le("create_date", endOfDay)); 
		
		qw.orderByDesc("create_date");
		
		List<EntRevisionDataEntity> listData = entRevisionDataDao.selectList(qw);
        
		// 2 查询栋舍详情数据并且设置到返回数据中
        Map<Long, EntDormTypeDTO> dormTypeMap = entDormTypeService.queryEnable2Map(); 
        List<EntDormTypeDTO> dormTypeList = new ArrayList<>() ;
		Set<Long> dormTypeIdSet = dormTypeMap.keySet(); 
		if(!Util.isEmpty(dormTypeIdSet)) {
			for(Long id : dormTypeIdSet) {
				EntDormTypeDTO type = dormTypeMap.get(id); 
				String keyWord = currentPlace.getKeyWord(); 
				if(!Util.isEmpty(keyWord)) {
					String typeName = type.getTypeName();  
					// 将栋舍类型的第二个字符替换为场地关键字
					if(!Util.isEmpty(typeName) && typeName.contains("公") && typeName.length() >= 2) {
						typeName = typeName.replaceAll("^(.{1}).(.*)$", "$1"+keyWord+"$2");
						type.setTypeName(typeName);
					}
				}
				dormTypeList.add(type) ;
			}
		}
		
		// 分别统计各栋舍类型的数量
		Map<String,DataSheetDto> tbleData = new HashMap<>() ; 
		if(!Util.isEmpty(listData)) {
    		for(EntRevisionDataEntity one : listData) {
    			Long dormTypeId = one.getDormType().longValue();  
    			Date createDate = one.getCreateDate(); 
    			String day = DateUtil.format(createDate, DAY_FORMAT) ;
    			String key = dormTypeId + KEY_SPLITTER + day; 
    			
    			DataSheetDto td = tbleData.get(key);  
    			if(null == td) {
    				EntDormTypeDTO dormType = dormTypeMap.get(dormTypeId);
    				
    				td = new DataSheetDto() ;
    				td.setId(dormType.getId()); 
    				td.setName(dormType.getTypeName()); 
    				td.setDeliver(dormType.getDeliver()); 
    				td.setValue(one.getRevisionCount()); 
    				
    				tbleData.put(key, td) ;
    			}else {
    				td.setValue(td.getValue() + one.getRevisionCount());
    			}
    		}
    		
    		// 补足不存在的栋舍类型数据
    		for(EntRevisionDataEntity one : listData) {
    			if(!Util.isEmpty(dormTypeList)) {
    				for(EntDormTypeDTO dt : dormTypeList) { 
    					Long dormTypeId = dt.getId(); 
    					Date createDate = one.getCreateDate(); 
    					String day = DateUtil.format(createDate, DAY_FORMAT) ;
            			String key = dormTypeId + KEY_SPLITTER + day; 
            			
            			DataSheetDto td = tbleData.get(key); 
            			if(null == td) {
            				td = new DataSheetDto() ; 
            				td.setId(dt.getId()); 
            				td.setName(dt.getTypeName()); 
            				td.setDeliver(dt.getDeliver()); 
            				td.setValue(0); 
            				
            				tbleData.put(key, td) ;
            			}
    				}
    			}
    		}
    	}
        
        // 按量统计
		List<DataSheetRowDto> dataSheet = new ArrayList<>() ;
        Set<DataSheetRowDto> dataRowSet = new HashSet<> () ; 
        Set<String> keySet = tbleData.keySet() ;
        
        if(!Util.isEmpty(keySet)) {
        	for(String k : keySet) {
        		String day = Splitter.on(KEY_SPLITTER).splitToList(k).get(1) ;   
        		
        		DataSheetRowDto row = new DataSheetRowDto() ; 
        		row.setDay(day); 
        		row.setDayDate(DateUtil.parseDate(day)); 
        		dataRowSet.add(row) ;
        	}
        	dataSheet.addAll(dataRowSet) ;
        	dataSheet = dataSheet.stream().sorted(Comparator.comparing(DataSheetRowDto::getDayDate).reversed()).collect(Collectors.toList()) ; 
        }
        
        Map<String, Object> data = new HashMap<>() ; 
        data.put("enterpriseId", enterpriseId);
        data.put("placeId", entPlaceId);
        data.put("dormTypeList", dormTypeList); 
        data.put("tbleData", tbleData); 
        data.put("dataSheet", dataSheet); 
		return data; 
	}
	
	public Map<String,Object> deliverData(WxRevisionQueryForm form) {
		Long enterpriseId = form.getEnterpriseId(); 
		if(Util.isEmpty(enterpriseId) || enterpriseId <= 0) {
			throw new IllegalArgumentException("未设置企业"); 
		}
		
		// 1 先按照企业、场地分组进行分页查询
		QueryWrapper<EntRevisionDataEntity> qw = new QueryWrapper<>();
		final Long entpId = enterpriseId ;
		qw.and(wrapper -> wrapper.eq("enterprise_id", entpId));
		
		Long entPlaceId = form.getPlaceId();  
		if(Util.isEmpty(entPlaceId) || entPlaceId <= 0) { 
			List<UserPlaceDTO> placeList = userPlaceList(form) ;
			if(Util.isEmpty(placeList)) {
				throw new IllegalArgumentException("该企业无任何场地"); 
			}
			entPlaceId = placeList.get(0).getPlaceId() ;
		}
		
		final Long fPlaceId = entPlaceId ;
		qw.and(wrapper -> wrapper.eq("place_id", fPlaceId));
		
		Integer queryType = form.getQueryType(); 
		Date now = DateUtil.date(); 
		Date begin = now ;
		if(Util.eq(queryType, QueryDateTypeEnum.WEEK.intKey())) {
			begin = DateUtil.offsetDay(now, -7) ;
		}
		else if(Util.eq(queryType, QueryDateTypeEnum.MONTH.intKey())) {
			begin = DateUtil.offsetMonth(now, -1) ; 
		}
		else if(Util.eq(queryType, QueryDateTypeEnum.SEASON.intKey())) {
			begin = DateUtil.offsetMonth(now, -3) ;	
		}
		else if(Util.eq(queryType, QueryDateTypeEnum.HALF_YEAR.intKey())) {
			begin = DateUtil.offsetMonth(now, -6) ;
		}
		else if(Util.eq(queryType, QueryDateTypeEnum.YEAR.intKey())) {
			begin = DateUtil.offsetMonth(now, -12) ; 
		}
		
		DateTime beginOfDay = DateUtil.beginOfDay(begin); 
		DateTime endOfDay = DateUtil.endOfDay(now); 
		qw.and(wrapper -> wrapper.gt("create_date", beginOfDay)); 
		qw.and(wrapper -> wrapper.le("create_date", endOfDay)); 
		
		qw.orderByDesc("create_date");
		
		List<EntRevisionDataEntity> listData = entRevisionDataDao.selectList(qw); 
        
		// 2 查询栋舍详情数据并且设置到返回数据中
        Map<Long, EntDormTypeDTO> dormTypeMap = entDormTypeService.queryEnable2Map(); 
        List<EntDormTypeDTO> dormTypeList = new ArrayList<>() ;
		Set<Long> dormTypeIdSet = dormTypeMap.keySet(); 
		if(!Util.isEmpty(dormTypeIdSet)) {
			for(Long id : dormTypeIdSet) {
				EntDormTypeDTO type = dormTypeMap.get(id); 
				dormTypeList.add(type) ;
			}
		}
		
		// 分别统计各栋舍类型的数量
		Map<String,DeliverSheetDto> tbleData = new HashMap<>() ;
    	if(!Util.isEmpty(listData)) {
    		// 过滤得到分娩数据
    		List<EntRevisionDataEntity> deliverAll = listData.stream().filter(r -> {return Util.eq(r.getDeliver(), YesOrNoEnum.YES.intKey()) ;}).collect(Collectors.toList()) ;
    		for(EntRevisionDataEntity one : deliverAll) { 
    			Long dormTypeId = one.getDormType().longValue(); 
    			
    			Date createDate = one.getCreateDate(); 
    			String day = DateUtil.format(createDate, DAY_FORMAT) ;
    			
    			String key = dormTypeId + KEY_SPLITTER + day; 
    			
    			DeliverSheetDto td = tbleData.get(key);  
    			if(null == td) {
    				EntDormTypeDTO dormType = dormTypeMap.get(dormTypeId);
    				
    				td = new DeliverSheetDto() ;
    				td.setId(dormType.getId()); 
    				td.setName(dormType.getTypeName()); 
    				td.setRevisionCount(one.getRevisionCount());
    				td.setDeliverCount(one.getDeliverCount());  
    				
    				tbleData.put(key, td) ;
    			}else {
    				td.setRevisionCount(td.getRevisionCount() + one.getRevisionCount());
    				td.setDeliverCount(td.getDeliverCount() + one.getDeliverCount());
    			}
    		}
    	}
        
        // 分娩统计
    	// 因为分娩统计其实就是单一分娩舍的统计，因此这里可以直接取tbleData的key，每天只有一个
        List<DeliverSheetDto> deliverSheet = new ArrayList<> () ; 
        Set<String> keySet = tbleData.keySet() ;
        
        if(!Util.isEmpty(keySet)) {
        	for(String k : keySet) {
        		String dayStr = Splitter.on(KEY_SPLITTER).splitToList(k).get(1) ;   
        		Date day = DateUtil.parse(dayStr,DAY_FORMAT);
        		
        		DeliverSheetDto dto = tbleData.get(k) ;
        		dto.setDay(day); 
        		
        		deliverSheet.add(dto) ;
        	}
        	deliverSheet = deliverSheet.stream().sorted(Comparator.comparing(DeliverSheetDto::getDay).reversed()).collect(Collectors.toList()) ;
        }
        
        Map<String, Object> data = new HashMap<>() ; 
        data.put("deliverSheet", deliverSheet); 
		return data; 
	}
	
	private List<UserPlaceDTO> userPlaceList(WxRevisionQueryForm form) {
		JwtMinappUserDto loginUser = WxLoginContextHolder.getLoginUser() ; 
		Long loginUserId = loginUser.getUserId() ; 
		Long enterpriseId = form.getEnterpriseId(); 
		
		List<UserPlaceDTO> userPlaceList = entUserManageDao.userPlaceList(loginUserId, null); 
		List<UserPlaceDTO> placeList = new ArrayList<>() ;
		if(!Util.isEmpty(userPlaceList)) {
			final Long entId = enterpriseId ;
			placeList = userPlaceList.stream().filter(up -> {return Util.eq(entId, up.getEnterpriseId());}).collect(Collectors.toList()) ; 
		}
        return placeList ;
	}
}