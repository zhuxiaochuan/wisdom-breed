package io.renren.modules.ext.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.renren.common.enums.EnableEnum;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntDormDao;
import io.renren.modules.ext.dao.EntDormDao;
import io.renren.modules.ext.dao.EntRevisionDataDao;
import io.renren.modules.ext.dto.DormTypeGroupCountDTO;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.dto.PlaceRevisionDTO;
import io.renren.modules.ext.entity.EntDormEntity;
import io.renren.modules.ext.entity.EntRevisionDataEntity;
import io.renren.modules.ext.enums.YesOrNoEnum;
import io.renren.modules.ext.form.revision.PlaceRevisionQueryForm;
import io.renren.modules.ext.service.EntDormTypeService;
import io.renren.modules.ext.service.PlaceRevisionService;
import lombok.RequiredArgsConstructor;

/**
 * 场地盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Service
@RequiredArgsConstructor
public class PlaceRevisionServiceImpl extends CrudServiceImpl<EntRevisionDataDao, EntRevisionDataEntity, PlaceRevisionDTO> implements PlaceRevisionService {
	
	private final EntRevisionDataDao entRevisionDataDao ;
	
	private final EntDormDao entDormDao ;
	
	private final EntDormTypeService entDormTypeService ;

	@Override
	public PageData<PlaceRevisionDTO> pageData(PlaceRevisionQueryForm form, Pager pager) {
		// 1 先按照企业、场地分组进行分页查询
		QueryWrapper<EntRevisionDataEntity> qw = new QueryWrapper<>();
		
		String enterpriseName = form.getEnterpriseName();
		if(!Util.isEmpty(enterpriseName)) { 
			if(!Util.isEmpty(enterpriseName)) {
				qw.and(wrapper -> wrapper.like("enterprise_name", enterpriseName));
			}
		}
		
		String placeName = form.getPlaceName(); 
		if(!Util.isEmpty(placeName)) { 
			if(!Util.isEmpty(placeName)) {
				qw.and(wrapper -> wrapper.like("place_name", placeName));
			}
		}
		
		Date date = form.getDate(); 
		if(!Util.isEmpty(date)) {
			DateTime beginOfDay = DateUtil.beginOfDay(date); 
			DateTime endOfDay = DateUtil.endOfDay(date); 
			qw.and(wrapper -> wrapper.gt("create_date", beginOfDay)); 
			qw.and(wrapper -> wrapper.le("create_date", endOfDay)); 
		}
		
		qw.groupBy("enterprise_id", "place_id","DATE_FORMAT(create_date, '%Y-%m-%d')");
		qw.orderByDesc("create_date");
		
		Page<EntRevisionDataEntity> page = new Page<>(pager.getPageNumber(), pager.getPageSize());
        Page<EntRevisionDataEntity> pg = entRevisionDataDao.selectPage(page, qw);
        
		// 2 查询栋舍详情数据并且设置到返回数据中
        Map<String,Object> extra = new HashMap<>() ;
       
        Map<Long, EntDormTypeDTO> dormTypeMap = entDormTypeService.queryEnable2Map(); 
        List<EntDormTypeDTO> dormTypeList = new ArrayList<>() ;
		Set<Long> dormTypeIdSet = dormTypeMap.keySet(); 
		if(!Util.isEmpty(dormTypeIdSet)) {
			for(Long id : dormTypeIdSet) {
				EntDormTypeDTO type = dormTypeMap.get(id); 
				dormTypeList.add(type) ;
			}
		}
		
		// 分别统计各栋舍类型的数量
		Map<String,DormTypeGroupCountDTO> tbleData = new HashMap<>() ;
		
        List<EntRevisionDataEntity> records = pg.getRecords();
        if(!Util.isEmpty(records)) {
        	List<Long> ids = records.stream().map(EntRevisionDataEntity::getEnterpriseId).collect(Collectors.toList()); 
        	records.stream().forEach(r -> {
        		Long dormCount = entDormDao.selectCount(Wrappers.lambdaQuery(EntDormEntity.class).eq(EntDormEntity::getPlaceId, r.getPlaceId()).eq(EntDormEntity::getStatus,EnableEnum.ENABLE.intKey())); 
        		r.setDormCount(Integer.valueOf(dormCount + ""));  
        	});
        	
        	List<EntRevisionDataEntity> listData = entRevisionDataDao.selectList(Wrappers.lambdaQuery(EntRevisionDataEntity.class).in(EntRevisionDataEntity::getEnterpriseId, ids));
        	if(!Util.isEmpty(listData)) {
        		for(EntRevisionDataEntity one : listData) {
        			Long typeId = one.getDormType().longValue(); 
        			Long placeId = one.getPlaceId();
        			Date createDate = one.getCreateDate(); 
					String day = DateUtil.format(createDate, "yyyy-MM-dd") ;
        			// 企业id-场地id-栋舍类型
        			String key = one.getEnterpriseId() + "-" + placeId + "-" + typeId + "-" + day; 
        			
        			EntDormTypeDTO dormType = dormTypeMap.get(typeId);
        			DormTypeGroupCountDTO cd = tbleData.get(key); 
        			if(null == cd) {
        				cd = new DormTypeGroupCountDTO() ;
        				cd.setLabel(dormType.getTypeName()); 
        				cd.setCount(one.getRevisionCount()); 
        				
        				tbleData.put(key, cd) ;
        			}else {
        				cd.setCount(cd.getCount() + one.getRevisionCount());
        			}
        			
        			// 企业按天统计 分娩数量(不包含非分娩舍)
    				if(Util.eq(dormType.getDeliver(), YesOrNoEnum.YES.intKey())) {
    					String deliverKey = one.getEnterpriseId() + "-" + placeId + "-已分娩-" + day; 
        				DormTypeGroupCountDTO deliverCd = tbleData.get(deliverKey);
    					if(null == deliverCd) {
    						deliverCd = new DormTypeGroupCountDTO() ;
    						deliverCd.setLabel(dormType.getTypeName()); 
    						deliverCd.setCount(one.getDeliverCount()); 
            				
            				tbleData.put(deliverKey, deliverCd) ;
        				}else {
        					deliverCd.setCount(deliverCd.getCount() + one.getDeliverCount());
        				}
    					
    					String undeliverKey = one.getEnterpriseId() + "-" + placeId + "-未分娩-" + day; 
        				DormTypeGroupCountDTO undeliverCd = tbleData.get(undeliverKey); 
        				if(null == undeliverCd) {
        					undeliverCd = new DormTypeGroupCountDTO() ;
    						undeliverCd.setLabel(dormType.getTypeName()); 
    						
    						Integer undeliverCount = one.getRevisionCount() - one.getDeliverCount();
    						undeliverCd.setCount(undeliverCount); 
    						
    						tbleData.put(undeliverKey, undeliverCd) ;
        					
        				}else {
        					Integer undeliverCount = one.getRevisionCount() - one.getDeliverCount();
        					undeliverCd.setCount(undeliverCd.getCount() + undeliverCount);
        				}
        			}
        		}
        		
        		// 补足不存在的栋舍类型数据
        		for(EntRevisionDataEntity one : listData) {
        			if(!Util.isEmpty(dormTypeList)) {
        				for(EntDormTypeDTO dt : dormTypeList) {
        					Long typeId = dt.getId(); 
        					Long placeId = one.getPlaceId();
        					Date createDate = one.getCreateDate(); 
                			String day = DateUtil.format(createDate, "yyyy-MM-dd") ;
                			String key = one.getEnterpriseId() + "-" + placeId + "-" + typeId + "-" + day; 
                			
                			DormTypeGroupCountDTO cd = tbleData.get(key); 
                			if(null == cd) {
                				cd = new DormTypeGroupCountDTO() ;
                				cd.setLabel(dt.getTypeName()); 
                				cd.setCount(0); 
                				
                				tbleData.put(key, cd) ;
                			}
        				}
        			}
        		}
        	}
        }
        
        PageData<PlaceRevisionDTO> pageData = getPageData(pg, PlaceRevisionDTO.class); 
        
        extra.put("dormTypeList", dormTypeList);
        extra.put("tbleData", tbleData);
        pageData.setExtra(extra); 
		return pageData; 
	}

	@Override
	public QueryWrapper<EntRevisionDataEntity> getWrapper(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}


}