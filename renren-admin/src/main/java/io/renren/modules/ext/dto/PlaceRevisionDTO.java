package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import io.swagger.annotations.ApiModel;
import lombok.Data;


/**
 * 场地盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Data
@ApiModel(value = "场地盘点")
public class PlaceRevisionDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	private Long id;

	private Long enterpriseId;

	private String enterpriseName;

	private Long placeId;

	private String placeName;

	private Integer placeCount;

	private Long dormId;

	private String dormName;

	private Integer dormCount;

	private Integer dormType;

	private String dormTypeName;

	private Integer revisionCount;

	private Long creator;

	private Date createDate;
	private String day;
	
	public String getDay() {
		if(!Util.isEmpty(createDate)) {
			day = DateUtil.format(createDate, "yyyy-MM-dd") ;
		}
		return day ;
	}

}