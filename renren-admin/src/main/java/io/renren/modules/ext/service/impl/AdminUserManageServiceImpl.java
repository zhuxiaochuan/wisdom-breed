package io.renren.modules.ext.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.AdminUserManageDao;
import io.renren.modules.ext.dto.AdminUserManageDTO;
import io.renren.modules.ext.dto.EntTaskUserDTO;
import io.renren.modules.ext.entity.EntUserEnterpriseEntity;
import io.renren.modules.ext.form.adminuser.AdminUserListQueryForm;
import io.renren.modules.ext.form.adminuser.TaskUserForm;
import io.renren.modules.ext.service.AdminUserManageService;
import lombok.RequiredArgsConstructor;

/**
 * 后台用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Service
@RequiredArgsConstructor
public class AdminUserManageServiceImpl extends CrudServiceImpl<AdminUserManageDao, EntUserEnterpriseEntity, AdminUserManageDTO> implements AdminUserManageService {
	
	private final AdminUserManageDao adminUserManageDao ;
	
	@Override
	public PageData<AdminUserManageDTO> pageData(AdminUserListQueryForm form, Pager pager) {
		List<AdminUserManageDTO> list = adminUserManageDao.listData(form, pager) ;   
		int total = adminUserManageDao.listDataCount(form);  
		return getPageData(list, total, AdminUserManageDTO.class); 
	}

	@Override
	public QueryWrapper<EntUserEnterpriseEntity> getWrapper(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
    public void delete(Long[] ids) {
		QueryWrapper<EntUserEnterpriseEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("user_id", ids);
		List<EntUserEnterpriseEntity> selectList = baseDao.selectList(queryWrapper);
        baseDao.deleteBatchIds(selectList);
    }

	@Override
	public AdminUserManageDTO userDetail(Long id) {
		AdminUserManageDTO dto = new  AdminUserManageDTO();
		dto.setId(id);
		QueryWrapper<EntUserEnterpriseEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", id);
        List<EntUserEnterpriseEntity> selectList = adminUserManageDao.selectList(queryWrapper); 
        if(!Util.isEmpty(selectList)) {
        	List<Long> entList = new ArrayList<Long>();
        	for (EntUserEnterpriseEntity ent : selectList) {
        		entList.add(ent.getEnterpriseId());
			}
        	dto.setEntList(entList);
        }
    	return dto ;
	}
	
	
	@Override
	@Transactional
    public void save(AdminUserManageDTO dto) {
		Long id = dto.getId();
		QueryWrapper<EntUserEnterpriseEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", id);
		List<EntUserEnterpriseEntity> selectList = baseDao.selectList(queryWrapper);
		if(!Util.isEmpty(selectList)) {
			baseDao.deleteBatchIds(selectList);
		}    
        List<EntUserEnterpriseEntity> addList = new ArrayList<EntUserEnterpriseEntity>();
        List<Long> entList = dto.getEntList();
        if(!Util.isEmpty(entList)) {
        	for (Long entId : entList) {
				EntUserEnterpriseEntity en = new EntUserEnterpriseEntity();
				en.setEnterpriseId(entId);
				en.setUserId(id);
				addList.add(en);
			}
        	baseDao.insertBatch(addList);
        }
    }
	
	@Override
	public RR taskUserList(TaskUserForm form, Pager pager) {
		List<EntTaskUserDTO> userList = adminUserManageDao.taskUserList(form, pager);   
		return RR.ok(userList); 
	}

}