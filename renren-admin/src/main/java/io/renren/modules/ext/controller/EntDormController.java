package io.renren.modules.ext.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.RR;
import io.renren.common.utils.Result;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.ext.dto.EntDormDTO;
import io.renren.modules.ext.form.dorm.DormListQueryForm;
import io.renren.modules.ext.service.EntDormService;


/**
 * 栋舍
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@RestController
@RequestMapping("ext/entdorm")
public class EntDormController {
    @Autowired
    private EntDormService entDormService;

    @GetMapping("page")
    @RequiresPermissions("ext:entdorm:page")
    public Result<PageData<EntDormDTO>> page(final DormListQueryForm form,final Pager pager){
        PageData<EntDormDTO> page = entDormService.pageData(form,pager);

        return new Result<PageData<EntDormDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @RequiresPermissions("ext:entdorm:info")
    public Result<EntDormDTO> get(@PathVariable("id") Long id){
        EntDormDTO data = entDormService.dormDetail(id);

        return new Result<EntDormDTO>().ok(data);
    }

    @PostMapping
    @LogOperation("保存")
    @RequiresPermissions("ext:entdorm:save")
    public Result save(@RequestBody EntDormDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        entDormService.save(dto);

        return new Result();
    }

    @PutMapping
    @LogOperation("修改")
    @RequiresPermissions("ext:entdorm:update")
    public Result update(@RequestBody EntDormDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        entDormService.update(dto);

        return new Result();
    }
    
    @RequestMapping(value = { "/enable" })
    @LogOperation("启用")
    public RR enable(@RequestBody final EntDormDTO dto) {
		return entDormService.enable(dto.getId());   
	}
    
    @RequestMapping(value = { "/disable" })
    @LogOperation("禁用")
    public RR disable(@RequestBody final EntDormDTO dto) {
		return entDormService.disable(dto.getId());   
	}

    @GetMapping("listEnable")
    public Result<PageData<EntDormDTO>> listEnable(@RequestParam Map<String, Object> params, final Pager pager){
        PageData<EntDormDTO> data = entDormService.findEnableList(params,pager);

        return new Result<PageData<EntDormDTO>>().ok(data);
    }

}