package io.renren.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tangzc.mpe.bind.metadata.annotation.BindField;
import com.tangzc.mpe.bind.metadata.annotation.JoinCondition;

import io.renren.common.entity.BaseEntity;
import io.renren.modules.sys.entity.SysUserEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 任务管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Getter
@Setter
@TableName("ent_revision_task")
public class EntRevisionTaskEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 任务提交id
	 */
	private Long commitId;
    /**
     * 企业id
     */
	private Long enterpriseId;
    /**
     * 企业名称
     */
	private String enterpriseName;
    /**
     * 场地id
     */
	private Long placeId;
    /**
     * 场地名称
     */
	private String placeName;
    /**
     * 栋舍类型
     */
	private Integer dormType;
    /**
     * 栋舍类型名称
     */
	private String dormTypeName;
    /**
     * 栋舍id
     */
	private Long dormId;
    /**
     * 栋舍名称
     */
	private String dormName;
    /**
     * 数量结果
     */
	private Integer revisionCount = 0;
    /**
     * 摄像头数量
     */
	private Integer cameraCount = 0;
    /**
     * 图片抓取状态
     */
	private Integer catchStatus = 0;
    /**
     * 图片上传状态
     */
	private Integer uploadStatus = 0;
	
    /**
     * 已分娩
     */
	private Integer delivered = 0;
    /**
     * 未分娩
     */
	private Integer undelivered = 0;
	
	// 栋舍任务负责人
	@TableField(exist = false)
	private Long userId;
	
	@BindField(entity = SysUserEntity.class,field = "username",conditions = { @JoinCondition(selfField = "creator",joinField = "id") })
	@TableField(exist = false) 
	private String creatorName;
	
	@TableField(exist = false)
	private Integer uploadCount;
	
	@TableField(exist = false)
	private Integer commitStatus ;
	
	// 是否分娩舍
	@BindField(entity = EntDormTypeEntity.class,field = "deliver",conditions = { @JoinCondition(selfField = "dormType",joinField = "id") })
	@TableField(exist = false)
	private Integer deliver;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dormId == null) ? 0 : dormId.hashCode());
		result = prime * result + ((enterpriseId == null) ? 0 : enterpriseId.hashCode());
		result = prime * result + ((getCreator() == null) ? 0 : getCreator().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntRevisionTaskEntity other = (EntRevisionTaskEntity) obj;
		if (dormId == null) {
			if (other.dormId != null)
				return false;
		} else if (!dormId.equals(other.dormId))
			return false;
		if (enterpriseId == null) {
			if (other.enterpriseId != null)
				return false;
		} else if (!enterpriseId.equals(other.enterpriseId))
			return false;
		if (getCreator() == null) { 
			if (other.getCreator() != null)
				return false;
		} else if (!getCreator().equals(other.getCreator()))
			return false;
		return true;
	}
	
	
}