package io.renren.modules.quartz.service;

import java.util.Date;
import java.util.List;

import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import io.renren.common.constant.Constant;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.framework.vendors.quartz.form.FixedTimeForm;
import io.renren.framework.vendors.quartz.form.RepeatJobForm;
import io.renren.framework.vendors.quartz.util.EventJobHelper;
import io.renren.modules.ext.entity.EntCameraEntity;
import io.renren.modules.ext.service.EntCameraService;
import io.renren.modules.quartz.job.ActiveCaptureImageJob;
import io.renren.modules.quartz.job.CaptureImageJob;
import io.renren.modules.quartz.job.EzvizCaptureConf;
import io.renren.modules.quartz.payload.ActiveCapturePayloadDto;
import io.renren.modules.quartz.payload.CapturePayloadDto;
import io.renren.modules.sys.service.SysParamsService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CaptureImageService {
	
	protected Logger logger  = LoggerFactory.getLogger(CaptureImageService.class) ; 
	
	private static final int MAX_RETRY = 5 ;
	
	private final EntCameraService entCameraService ;
	
	private final Scheduler scheduler;
	
	private final SysParamsService sysParamsService;
	
	public void quartzActiveCaptureJob() {
		EventJobHelper jh = new EventJobHelper(scheduler);
		String taskId = "active-001" ; 
		// 每次调度之前先删除
		jh.removeJob(taskId); 
		
		RepeatJobForm<ActiveCapturePayloadDto> form = new RepeatJobForm<>() ;  
        form.setTaskId(taskId); 
        
        EzvizCaptureConf conf = sysParamsService.getValueObject(Constant.EZVIZ_CAPTURE_CONFIG, EzvizCaptureConf.class); 
        String time = conf.getTime(); 
        Date todayExecTime = DateUtil.parseTimeToday(time) ; 
        
        Date startDate = null;
        Date now = DateUtil.date(); 
        
        // 如果已经过了当日执行时间，则下一日再执行
        if(now.after(todayExecTime)) { 
        	startDate = DateUtil.offsetDay(todayExecTime, 1);
        }else {
        	startDate = todayExecTime;
        }
        form.setStartDate(startDate); 
        
        /**
         *  每1日执行一次,测试时可修改为每5分钟执行一次
         */
        int interval = 1; 
        form.setInterval(interval);
        form.setDateUnit(DateUnit.DAY);
        
        ActiveCapturePayloadDto dto = new ActiveCapturePayloadDto() ;
        dto.setTaskId(taskId);
		form.setData(dto);
		
        jh.quartzRepeatJob(ActiveCaptureImageJob.class, form);
	}
	
	/**
	 * 1 查询全部可用的摄像头
	 * 2 为每个摄像头启动一个抓取图片任务
	 */
	public RR startCptureImgTask() {
		// 1 查询全部摄像头,先删除之前的任务
		List<EntCameraEntity> all = entCameraService.listAll();
		if(!Util.isEmpty(all)) {
			for(EntCameraEntity camera : all) {
				EventJobHelper jh = new EventJobHelper(scheduler);
				String taskId = camera.getId() + "" ;
				jh.removeJob(taskId);
			}
		}
		
		// 2 查询启用中的摄像头
		List<EntCameraEntity> listEnabled = entCameraService.listEnabled();
		if(!Util.isEmpty(listEnabled)) {
			for(EntCameraEntity camera : listEnabled) {
				int errCnt = 0 ;
				inner : while(errCnt < MAX_RETRY) {
					try {
						quartzCaptureJob(camera); 
						break inner;
					}catch(Exception e) {
						errCnt++ ;
					}
				}
			}
		}
		return RR.ok() ;	
	}
	
	private void quartzCaptureJob(EntCameraEntity camera) {
		EventJobHelper jh = new EventJobHelper(scheduler);
        
		String taskId = camera.getId() + "" ;
		// 每次调度之前先删除
		jh.removeJob(taskId); 
		
        FixedTimeForm<CapturePayloadDto> form = new FixedTimeForm<>() ;  
        form.setTaskId(taskId);
        
        Date now = DateUtil.date(); 
        DateTime startDate = DateUtil.offsetSecond(now, 5);
        form.setStartDate(startDate); 
        
        String videoSn = camera.getVideoSn(); 
        Integer channel = camera.getChannel(); 
        
        CapturePayloadDto dto = new CapturePayloadDto() ;
        dto.setTaskId(taskId);
		dto.setStartDate(startDate);
		dto.setVideoSn(videoSn); 
		dto.setChannel(channel); 
		
		form.setData(dto);
		
        jh.quartzFixedTime(CaptureImageJob.class, form); 
	}

}