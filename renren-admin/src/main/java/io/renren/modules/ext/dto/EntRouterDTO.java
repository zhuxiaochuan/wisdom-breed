package io.renren.modules.ext.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 路由器
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
public class EntRouterDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	// @ApiModelProperty(value = "")
	private Long id;

	// @ApiModelProperty(value = "企业id")
	private Long enterpriseId;

	// @ApiModelProperty(value = "场地id")
	private Long placeId;

	// @ApiModelProperty(value = "栋舍id")
	private Long dormId;

	// @ApiModelProperty(value = "厂家")
	private String producer;

	// @ApiModelProperty(value = "型号")
	private String model;

	// @ApiModelProperty(value = "内网IP")
	private String innerIp;

	// @ApiModelProperty(value = "外网IP")
	private String outterIp;

	// @ApiModelProperty(value = "SIM卡号")
	private String simNumber;

	// @ApiModelProperty(value = "SN号")
	private String snNumber;

	// @ApiModelProperty(value = "创建者")
	private Long creator;

	// @ApiModelProperty(value = "创建时间")
	private Date createDate;

	// @ApiModelProperty(value = "更新者")
	private Long updater;

	// @ApiModelProperty(value = "更新时间")
	private Date updateDate;

	private String enterpriseName;
	private String placeName;
	private String dormName;

	private String updaterName;

}