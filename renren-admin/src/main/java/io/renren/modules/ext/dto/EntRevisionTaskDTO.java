package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 任务管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@ApiModel(value = "任务管理")
public class EntRevisionTaskDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "id")
	private Long id;
	
	@ApiModelProperty(value = "任务提交id")
	private Long commitId;

	@ApiModelProperty(value = "企业id")
	private Long enterpriseId;

	@ApiModelProperty(value = "企业名称")
	private String enterpriseName;

	@ApiModelProperty(value = "场地id")
	private Long placeId;

	@ApiModelProperty(value = "场地名称")
	private String placeName;

	@ApiModelProperty(value = "栋舍类型")
	private Integer dormType;
	
	@ApiModelProperty(value = "是否分娩舍")
	private Integer deliver;

	@ApiModelProperty(value = "栋舍类型名称")
	private String dormTypeName;

	@ApiModelProperty(value = "栋舍id")
	private Long dormId;

	@ApiModelProperty(value = "栋舍名称")
	private String dormName;

	@ApiModelProperty(value = "数量结果")
	private Integer revisionCount;

	@ApiModelProperty(value = "摄像头数量")
	private Integer cameraCount;

	@ApiModelProperty(value = "图片上传状态")
	private Integer uploadStatus;

	@ApiModelProperty(value = "已分娩")
	private Integer delivered;

	@ApiModelProperty(value = "未分娩")
	private Integer undelivered;

	@ApiModelProperty(value = "创建者")
	private Long creator;
	
	@ApiModelProperty(value = "创建者name")
	private String creatorName;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;
	
	private Long postUser;
	private String postUserName;
	private Date postDate;
	
	@ApiModelProperty(value = "已上传数量")
	private Integer uploadCount;
	
	// 提交状态
	private Integer commitStatus ;
	private Date commitDate;

}