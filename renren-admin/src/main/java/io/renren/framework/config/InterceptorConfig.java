package io.renren.framework.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import cn.hutool.core.convert.Convert;
import io.renren.framework.interceptor.MD5SignInterceptor;
import io.renren.framework.interceptor.WxLoginInterceptor;
/**
 * 拦截器配置
 * <p>
 * 注意：security.oauth2.ignore.urls中配置的url无法获取到登录后的用户信息
 */
@Configuration(proxyBeanMethods = false)
public class InterceptorConfig implements WebMvcConfigurer{
	
	private static final String[] PATH_PATTERNS = new String[]{"/wx/**"} ;
	
	private static final String[] MD5_PATH_PATTERNS = new String[]{"/reportup/**"} ;
	
	/**
	 * 需要放行的地址。
	 */
	private String[] exclude_patterns ;
	
	@Resource
	private WisdomConfig wisdomConfig ;
	
	// 登录
	@Bean
	public WxLoginInterceptor loginInterceptor() { 
		return new WxLoginInterceptor() ;
	}
	
	// MD5签名
	@Bean
	public MD5SignInterceptor md5signInterceptor() { 
		return new MD5SignInterceptor() ;
	}
	
	/**
	 * 1. 加入的顺序就是拦截器执行的顺序，
	 * 2. 按顺序执行所有拦截器的preHandle
	 * 3. 所有的preHandle 执行完再执行全部postHandle 最后是postHandle
	 * addPathPatterns 用于添加拦截规则
	 * excludePathPatterns 用于排除拦截
	 */
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
		List<String> urls = new ArrayList<>() ; 
		urls.add("/wx/auth/login") ;
		exclude_patterns = Convert.toStrArray(urls) ; 
		
        // 多个拦截器组成一个拦截器链
        registry.addInterceptor(loginInterceptor()).addPathPatterns(PATH_PATTERNS).excludePathPatterns(exclude_patterns);
        if(wisdomConfig.getEnableSign()) {
        	registry.addInterceptor(md5signInterceptor()).addPathPatterns(MD5_PATH_PATTERNS);
        }
    }

}
