package io.renren.modules.ext.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 任务用户
 *
 */
@Data
public class EntTaskUserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	private Long userId;

	@ApiModelProperty(value = "用户名")
	private String username;

	@ApiModelProperty(value = "姓名")
	private String realName;

}