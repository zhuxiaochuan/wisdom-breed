package io.renren.modules.ext.form.revision;

import java.util.Date;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

@Data
public class EnterpriseRevisionQueryForm {
	
	// 关键字
	private String enterpriseName;
	
	/*创建时间*/
	private Date date ; 
	
	public Date getEndTime() {
		if(!Util.isEmpty(date)) {
			date = DateUtil.endOfDay(date); 
		}
		return date ;
	}
}
