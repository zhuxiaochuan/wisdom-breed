package io.renren.modules.ext.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.Result;
import io.renren.modules.ext.dto.PlaceRevisionDTO;
import io.renren.modules.ext.form.revision.PlaceRevisionQueryForm;
import io.renren.modules.ext.service.PlaceRevisionService;
import io.swagger.annotations.Api;


/**
 * 场地盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@RestController
@RequestMapping("ext/placerevision")
@Api(tags="场地盘点")
public class PlaceRevisionController {
    @Autowired
    private PlaceRevisionService placeRevisionService;

    @GetMapping("page")
    @RequiresPermissions("ext:placerevision:page")
    public Result<PageData<PlaceRevisionDTO>> page(final PlaceRevisionQueryForm form,Pager pager){
        PageData<PlaceRevisionDTO> page = placeRevisionService.pageData(form, pager); 

        return new Result<PageData<PlaceRevisionDTO>>().ok(page);
    }

}