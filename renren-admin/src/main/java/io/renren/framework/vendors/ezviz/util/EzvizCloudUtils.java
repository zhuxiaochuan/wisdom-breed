package io.renren.framework.vendors.ezviz.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import io.renren.common.constant.Constant;
import io.renren.common.utils.SpringContextUtils;
import io.renren.common.utils.Util;
import io.renren.framework.vendors.ezviz.config.EzvizCloudConfig;
import io.renren.framework.vendors.ezviz.dto.EzvizResult;
import io.renren.framework.vendors.ezviz.dto.EzvizToken;
import io.renren.modules.sys.service.SysParamsService;

public class EzvizCloudUtils implements Serializable {
	
    private static final long serialVersionUID = 1L;
    
    private static final String TOKEN_KEY = "Ezviz_accessToken";
    
    //token缓存默认过期时间 7天-10秒
    private static final long expireTime = 7*24*3600*1000 - 10*1000;
    private static TimedCache<String, String> accessTokenCache = CacheUtil.newTimedCache(expireTime);
    
    private static SysParamsService sysParamsService;

    static {
        sysParamsService = SpringContextUtils.getBean(SysParamsService.class);
    }
    
    private EzvizCloudUtils() {}
    
    public static EzvizCloudConfig getConfig() {
    	//获取云存储配置信息
    	EzvizCloudConfig config = sysParamsService.getValueObject(Constant.EZVIZ_CLOUD_CONFIG_KEY, EzvizCloudConfig.class);
    	return config ;
    }
    
    /**
     *  获取 accessToken
     */
    public static String getAccessToken() {
    	String token = accessTokenCache.get(TOKEN_KEY); 
    	if(Util.isEmpty(token)) {
    		EzvizCloudConfig config = getConfig(); 
    		
    		String url = "https://open.ys7.com/api/lapp/token/get" ;
    		Map<String, Object> params = new HashMap<>();
    		params.put("appKey", config.getAppKey());
    		params.put("appSecret", config.getAppSecret());
    		String json = HttpRequest.post(url).form(params).execute().body() ;
    		EzvizResult result = JSONUtil.toBean(json, EzvizResult.class);
    		if(Util.eq("200", result.getCode())) {  
    			Object obj = result.getData();
    			EzvizToken et = JSONUtil.toBean(obj.toString(), EzvizToken.class);
    			token = et.getAccessToken();
        		accessTokenCache.put(TOKEN_KEY,token); 
    		}else {
    			accessTokenCache.put(TOKEN_KEY,null); 
    			throw new IllegalStateException("萤石云access token获取失败");
    		}
    	}
    	return token ;
    }
    
    /**
     * 作废当前token
     */
    public static void invalidToken() {
    	accessTokenCache.put(TOKEN_KEY,null); 
    }

}