package io.renren.modules.ext.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tangzc.mpe.bind.metadata.annotation.BindField;
import com.tangzc.mpe.bind.metadata.annotation.JoinCondition;

import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 摄像头
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_camera")
public class EntCameraEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 企业id
     */
	private Long enterpriseId;
	
    /**
     * 场地id
     */
	private Long placeId;
    /**
     * 栋舍id
     */
	private Long dormId;
	/**
	 * 硬盘录像机id
	 */
	private Long ipcId;
    /**
     * 名称
     */
	private String cameraName;
    /**
     * SN号
     */
	private String snNumber;
    /**
     * IP地址
     */
	private String ip;
	/**
	 * 用户名
	 */
	private String cuserName;
	/**
	 * 密码
	 */
	private String password;
    /**
     * 通道号
     */
	private Integer channel;
    
    /**
     * 更新者
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;
	
	/**
	 * 上报时间
	 */
	private Date reportTime;
	
	/**
	 * 状态
	 */
	private Integer status;
	
	// 关联字段，非本实体的字段
	@BindField(entity = EnterpriseEntity.class,field = "name",conditions = { @JoinCondition(selfField = "enterpriseId",joinField = "id") })
	@TableField(exist = false)
	private String enterpriseName;
	
	@BindField(entity = EntPlaceEntity.class,field = "placeName",conditions = { @JoinCondition(selfField = "placeId",joinField = "id") })
	@TableField(exist = false)
	private String placeName;
	
	@BindField(entity = EntDormEntity.class,field = "dormName",conditions = { @JoinCondition(selfField = "dormId",joinField = "id") })
	@TableField(exist = false)
	private String dormName;
	
	// 录像机SN
	@BindField(entity = EntIpcEntity.class,field = "videoSn",conditions = { @JoinCondition(selfField = "ipcId",joinField = "id") })
	@TableField(exist = false)
	private String videoSn;
	
	// 网络接入类型
	@BindField(entity = EntIpcEntity.class,field = "accessType",conditions = { @JoinCondition(selfField = "ipcId",joinField = "id") })
	@TableField(exist = false)
	private Integer accessType;
}