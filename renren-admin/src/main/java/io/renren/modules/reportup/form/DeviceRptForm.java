package io.renren.modules.reportup.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class DeviceRptForm {

	@NotNull(message="设备类型不能为空")
	private Integer deviceType;
	
	@NotEmpty(message="工控机ID不能为空")
	private String deviceNo;
	
	private String ip;

}
