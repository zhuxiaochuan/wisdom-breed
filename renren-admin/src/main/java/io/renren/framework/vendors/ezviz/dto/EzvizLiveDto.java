package io.renren.framework.vendors.ezviz.dto;

import java.util.Date;

import lombok.Data;

@Data
public class EzvizLiveDto {
	
	private String id ;
	private String url ;
	private Date expireTime ;

}
