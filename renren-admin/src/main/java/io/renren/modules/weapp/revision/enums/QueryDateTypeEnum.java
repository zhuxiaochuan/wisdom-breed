package io.renren.modules.weapp.revision.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.renren.common.enums.IEnum;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum QueryDateTypeEnum implements IEnum {
	
	WEEK(1,"周"),MONTH(2,"月"),SEASON(3,"季"),HALF_YEAR(4,"半年"),YEAR(5,"年");

	@EnumValue
	private int key;

    private String value;
    
	QueryDateTypeEnum(Integer key, String value){
        this.key = key;
        this.value = value;
    }

    @Override
	public String key() {
		return String.valueOf(key);
	}

	@Override
	public String value() {
		return value;
	}

	public int intKey() {
		return key;
	}


}
