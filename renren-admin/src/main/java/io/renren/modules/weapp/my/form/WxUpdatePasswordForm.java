package io.renren.modules.weapp.my.form;

import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
public class WxUpdatePasswordForm {
	
	@NotEmpty(message="原密码不能为空")
	private String originPassword;
	
	@NotEmpty(message="新密码不能为空")
	private String newPassword;
	
	@NotEmpty(message="确认密码不能为空")
	private String confirmPassword;
	
}
