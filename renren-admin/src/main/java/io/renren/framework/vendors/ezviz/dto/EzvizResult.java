package io.renren.framework.vendors.ezviz.dto;

import lombok.Data;

@Data
public class EzvizResult {
	
	private String code ;
	
	private String msg ;
	
	private Object data ;

}
