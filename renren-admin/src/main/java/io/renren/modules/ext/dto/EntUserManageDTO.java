package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;


/**
 * 企业用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Data
public class EntUserManageDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	//@ApiModelProperty(value = "")
	private Long id;
	
	private Long oldId;
	
	private Long userId;

	//@ApiModelProperty(value = "用户名")
	private String username;

	//@ApiModelProperty(value = "姓名")
	private String realName;

	//@ApiModelProperty(value = "手机号")
	private String mobile;
	
	private Long enterpriseId;
	
	//@ApiModelProperty(value = "负责企业")
	private String entName;

	//@ApiModelProperty(value = "权限场地")
	private String dutyPlace;

	//@ApiModelProperty(value = "职位")
	private String position;

	//@ApiModelProperty(value = "状态")
	private Integer status;

	//@ApiModelProperty(value = "状态名称")
	private String statusName;

	//@ApiModelProperty(value = "更新时间")
	private Date createDate;

	//@ApiModelProperty(value = "操作人")
	private String creatorName;
	
	private Integer userType;
	private String job;
	
	private String clearPassword;
	
	private List<Long> entPlaceList = new ArrayList<>();


}