package io.renren.modules.ext.service;

import java.util.List;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.common.utils.RR;
import io.renren.modules.ext.dto.EntCameraDTO;
import io.renren.modules.ext.entity.EntCameraEntity;
import io.renren.modules.ext.form.camera.CameraListQueryForm;
import io.renren.modules.ext.form.camera.CameraPlayForm;

/**
 * 摄像头
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
public interface EntCameraService extends CrudService<EntCameraEntity, EntCameraDTO> {
	
	public PageData<EntCameraDTO> pageData(final CameraListQueryForm form,Pager pager);
	
	public EntCameraDTO detail(Long id) ;
	
	public RR enable(final Long id);
	
	public RR disable(final Long id);
	
	public RR playurl(final CameraPlayForm pf);
	
	/**
	 * 查询全部启用中的摄像头
	 */
	public List<EntCameraEntity> listEnabled() ;
	
	/**
	 * 查询全部
	 */
	public List<EntCameraEntity> listAll() ;
	
	public List<EntCameraEntity> cameraList(final Long dormId) ;

}