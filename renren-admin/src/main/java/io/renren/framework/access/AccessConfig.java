package io.renren.framework.access;

/**
 * web访问请求配置
 * @author 朱晓川
 *
 */
public class AccessConfig {

	/**存取请求参数中的URL请求地址的key*/
	public static final String URL_KEY = "url";

	/**签名秘钥*/
	public static final String terminal_secret = "wisdom-1qa2wsxzxcv#!";

	/**字符编码格式 */
	public static final String INPUT_CHARSET = "utf-8";

	/**签名方式*/
	public static final String sign_type = "MD5";

}
