package io.renren.modules.ext.form.place;

import java.util.Date;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

@Data
public class PlaceListQueryForm {
	
	// 企业
	private String enterpriseName;
		
	// 场地
	private String placeName;
	
	// 联系人
	private String contactPerson;
	
	// 场地类型
	private Long placeType;
	
	/*创建时间*/
	private Date beginTime ;
	private Date endTime ;
	
	
	public Date getBeginTime() {
		if(!Util.isEmpty(beginTime)) {
			beginTime = DateUtil.beginOfDay(beginTime);
		}
		return beginTime;
	}
	
	public Date getEndTime() {
		if(!Util.isEmpty(endTime)) {
			endTime = DateUtil.endOfDay(endTime); 
		}
		return endTime ;
	}
}
