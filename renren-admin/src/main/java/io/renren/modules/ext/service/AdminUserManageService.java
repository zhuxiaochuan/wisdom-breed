package io.renren.modules.ext.service;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.common.utils.RR;
import io.renren.modules.ext.dto.AdminUserManageDTO;
import io.renren.modules.ext.entity.EntUserEnterpriseEntity;
import io.renren.modules.ext.form.adminuser.AdminUserListQueryForm;
import io.renren.modules.ext.form.adminuser.TaskUserForm;

/**
 * 后台用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
public interface AdminUserManageService extends CrudService<EntUserEnterpriseEntity, AdminUserManageDTO>{
	
	public PageData<AdminUserManageDTO> pageData(final AdminUserListQueryForm form, Pager pager);

	public void delete(Long[] ids);

	public AdminUserManageDTO userDetail(Long id);

	public void save(AdminUserManageDTO dto);
	
	// 任务用户
	public RR taskUserList(TaskUserForm form, Pager pager);
	

}