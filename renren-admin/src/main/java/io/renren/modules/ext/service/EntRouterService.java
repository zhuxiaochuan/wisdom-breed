package io.renren.modules.ext.service;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.modules.ext.dto.EntPlaceTypeDTO;
import io.renren.modules.ext.dto.EntRouterDTO;
import io.renren.modules.ext.entity.EntRouterEntity;
import io.renren.modules.ext.form.placeType.PlaceTypeListQueryForm;
import io.renren.modules.ext.form.router.RouterListQueryForm;

import java.util.Map;

/**
 * 路由器
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
public interface EntRouterService extends CrudService<EntRouterEntity, EntRouterDTO> {
    /**
     * 路由器列表
     */
    public PageData<EntRouterDTO> pageData(final RouterListQueryForm form, Pager pager);
    /**
     * 查询启用中的
     */
    public PageData<EntRouterDTO> findEnableList(Map<String, Object> params, Pager pager);
}