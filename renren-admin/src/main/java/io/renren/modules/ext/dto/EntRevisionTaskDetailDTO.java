package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 任务详情
 *
 * @author zxc 
 * @since 1.0.0 2022-07-27
 */
@Data
@ApiModel(value = "任务详情")
public class EntRevisionTaskDetailDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Long id;

	@ApiModelProperty(value = "任务id")
	private Long taskId;

	@ApiModelProperty(value = "摄像头id")
	private Long cameraId;

	@ApiModelProperty(value = "摄像头SN")
	private String cameraSn;

	@ApiModelProperty(value = "盘点数量")
	private Integer revisionCount;
	
	@ApiModelProperty(value = "已分娩数量")
	private Integer delivered;

	@ApiModelProperty(value = "完成状态(是/否)")
	private Integer completeStatus;

	@ApiModelProperty(value = "任务完成时间")
	private Date completeTime;

	@ApiModelProperty(value = "创建者")
	private Long creator;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;


}