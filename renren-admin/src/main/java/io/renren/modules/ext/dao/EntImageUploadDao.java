package io.renren.modules.ext.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.renren.common.dao.BaseDao;
import io.renren.modules.ext.dto.EntImageUploadDTO;
import io.renren.modules.ext.entity.EntImageUploadEntity;

/**
 * 图片上传表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-23
 */
@Mapper
public interface EntImageUploadDao extends BaseDao<EntImageUploadEntity> {
	
	List<EntImageUploadDTO> listImageUploadDetail(@Param("taskId") final Long taskId) ;
	
}