package io.renren.modules.ext.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 栋舍
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Data
public class EntDormExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "企业id")
    private Long enterpriseId;
    @Excel(name = "场地id")
    private Long placeId;
    @Excel(name = "场地类型")
    private Long placeType;
    @Excel(name = "栋舍名称")
    private String dormName;
    @Excel(name = "关键字")
    private String keyword;
    @Excel(name = "栋舍尺寸")
    private String dormSize;
    @Excel(name = "栏位行数")
    private Integer fieldRow;
    @Excel(name = "栏位列数")
    private Integer fieldColumn;
    @Excel(name = "栏位尺寸")
    private String fieldSize;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;

}