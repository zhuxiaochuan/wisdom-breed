package io.renren.modules.ext.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 路由器
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
public class EntRouterExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "企业id")
    private Long enterpriseId;
    @Excel(name = "场地id")
    private Long placeId;
    @Excel(name = "栋舍id")
    private Long dormId;
    @Excel(name = "厂家")
    private String producer;
    @Excel(name = "型号")
    private String model;
    @Excel(name = "内网IP")
    private String innerIp;
    @Excel(name = "外网IP")
    private String outterIp;
    @Excel(name = "SIM卡号")
    private String simNumber;
    @Excel(name = "SN号")
    private String snNumber;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;

}