package io.renren.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 企业
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("enterprise")
public class EnterpriseEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 企业名称
     */
	private String name;
    /**
     * 企业简称
     */
	private String shortName;
    /**
     * 地区代码
     */
	private String areaCode;
    /**
     * 详细地址
     */
	private String address;
    /**
     * 联系人
     */
	private String contactPerson;
    /**
     * 联系电话
     */
	private String contactNumber;
    /**
     * 状态
     */
	private Integer status;
    /**
     * 更新者
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;
}