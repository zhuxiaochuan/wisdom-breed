package io.renren.modules.ext.dao;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.EntPlaceTypeDTO;
import io.renren.modules.ext.dto.EntRouterDTO;
import io.renren.modules.ext.entity.EntRouterEntity;
import io.renren.modules.ext.form.placeType.PlaceTypeListQueryForm;
import io.renren.modules.ext.form.router.RouterListQueryForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 路由器
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Mapper
public interface EntRouterDao extends BaseDao<EntRouterEntity> {
    List<EntRouterDTO> listData(@Param("form") final RouterListQueryForm form, @Param("pager") Pager pager);

    public int listDataCount(@Param("form") final RouterListQueryForm form);
}