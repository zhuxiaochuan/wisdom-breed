package io.renren.modules.ext.service;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.common.utils.RR;
import io.renren.modules.ext.dto.EntUserManageDTO;
import io.renren.modules.ext.entity.EntUserPlaceEntity;
import io.renren.modules.ext.form.entuser.EntUserListQueryForm;

/**
 * 企业用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
public interface EntUserManageService extends CrudService<EntUserPlaceEntity, EntUserManageDTO> {
	
	public PageData<EntUserManageDTO> pageData(EntUserListQueryForm form, Pager pager);

	public EntUserManageDTO userDetail(Long userId,Long enterpriseId);
	
	public void save(EntUserManageDTO dto);

	public RR enable(Long userId, Long enterpriseId);

	public RR disable(Long userId, Long enterpriseId);
	
	public RR delete(Long userId, Long enterpriseId);
	
}