package io.renren.modules.ext.dao;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.dto.EntPlaceTypeDTO;
import io.renren.modules.ext.entity.EntPlaceTypeEntity;
import io.renren.modules.ext.form.place.PlaceListQueryForm;
import io.renren.modules.ext.form.placeType.PlaceTypeListQueryForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 场地类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Mapper
public interface EntPlaceTypeDao extends BaseDao<EntPlaceTypeEntity> {
    List<EntPlaceTypeDTO> listData(@Param("form") final PlaceTypeListQueryForm form, @Param("pager") Pager pager);

    public int listDataCount(@Param("form") final PlaceTypeListQueryForm form);
}