package io.renren.modules.ext.enums;

import io.renren.common.enums.IEnum;

public enum YesOrNoEnum implements IEnum{
	NO(0,"否"),
    YES(1,"是");

	YesOrNoEnum(Integer key, String value){
        this.key = key;
        this.value = value;
    }
    private String value;
    private Integer key;

    public String getValue() {
        return value;
    }

    public Integer getKey() {
        return key;
    }

    @Override
	public String key() {
		return String.valueOf(key);
	}

	@Override
	public String value() {
		return value;
	}

	public int intKey() {
		return key;
	}
}
