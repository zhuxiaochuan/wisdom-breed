package io.renren.modules.ext.service.impl;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import cn.hutool.core.date.DateUtil;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntImageUploadDao;
import io.renren.modules.ext.dto.EntImageUploadDTO;
import io.renren.modules.ext.entity.EntImageUploadEntity;
import io.renren.modules.ext.enums.YesOrNoEnum;
import io.renren.modules.ext.service.EntImageUploadService;

/**
 * 图片上传表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-23
 */
@Service
public class EntImageUploadServiceImpl extends CrudServiceImpl<EntImageUploadDao, EntImageUploadEntity, EntImageUploadDTO> implements EntImageUploadService {

    @Override
    public QueryWrapper<EntImageUploadEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<EntImageUploadEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

	@Override
	public Long dormCatchCompleteCnt(Long dormId,Date date) {
		if(Util.isEmpty(dormId)) {
			return 0l ;
		}
		Long cnt = baseDao.selectCount(
				Wrappers.lambdaQuery(EntImageUploadEntity.class)
				.eq(EntImageUploadEntity::getDormId, dormId)
				.eq(EntImageUploadEntity::getCatchStatus, YesOrNoEnum.YES.intKey())
				.between(EntImageUploadEntity::getCreateDate, DateUtil.beginOfDay(date), DateUtil.endOfDay(date))
				) ;
		return cnt ;
	}

	@Override
	public Long dormUploadCompleteCnt(Long dormId,Date date) {
		if(Util.isEmpty(dormId)) {
			return 0l ;
		}
		Long cnt = baseDao.selectCount(Wrappers.lambdaQuery(EntImageUploadEntity.class)
				.eq(EntImageUploadEntity::getDormId, dormId)
				.eq(EntImageUploadEntity::getUploadStatus, YesOrNoEnum.YES.intKey())
				.between(EntImageUploadEntity::getCreateDate, DateUtil.beginOfDay(date), DateUtil.endOfDay(date))
				) ; 
		return cnt ;
	}


}