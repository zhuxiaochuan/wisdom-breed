package io.renren.modules.ext.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.renren.common.enums.IEnum;

/**
 * 任务提交状态
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TaskCommitStatusEnum implements IEnum {
	
	UN_COMMIT(1,"未提交"),COMMITED(2,"已提交"),POSTED(3,"已发布");

	@EnumValue
	private int key;

    private String value;
    
	TaskCommitStatusEnum(Integer key, String value){
        this.key = key;
        this.value = value;
    }

    @Override
	public String key() {
		return String.valueOf(key);
	}

	@Override
	public String value() {
		return value;
	}

	public int intKey() {
		return key;
	}


}
