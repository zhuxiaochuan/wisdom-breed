package io.renren.modules.ext.form.revision;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class SubmitTaskDataForm {
	
	// 任务id
	@NotNull(message="请选择任务")
	private Long taskId;
	
	// 摄像头id
	@NotNull(message="请选择摄像头")
	private Long cameraId;
	
	// 盘点数量
	@NotNull(message="请填写盘点数量")
	private Integer revisionCount ;
	
	// 已分娩
	private Integer deliveredCount ;
	
}
