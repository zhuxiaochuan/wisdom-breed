package io.renren.modules.ext.service;

import java.util.List;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.common.utils.RR;
import io.renren.modules.ext.dto.EntRevisionTaskDTO;
import io.renren.modules.ext.entity.EntRevisionTaskEntity;
import io.renren.modules.ext.form.revision.EnterpriseRevisionTaskQueryForm;

/**
 * 今日任务
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
public interface TodayTaskService extends CrudService<EntRevisionTaskEntity, EntRevisionTaskDTO> {
	
	public PageData<EntRevisionTaskDTO> todayTask(final EnterpriseRevisionTaskQueryForm form,Pager pager);
	
	/**用户应该执行的任务*/
	public List<EntRevisionTaskEntity> todayTaskForUser(Long userId);
	
	// 提交今日任务
	public RR commit() ;

}