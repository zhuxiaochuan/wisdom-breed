package io.renren.modules.ext.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 今日任务
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Data
public class TodayTaskExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "企业id")
    private Long enterpriseId;
    @Excel(name = "企业名称")
    private String enterpriseName;
    @Excel(name = "场地id")
    private Long placeId;
    @Excel(name = "场地名称")
    private String placeName;
    @Excel(name = "栋舍类型")
    private Integer dormType;
    @Excel(name = "栋舍类型名称")
    private String dormTypeName;
    @Excel(name = "栋舍id")
    private Long dormId;
    @Excel(name = "栋舍名称")
    private String dormName;
    @Excel(name = "盘点数量")
    private Integer revisionCount;
    @Excel(name = "摄像头数量")
    private Integer cameraCount;
    @Excel(name = "图片抓取状态")
    private Integer catchStatus;
    @Excel(name = "图片上传状态")
    private Integer uploadStatus;
    @Excel(name = "数量结果")
    private Integer total;
    @Excel(name = "已分娩")
    private Integer delivered;
    @Excel(name = "未分娩")
    private Integer undelivered;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}