package io.renren.modules.ext.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tangzc.mpe.bind.Binder;

import cn.hutool.core.lang.Validator;
import io.renren.common.enums.EnableEnum;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.EnableService;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ConvertUtils;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.framework.config.WisdomConfig;
import io.renren.framework.vendors.ezviz.dto.EzvizLiveDto;
import io.renren.framework.vendors.ezviz.form.EzvizLiveForm;
import io.renren.framework.vendors.ezviz.service.EzvizCloudService;
import io.renren.modules.ext.dao.EntCameraDao;
import io.renren.modules.ext.dao.EntIpcDao;
import io.renren.modules.ext.dto.EntCameraDTO;
import io.renren.modules.ext.entity.EntCameraEntity;
import io.renren.modules.ext.entity.EntIpcEntity;
import io.renren.modules.ext.form.camera.CameraListQueryForm;
import io.renren.modules.ext.form.camera.CameraPlayForm;
import io.renren.modules.ext.service.EntCameraService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 摄像头
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class EntCameraServiceImpl extends CrudServiceImpl<EntCameraDao, EntCameraEntity, EntCameraDTO> implements EntCameraService {
	
	private final EntCameraDao entCameraDao ;
	
	private final EntIpcDao entIpcDao ;
	
	private final WisdomConfig wisdomConfig ;
	
	private final EnableService<EntCameraEntity> enableService;
	
	private final EzvizCloudService ezvizCloudService;

    @Override
    public QueryWrapper<EntCameraEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<EntCameraEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

	@Override
	public PageData<EntCameraDTO> pageData(CameraListQueryForm form, Pager pager) {
		form.setOnlineThreshold(wisdomConfig.getOnlineThreshold()); 
		
		List<EntCameraDTO> list = entCameraDao.listData(form, pager) ;   
		
		// 萤石云只挂载了硬盘录像机，摄像头未添加到萤石云，因此不进行在线状态查询
		/*
		if(!Util.isEmpty(list)) {
			for(EntCameraDTO camera : list) {
				String deviceSerial = camera.getSnNumber();
				if(!Util.isEmpty(deviceSerial)) {
					try {
						EzvizDeviceInfoDto deviceInfo = ezvizCloudService.deviceInfo(deviceSerial) ; 
						camera.setOnlineStatus(deviceInfo.getStatus());
					}catch(Exception e) {
						log.error("萤石云设备信息查询失败,deviceSerial:" + deviceSerial); 
						continue ;
					}
				}
			}
		}*/
		
		int total = entCameraDao.listDataCount(form);  
		return getPageData(list, total, EntCameraDTO.class); 
	}

	@Override
	public EntCameraDTO detail(Long id) {
		CameraListQueryForm form = new CameraListQueryForm() ;
		form.setId(id); 
		
		PageData<EntCameraDTO> pageData = pageData(form, null) ; 
		if(!Util.isEmpty(pageData)) {
			List<EntCameraDTO> list = pageData.getList(); 
			if(!Util.isEmpty(list) && list.size() > 1) {
				throw new IllegalStateException("摄像头数据不唯一，id:" + id) ;
			}else {
				return list.get(0);
			}
		}
		return null;
	}
	
	@Override
    public void save(EntCameraDTO dto) {
		Long ipcId = dto.getIpcId(); 
		EntIpcEntity ipc = entIpcDao.selectOne(Wrappers.lambdaQuery(EntIpcEntity.class).eq(EntIpcEntity::getId, ipcId));  
		if(Util.isEmpty(ipc)) {
			throw new IllegalArgumentException("录像机不存在！") ;
		}
		
		Integer channel = dto.getChannel(); 
		if(Util.isEmpty(channel)) {
			throw new IllegalArgumentException("请填写通道号！") ;
		}
		
		// 工控机ID & 摄像头ip 不能重复
		String ip = dto.getIp(); 
		if(!Util.isEmpty(ip)) {
			ip = ip.trim() ;
			boolean ipv4 = Validator.isIpv4(ip) ; 
			if(!ipv4) {
				throw new IllegalArgumentException("ip地址格式不正确，仅支持ipv4地址") ;
			}
		}
		
		LambdaQueryWrapper<EntCameraEntity> wrapper = Wrappers.lambdaQuery(EntCameraEntity.class).eq(EntCameraEntity::getIpcId, ipcId).eq(EntCameraEntity::getChannel, channel); 
		List<EntCameraEntity> list = entCameraDao.selectList(wrapper) ; 
		if(!Util.isEmpty(list)) {
			throw new IllegalArgumentException("该录像机已存在相同通道的摄像头！") ;
		}
		
		EntCameraEntity entity = ConvertUtils.sourceToTarget(dto, currentModelClass()); 
		entity.setIpcId(ipc.getId()); 
        insert(entity);

        //copy主键值到dto
        BeanUtils.copyProperties(entity, dto);
    }
	
	@Override
    public void update(EntCameraDTO dto) { 
		Long currentId = dto.getId();  
		if(Util.isEmpty(currentId)) {
			throw new IllegalArgumentException("请选择摄像头！") ;
		}
		
		Long ipcId = dto.getIpcId(); 
		EntIpcEntity ipc = entIpcDao.selectOne(Wrappers.lambdaQuery(EntIpcEntity.class).eq(EntIpcEntity::getId, ipcId));  
		if(Util.isEmpty(ipc)) {
			throw new IllegalArgumentException("录像机不存在！") ;
		}
		
		Integer channel = dto.getChannel(); 
		if(Util.isEmpty(channel)) {
			throw new IllegalArgumentException("请填写通道号！") ;
		}
		
		// 工控机ID & 摄像头ip 不能重复
		String ip = dto.getIp(); 
		if(!Util.isEmpty(ip)) {
			ip = ip.trim() ;
			boolean ipv4 = Validator.isIpv4(ip) ; 
			if(!ipv4) {
				throw new IllegalArgumentException("ip地址格式不正确，仅支持ipv4地址") ;
			}
		}
		
		LambdaQueryWrapper<EntCameraEntity> wrapper = Wrappers.lambdaQuery(EntCameraEntity.class).eq(EntCameraEntity::getIpcId, ipcId).eq(EntCameraEntity::getChannel, channel); 
		List<EntCameraEntity> list = entCameraDao.selectList(wrapper) ; 
		if(!Util.isEmpty(list)) {
			if(list.size() > 1) {
				throw new IllegalArgumentException("该录像机已存在相同通道的摄像头！") ;
			}else {
				EntCameraEntity exist = list.get(0) ; 
				Long existId = exist.getId(); 
				if(!Util.eq(currentId, existId)) { 
					throw new IllegalArgumentException("该录像机已存在相同通道的摄像头！") ;
				}
			}
		}
		
		EntCameraEntity entity = ConvertUtils.sourceToTarget(dto, currentModelClass());
		entity.setIpcId(ipc.getId()); 
        updateById(entity);
    }
	
	@Transactional
	public RR enable(final Long id) {
		RR r = enableService.enable(id, baseDao);  
		return r ;
	}
	
	@Transactional
	public RR disable(final Long id) {
		RR r = enableService.disable(id, baseDao);  
		return r ;
	}

	@Override
	public List<EntCameraEntity> listEnabled() {
		LambdaQueryWrapper<EntCameraEntity> qw = Wrappers.lambdaQuery(EntCameraEntity.class).eq(EntCameraEntity::getStatus, EnableEnum.ENABLE.intKey()); 
		List<EntCameraEntity> list = entCameraDao.selectList(qw) ; 
		
		// 查询填充关联字段
        Binder.bindOn(list,EntCameraEntity::getVideoSn);
		
		return list;
	}

	@Override
	public RR playurl(CameraPlayForm pf) {
		Long id = pf.getId(); 
		int quality = pf.getQuality(); 
		
		EntCameraEntity camera = entCameraDao.selectById(id) ; 
		// 查询填充关联字段
        Binder.bindOn(camera,EntCameraEntity::getVideoSn);
        String videoSn = camera.getVideoSn(); 
        Integer channel = camera.getChannel(); 
        
        EzvizLiveForm form = new EzvizLiveForm() ;
        form.setDeviceSerial(videoSn); 
        form.setChannelNo(String.valueOf(channel)); 
        form.setQuality(quality);
        EzvizLiveDto ezDto = ezvizCloudService.deviceLiveUrl(form) ; 
        if(!Util.isEmpty(ezDto)) {
        	return RR.ok(ezDto.getUrl()) ; 
        }else {
        	return RR.failed("直播地址查询失败"); 
        }
	}

	@Override
	public List<EntCameraEntity> cameraList(Long dormId) {
		LambdaQueryWrapper<EntCameraEntity> wrapper = Wrappers.lambdaQuery(EntCameraEntity.class).eq(EntCameraEntity::getDormId, dormId).eq(EntCameraEntity::getStatus,EnableEnum.ENABLE.intKey()) ; 
		List<EntCameraEntity> list = entCameraDao.selectList(wrapper) ;
		Binder.bindOn(list,EntCameraEntity::getVideoSn,EntCameraEntity::getAccessType);
		return list;
	}

	@Override
	public List<EntCameraEntity> listAll() {
		LambdaQueryWrapper<EntCameraEntity> qw = Wrappers.lambdaQuery(EntCameraEntity.class); 
		List<EntCameraEntity> list = entCameraDao.selectList(qw) ; 
		
		// 查询填充关联字段
        Binder.bindOn(list,EntCameraEntity::getVideoSn);
		
		return list;
	}

}