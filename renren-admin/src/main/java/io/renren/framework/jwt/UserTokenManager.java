package io.renren.framework.jwt;

import javax.servlet.http.HttpServletRequest;

/**
 * 维护用户token
 */
public class UserTokenManager {
	
	/**
	 * 小程序端token的key
	 */
	public static final String MINAPP_TOKEN_KEY = "X-Wds-Token";

	public static String generateToken4Minapp(JwtMinappUserDto user) {
		return JwtHelper.createMinappToken(user);
	}

	public static JwtMinappUserDto getUser4Minapp(String token) throws Exception {
		return JwtHelper.verifyTokenUserMinapp(token);
	}

	/**
	 * 获取小程序登录用户
	 * <p>
	 * 请在小程序token登录拦截器之后使用
	 */
	public static JwtMinappUserDto getUser4Minapp(HttpServletRequest request) {
		String token = request.getHeader(MINAPP_TOKEN_KEY);
		try {
			return getUser4Minapp(token);
		} catch (Exception e) {
			//e.printStackTrace();   
		}
		return null;
	}
}
