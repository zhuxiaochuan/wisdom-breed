package io.renren.modules.weapp.revision.controller;


import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.RR;
import io.renren.modules.weapp.revision.form.WxRevisionQueryForm;
import io.renren.modules.weapp.revision.service.WxRevisionService;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/revision")
public class WxRevisionController {
	
    private final WxRevisionService wxRevisionService; 

    @GetMapping("data")
    public RR data(final WxRevisionQueryForm form){
    	Map<String, Object> data = wxRevisionService.data(form) ;
        return RR.ok(data);
    }
    
    @GetMapping("deliverData")
    public RR deliverData(final WxRevisionQueryForm form){
    	Map<String, Object> data = wxRevisionService.deliverData(form) ;
        return RR.ok(data);
    }
    
}