package io.renren.modules.ext.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 企业
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
public class EnterpriseExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "企业名称")
    private String name;
    @Excel(name = "企业简称")
    private String shortName;
    @Excel(name = "地区代码")
    private String areaCode;
    @Excel(name = "详细地址")
    private String address;
    @Excel(name = "联系人")
    private String contactPerson;
    @Excel(name = "联系电话")
    private String contactNumber;
    @Excel(name = "状态")
    private Integer status;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;

}