package io.renren.modules.ext.controller;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import io.renren.common.utils.DateUtils;
import io.renren.framework.vendors.ezviz.dto.EzvizDeviceInfoDto;
import io.renren.modules.ext.dao.EntIpcDao;
import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.form.ipc.DeviceQueryForm;
import io.renren.modules.ext.form.revision.SubmitTaskDataForm;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.ext.dto.EntIpcDTO;
import io.renren.modules.ext.excel.EntIpcExcel;
import io.renren.modules.ext.form.ipc.IPCListQueryForm;
import io.renren.modules.ext.service.EntIpcService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 工控机
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@RestController
@RequestMapping("ext/entipc")
@Api(tags="工控机")
public class EntIpcController {
    @Autowired
    private EntIpcService entIpcService;
    @Autowired
    private EntIpcDao entIpcDao ;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("ext:entipc:page")
    public Result<PageData<EntIpcDTO>> page(final IPCListQueryForm form,final Pager pager){
        PageData<EntIpcDTO> page = entIpcService.pageData(form, pager);

        return new Result<PageData<EntIpcDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("ext:entipc:info")
    public Result<EntIpcDTO> get(@PathVariable("id") Long id){
        EntIpcDTO data = entIpcService.get(id);

        return new Result<EntIpcDTO>().ok(data);
    }

    @PostMapping("getDeviceInfo")
    @ApiOperation("获取设备信息")
    @RequiresPermissions("ext:entipc:info")
    public Result<List<EzvizDeviceInfoDto>> getDeviceInfo(@RequestBody DeviceQueryForm deviceQueryForm){
        List<String> deviceSerialList = deviceQueryForm.getDeviceSerialList();
        List<EzvizDeviceInfoDto> data = entIpcService.getDeviceInfo(deviceSerialList);

        return new Result<List<EzvizDeviceInfoDto>>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("ext:entipc:save")
    public Result save(@RequestBody EntIpcDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        entIpcService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("ext:entipc:update")
    public Result update(@RequestBody EntIpcDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        entIpcService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("ext:entipc:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        entIpcService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("ext:entipc:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<EntIpcDTO> list = entIpcService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, EntIpcExcel.class);
    }

    @GetMapping("getIdNo")
    public Result<String> getIdNo(){
        IPCListQueryForm form = new IPCListQueryForm();
        int total = entIpcDao.listDataCount(form);
        String date_string = DateUtils.format_String(new Date());
        date_string += date_string + pad(2, Long.valueOf(total));
        String[] split = date_string.split("");
        BigDecimal bigDecimal = new BigDecimal(BigInteger.ZERO);
        for (String s :split){
            bigDecimal.add(new BigDecimal(s));
        }
        String idNo=date_string + bigDecimal.toString();
        return new Result<String>().ok(idNo);
    }

    public static String pad (int length, long num){
        return String.format("%0".concat(String.valueOf(length)).concat("d"),num);
    }

    public  static  void main (String[] args){
        String[] split = "20220026".split("");
        for (String s :split){}
        //System.out.println(s);
        System.out.println(pad(2,99));
    }
}