package io.renren.modules.ext.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 栋舍
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_dorm")
public class EntDormEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 企业id
     */
	private Long enterpriseId;
    /**
     * 场地id
     */
	private Long placeId;
	/**
     * 栋舍类型
     */
	private Long dormType;
    /**
     * 栋舍名称
     */
	private String dormName;
    /**
     * 关键字
     */
	private String keyword;
    /**
     * 栋舍尺寸
     */
	private String dormSize;
    /**
     * 栏位行数
     */
	private Integer fieldRow;
    /**
     * 栏位列数
     */
	private Integer fieldColumn;
    /**
     * 栏位尺寸
     */
	private String fieldSize;

	/**
	 * 状态
	 */
	private Integer status;
    /**
     * 更新者
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;
}