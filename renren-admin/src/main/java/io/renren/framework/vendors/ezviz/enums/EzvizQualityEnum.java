package io.renren.framework.vendors.ezviz.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.renren.common.enums.IEnum;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum EzvizQualityEnum implements IEnum {
	
	HD(1,"高清"),FLUENT(2,"流畅");

	@EnumValue
	private int key;

    private String value;
    
	EzvizQualityEnum(Integer key, String value){
        this.key = key;
        this.value = value;
    }

    @Override
	public String key() {
		return String.valueOf(key);
	}

	@Override
	public String value() {
		return value;
	}

	public int intKey() {
		return key;
	}


}
