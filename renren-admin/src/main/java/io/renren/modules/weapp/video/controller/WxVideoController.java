package io.renren.modules.weapp.video.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.RR;
import io.renren.modules.ext.form.camera.CameraPlayForm;
import io.renren.modules.ext.service.EntCameraService;
import io.renren.modules.weapp.video.form.WxCameraForm;
import io.renren.modules.weapp.video.form.WxDormForm;
import io.renren.modules.weapp.video.service.WxVideoService;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/video")
public class WxVideoController {
	
	@Autowired
    private EntCameraService entCameraService;  
	
	@Autowired
	private WxVideoService wxVideoService;  
	
	/**
	 * 根据场地查询栋舍列表
	 */
    @RequestMapping("dormList")
    public RR dormList(@Validated final WxDormForm form){
        return wxVideoService.dormList(form); 
    }
    
    /**
	 * 根据栋舍id查询摄像头
	 */
    @RequestMapping("cameraList")
    public RR cameraList(@Validated final WxCameraForm form){
        return wxVideoService.cameraList(form); 
    }
    
    /**
     * 查询摄像头直播地址
     */
    @RequestMapping("/playurl/{id}")
    public RR playurl(@PathVariable("id") Long id){
    	
    	CameraPlayForm pf = new CameraPlayForm() ;
    	pf.setId(id); 
    	RR r = entCameraService.playurl(pf) ; 
        return r;
    }
    
}