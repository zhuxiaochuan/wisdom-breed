package io.renren.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 任务提交表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-27
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_revision_task_commit")
public class EntRevisionTaskCommitEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 企业id
     */
	private Long enterpriseId;
    /**
     * 负责人id
     */
	private Long userId;
    /**
     * 提交状态
     */
	private Integer commitStatus;
	private Date commitDate;
    /**
     * 发布者
     */
	private Long postUser;
    /**
     * 发布时间
     */
	private Date postDate;
}