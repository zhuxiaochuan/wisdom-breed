package io.renren.modules.quartz.job;

import java.io.Serializable;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import io.renren.common.constant.Constant;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.framework.vendors.ezviz.dto.EzvizDeviceInfoDto;
import io.renren.framework.vendors.ezviz.service.EzvizCloudService;
import io.renren.framework.vendors.quartz.util.EventJobHelper;
import io.renren.modules.ext.enums.ImageReportTypeEnum;
import io.renren.modules.quartz.payload.CapturePayloadDto;
import io.renren.modules.reportup.form.ImageRptForm;
import io.renren.modules.reportup.service.DataReportService;
import io.renren.modules.sys.service.SysParamsService;
import lombok.extern.slf4j.Slf4j;

/**
 * 指定时间点的任务
 * 
 * @author   朱晓川
 */
@Component
@Slf4j
public class CaptureImageJob implements Job ,Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 注入业务service
	 */
	@Autowired
	private SysParamsService sysParamsService;
	
	@Autowired
	private EzvizCloudService ezvizCloudService;
	
	@Autowired
	private DataReportService dataReportService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		log.info("=======================执行抓取图片任务,时间:" + DateUtil.date()); 
		EzvizCaptureConf conf = null ;
		try {
			CapturePayloadDto payload =  (CapturePayloadDto) dataMap.get(EventJobHelper.PAYLOAD_KEY);
			if(!Util.isEmpty(payload)) {
				Date startDate = payload.getStartDate() ; 
				conf = sysParamsService.getValueObject(Constant.EZVIZ_CAPTURE_CONFIG, EzvizCaptureConf.class); 
				int limit = conf.getLimit() ; 
				
				Date now = DateUtil.date() ;
				long delta = DateUtil.between(startDate, now, DateUnit.MINUTE); 
				
				String taskId = payload.getTaskId() ; 
				// 序列号
				String videoSn = payload.getVideoSn(); 
				// 通道号
				Integer channel = payload.getChannel() ; 
				
				if(delta >= limit) {
					// 超过时限，不再执行
					log.warn("=======================任务id:"+taskId+",录像机SN[" +videoSn + "]通道[" +channel+"]，超过时限，不再执行.");
				}else {
					String channelNo = String.valueOf(channel);
					
					EzvizDeviceInfoDto deviceInfo = null;
					try {
						deviceInfo = ezvizCloudService.deviceInfo(videoSn) ; 
					}catch(Exception e) {
						e.printStackTrace();
						log.info("=======================任务id:"+taskId + ",录像机SN[" +videoSn + "]通道[" +channel+"]，萤石云设备信息获取失败.");
					}
					
					if(!Util.isEmpty(deviceInfo)) {
						String imageUrl = ezvizCloudService.deviceCapture(videoSn,channelNo); 
						log.info("=======================任务id:"+taskId + ",录像机SN[" +videoSn + "]通道[" +channel+"]，图片地址:" + imageUrl);
						
						ImageRptForm rptForm = new ImageRptForm() ;
						rptForm.setType(ImageReportTypeEnum.UPLOAD.intKey());
						rptForm.setVideoSn(videoSn); 
						rptForm.setChannel(channel); 
						rptForm.setImageUrl(imageUrl); 
						RR upResult = dataReportService.reportImage(rptForm) ; 
						if(Util.eq(RR.SUCCESS, upResult.getCode())) { 
							log.info("=======================任务id:"+taskId+ ",录像机SN[" +videoSn + "]通道[" +channel+"]，执行成功.");
						}else {
							log.error("=======================任务id:"+taskId+",录像机SN[" +videoSn + "]通道[" +channel+"]，执行失败，message:" + upResult.getMsg()); 
						}
					}
				}
			}
        } catch (Exception e) {
        	e.printStackTrace();
        	if(null == conf) {
        		conf = new EzvizCaptureConf() ;
        	}
        	int interval = conf.getInterval() ; 
        	
        	// sleep for interval mins
        	try {
				Thread.sleep(interval * 60000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			} 
        	
        	// fire it again
        	JobExecutionException jobEx = new JobExecutionException(e); 
        	jobEx.setRefireImmediately(true);
        	throw jobEx;
        }
	}
}
