package io.renren.modules.ext.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.EntUserManageDTO;
import io.renren.modules.ext.dto.UserPlaceDTO;
import io.renren.modules.ext.entity.EntUserPlaceEntity;
import io.renren.modules.ext.form.entuser.EntUserListQueryForm;

/**
 * 企业用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Mapper
public interface EntUserManageDao extends BaseDao<EntUserPlaceEntity> {

	List<EntUserManageDTO> listData(@Param ("form") final EntUserListQueryForm form, @Param ("pager") Pager pager);

	int listDataCount(@Param ("form") final EntUserListQueryForm form);
	
	// 用户场地
	public List<UserPlaceDTO> userPlaceList(@Param("userId") Long userId,@Param("pager") Pager pager);
}