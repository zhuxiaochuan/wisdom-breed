package io.renren.modules.ext.service;

import java.util.Date;

import io.renren.common.service.CrudService;
import io.renren.modules.ext.dto.EntImageUploadDTO;
import io.renren.modules.ext.entity.EntImageUploadEntity;

/**
 * 图片上传表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-23
 */
public interface EntImageUploadService extends CrudService<EntImageUploadEntity, EntImageUploadDTO> {
	
	/**
	 * 按日期查询栋舍摄像头已完成图片抓取数
	 */
	public Long dormCatchCompleteCnt(Long dormId,Date date) ;
	
	/**
	 * 已完成图片上传数
	 */
	public Long dormUploadCompleteCnt(Long dormId,Date date) ;

}