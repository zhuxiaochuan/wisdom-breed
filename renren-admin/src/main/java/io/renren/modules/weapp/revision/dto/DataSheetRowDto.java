package io.renren.modules.weapp.revision.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;


@Data
public class DataSheetRowDto implements Serializable {
	
    private static final long serialVersionUID = 1L;
    
    // 日期
    private String day;
    
    private Date dayDate ;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataSheetRowDto other = (DataSheetRowDto) obj;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		return result;
	}
}