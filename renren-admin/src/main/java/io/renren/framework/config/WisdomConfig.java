/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.framework.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "wisdom")
@Data
public class WisdomConfig {
	
	private static final int default_online_threshold = 10;

	private Integer onlineThreshold;
	
    private Boolean enableSign;
    
    public void setEnableSign(Boolean enableSign) {
    	if(null == enableSign) {
    		enableSign = false;
    	}
    	this.enableSign = enableSign ;
    }
    
    public void setOnlineThreshold(Integer onlineThreshold) {
    	if(null == onlineThreshold) {
    		onlineThreshold = default_online_threshold ;
    	}
    	this.onlineThreshold = onlineThreshold ;
    }
    
}
