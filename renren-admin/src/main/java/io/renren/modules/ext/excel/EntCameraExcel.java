package io.renren.modules.ext.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 摄像头
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
public class EntCameraExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "企业id")
    private Long enterpriseId;
    @Excel(name = "场地id")
    private Long placeId;
    @Excel(name = "栋舍id")
    private Long dormId;
    @Excel(name = "名称")
    private String cameraName;
    @Excel(name = "SN号")
    private String snNumber;
    @Excel(name = "IP地址")
    private String ip;
    @Excel(name = "通道号")
    private Integer channel;
    @Excel(name = "在线状态")
    private Integer onlineStatus;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;

}