package io.renren.modules.ext.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.ext.entity.EntRevisionTaskCommitEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 任务提交表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-27
 */
@Mapper
public interface EntRevisionTaskCommitDao extends BaseDao<EntRevisionTaskCommitEntity> {
	
}