package io.renren.modules.ext.dto;

import java.io.Serializable;

import lombok.Data;


/**
 * 用户场地
 */
@Data
public class UserPlaceDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	private Long userId;
	private String username;
	private String realName;
	
	private Long enterpriseId;
	private String enterpriseName;
	private String shortName;
	
	private Long placeId ;
	private String placeName ;
	
	private Long placeType ;
	private String placeTypeName;
	private String keyWord ;

}