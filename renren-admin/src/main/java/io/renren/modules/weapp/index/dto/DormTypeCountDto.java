package io.renren.modules.weapp.index.dto;

import java.io.Serializable;

import lombok.Data;


/**
 * 栋舍类型 - count
 */
@Data
public class DormTypeCountDto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    // 类型id
    private Long id ;
    
    private Integer deliver ;

    // 类型名称
	private String name;
	
	// 盘点数量
	private Integer value = 0; 
	
	// 分娩数量
	private Integer deliverCnt = 0; 
	
	public void setValue(Integer value) { 
		this.value = (null == value ? 0 : value) ;
	}

}