package io.renren.modules.weapp.video.form;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class WxCameraForm {
	
	// 栋舍
	@NotNull(message="栋舍id不能为空")
	private Long dormId;
	
}
