package io.renren.modules.ext.service;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.dto.EnterpriseDTO;
import io.renren.modules.ext.entity.EnterpriseEntity;
import io.renren.modules.ext.form.dormType.DormTypeListQueryForm;
import io.renren.modules.ext.form.enterprise.EnterpriseListQueryForm;

import java.util.Map;

/**
 * 企业
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
public interface EnterpriseService extends CrudService<EnterpriseEntity, EnterpriseDTO> {

	public PageData<EnterpriseDTO> pageData(final EnterpriseListQueryForm form, Pager pager);

	public PageData<EnterpriseDTO> findEnableList(Map<String, Object> params, Pager pager);
	/**
	 * 查询启用中的企业
	 */
	public PageData<EnterpriseDTO> findEnableList(String name,Pager pager);

}