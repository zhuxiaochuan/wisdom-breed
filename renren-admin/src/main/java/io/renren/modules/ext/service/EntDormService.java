package io.renren.modules.ext.service;

import java.util.List;
import java.util.Map;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.common.utils.RR;
import io.renren.modules.ext.dto.EntDormDTO;
import io.renren.modules.ext.entity.EntDormEntity;
import io.renren.modules.ext.form.dorm.DormListQueryForm;

/**
 * 栋舍
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
public interface EntDormService extends CrudService<EntDormEntity, EntDormDTO> {
	
	public EntDormDTO dormDetail(Long id);
	
	public PageData<EntDormDTO> pageData(final DormListQueryForm form,Pager pager);

	/**
	 * 查询启用中的
	 */
	public PageData<EntDormDTO> findEnableList(Map<String, Object> params, Pager pager);
	
	/**
	 * 栋舍的摄像头数目
	 */
	public Long dormCameraCount(final Long dormId);
	
	public RR enable(final Long id);
	
	public RR disable(final Long id);
	
	public List<EntDormEntity> dormList(final Long placeId) ;

}