package io.renren.modules.ext.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.EnableService;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntUserManageDao;
import io.renren.modules.ext.dto.EntUserManageDTO;
import io.renren.modules.ext.entity.EntUserPlaceEntity;
import io.renren.modules.ext.form.entuser.EntUserListQueryForm;
import io.renren.modules.ext.service.EntUserManageService;
import lombok.RequiredArgsConstructor;

/**
 * 企业用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Service
@RequiredArgsConstructor
public class EntUserManageServiceImpl extends CrudServiceImpl<EntUserManageDao,EntUserPlaceEntity, EntUserManageDTO> implements EntUserManageService {
	
	private final EntUserManageDao entUserManageDao ;
	
	private final EnableService enableService;

	@Override
	public PageData<EntUserManageDTO> pageData(EntUserListQueryForm form, Pager pager) {
		List<EntUserManageDTO> list = entUserManageDao.listData(form, pager) ;   
		int total = entUserManageDao.listDataCount(form);  
		return getPageData(list, total, EntUserManageDTO.class); 
	}

	@Override
	public QueryWrapper<EntUserPlaceEntity> getWrapper(Map<String, Object> params) {
		return null;
	}

	
	@Override
	public EntUserManageDTO userDetail(Long userId,Long enterpriseId) {
		EntUserManageDTO dto = new  EntUserManageDTO();
		dto.setId(userId);
		dto.setUserId(userId);
		dto.setEnterpriseId(enterpriseId);
		QueryWrapper<EntUserPlaceEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", userId);
		queryWrapper.eq("enterprise_id", enterpriseId);
        List<EntUserPlaceEntity> selectList = entUserManageDao.selectList(queryWrapper); 
        if(!Util.isEmpty(selectList)) {
        	List<Long> entList = new ArrayList<Long>();
        	for (EntUserPlaceEntity ent : selectList) {
        		entList.add(ent.getPlaceId());
        		String job = ent.getJob();
        		if(!Util.isEmpty(job)) {
        			dto.setJob(job);
        		}
        		Integer userType = ent.getUserType();
        		if(!Util.isEmpty(userType)) {
        			dto.setUserType(userType);
        		}
			}
        	dto.setEntPlaceList(entList);
        }
    	return dto ;
	}
	
	
	@Override
	@Transactional
    public void save(EntUserManageDTO dto) {
		Long id = dto.getId();
		Long enterpriseId = dto.getEnterpriseId();
		QueryWrapper<EntUserPlaceEntity> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", id);
		queryWrapper.eq("enterprise_id", enterpriseId);
		List<EntUserPlaceEntity> selectList = baseDao.selectList(queryWrapper);
		if(!Util.isEmpty(selectList)) {
			baseDao.deleteBatchIds(selectList);
		}    
        List<EntUserPlaceEntity> addList = new ArrayList<EntUserPlaceEntity>();
        List<Long> entList = dto.getEntPlaceList();
        if(!Util.isEmpty(entList)) {
        	for (Long pId : entList) {
        		EntUserPlaceEntity en = new EntUserPlaceEntity();
				en.setEnterpriseId(enterpriseId);
				en.setPlaceId(pId);
				en.setUserId(id);
				en.setStatus(dto.getStatus());
				en.setJob(dto.getJob());
				en.setUserType(dto.getUserType());
				addList.add(en);
			}
        	baseDao.insertBatch(addList);
        }
    }

	@Override
	@Transactional
	public RR enable(Long userId, Long enterpriseId) {
		
		if(Util.isEmpty(userId)) {
			return RR.failed("userId不能为空");
		}
		if(Util.isEmpty(enterpriseId)) {
			return RR.failed("enterpriseId不能为空");
		}
		QueryWrapper<EntUserPlaceEntity> queryWrapper = new QueryWrapper<EntUserPlaceEntity>();
		queryWrapper.eq("user_id", userId);
		queryWrapper.eq("enterprise_id", enterpriseId);
		List<EntUserPlaceEntity> userPlaceList = baseDao.selectList(queryWrapper);
		if(!Util.isEmpty(userPlaceList)) {
			userPlaceList.forEach((item)->{
				RR r = enableService.enable(item.getId(), baseDao);  
			});
		}
		return RR.ok();
	}

	@Override
	@Transactional
	public RR disable(Long userId, Long enterpriseId) {
		if(Util.isEmpty(userId)) {
			return RR.failed("userId不能为空");
		}
		if(Util.isEmpty(enterpriseId)) {
			return RR.failed("enterpriseId不能为空");
		}
		QueryWrapper<EntUserPlaceEntity> queryWrapper = new QueryWrapper<EntUserPlaceEntity>();
		queryWrapper.eq("user_id", userId);
		queryWrapper.eq("enterprise_id", enterpriseId);
		List<EntUserPlaceEntity> userPlaceList = baseDao.selectList(queryWrapper);
		if(!Util.isEmpty(userPlaceList)) {
			userPlaceList.forEach((item)->{
				RR r = enableService.disable(item.getId(), baseDao);  
			});
		}
		return RR.ok();
	}
	
	@Override
	@Transactional
	public RR delete(Long userId, Long enterpriseId) {
		if(Util.isEmpty(userId)) {
			return RR.failed("userId不能为空");
		}
		if(Util.isEmpty(enterpriseId)) {
			return RR.failed("enterpriseId不能为空");
		}
		QueryWrapper<EntUserPlaceEntity> queryWrapper = new QueryWrapper<EntUserPlaceEntity>();
		queryWrapper.eq("user_id", userId);
		queryWrapper.eq("enterprise_id", enterpriseId);
		List<EntUserPlaceEntity> userPlaceList = baseDao.selectList(queryWrapper);
		if(!Util.isEmpty(userPlaceList)) {
			baseDao.delete(queryWrapper);
		}
		return RR.ok();
	}

}