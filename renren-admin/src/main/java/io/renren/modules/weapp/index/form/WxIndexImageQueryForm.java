package io.renren.modules.weapp.index.form;

import java.util.Date;

import javax.validation.constraints.NotNull;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

@Data
public class WxIndexImageQueryForm {
	
	// 企业
	@NotNull(message = "请选择企业")
	private Long enterpriseId;
	
	// 场地
	@NotNull(message = "请选择场地")
	private Long placeId;
	
	// 栋舍
	@NotNull(message = "请选择栋舍")
	private Long dormId;
	
	/*创建时间*/
	private Date date ; 
	
	public Date getEndTime() {
		if(!Util.isEmpty(date)) {
			date = DateUtil.endOfDay(date); 
		}
		return date ;
	}
	
}
