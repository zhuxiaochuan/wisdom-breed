package io.renren.modules.ext.form.revision;

import java.util.Date;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

@Data
public class PlaceRevisionQueryForm {
	
	// 企业
	private String enterpriseName;
	
	// 场地
	private String placeName;
	
	/*创建时间*/
	private Date date ; 
	
	public Date getEndTime() {
		if(!Util.isEmpty(date)) {
			date = DateUtil.endOfDay(date); 
		}
		return date ;
	}
	
}
