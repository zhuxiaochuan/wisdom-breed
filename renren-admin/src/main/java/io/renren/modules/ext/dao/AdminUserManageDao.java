package io.renren.modules.ext.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.AdminUserManageDTO;
import io.renren.modules.ext.dto.EntTaskUserDTO;
import io.renren.modules.ext.entity.EntUserEnterpriseEntity;
import io.renren.modules.ext.form.adminuser.AdminUserListQueryForm;
import io.renren.modules.ext.form.adminuser.TaskUserForm;

/**
 * 后台用户管理(给用户分配企业)
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Mapper
public interface AdminUserManageDao extends BaseDao<EntUserEnterpriseEntity> {
	
    List<AdminUserManageDTO> listData(@Param("form") final AdminUserListQueryForm form,  @Param("pager") Pager pager);
	
	public int listDataCount(@Param("form") final AdminUserListQueryForm form);
	
	public List<EntTaskUserDTO> taskUserList(@Param("form") TaskUserForm form,@Param("pager") Pager pager);
	
}