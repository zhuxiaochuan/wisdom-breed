package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import io.renren.common.dto.EnableDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 企业
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
public class EnterpriseDTO extends EnableDto implements Serializable {
	
    private static final long serialVersionUID = 1L;

	// @ApiModelProperty(value = "id")
	private Long id;

	// @ApiModelProperty(value = "企业名称")
	private String name;

	// @ApiModelProperty(value = "企业简称")
	private String shortName;

	// @ApiModelProperty(value = "地区代码")
	private String areaCode;

	// @ApiModelProperty(value = "详细地址")
	private String address;

	// @ApiModelProperty(value = "联系人")
	private String contactPerson;

	// @ApiModelProperty(value = "联系电话")
	private String contactNumber;

	// @ApiModelProperty(value = "创建者")
	private Long creator;

	// @ApiModelProperty(value = "创建时间")
	private Date createDate;

	// @ApiModelProperty(value = "更新者")
	private Long updater;

	// @ApiModelProperty(value = "更新时间")
	private Date updateDate;

	private String updaterName;

	// @ApiModelProperty(value = "场地数量")
	private Integer placeCount;

	// @ApiModelProperty(value = "栋舍数量")
	private Integer dormCount;
	/**
	 * 工控机数量
	 */
	private Integer ipcCount;
	/**
	 * 摄像头数量
	 */
	private Integer cameraCount;

}