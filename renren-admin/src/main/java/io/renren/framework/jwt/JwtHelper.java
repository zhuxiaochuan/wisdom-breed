package io.renren.framework.jwt;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.base.Preconditions;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ReflectUtil;
import io.renren.common.utils.Util;

public class JwtHelper {
	// jwt签发者
	private static final String ISSUSER = "SFTZ-Integrity";

	// 秘钥
	private static final String MINAPP_SECRET = "X-Integrity-Sftz-Minapp";
	// jwt的主题
	private static final String MINAPP_SUBJECT = "jwt for minapp user";
	// 接收jwt的一方
	private static final String MINAPP_AUDIENCE = "MINIAPP-USER";

	// token 过期时间2(小时)
	private static final int EXPIRE_MINAPP = 2;

	// admin 部分-----------------------------------------------------------

	private static final String ADMIN_SECRET = "X-Integrity-Sftz-Admin";
	private static final String ADMIN_SUBJECT = "jwt for admin user";
	private static final String ADMIN_AUDIENCE = "ADMIN-USER";

	private static final int EXPIRE_ADMIN = 2;

	private JwtHelper() {
	}

	/**
	 * 根据dto生成minapp-token
	 */
	public static String createMinappToken(JwtMinappUserDto user) {
		Preconditions.checkNotNull(user, "user cannot be null");
		Map<String, Object> claims = BeanUtil.beanToMap(user);  
		return createMinappToken(claims);
	}

	/**
	 * 根据claims生成minapp-token
	 */
	public static String createMinappToken(Map<String, Object> claims) {
		Preconditions.checkNotNull(claims, "claims cannot be null");
		// 签名时间 
		Date issueAt = new Date();
		// 过期时间 
		Date expireDate = getAfterDate(issueAt, 0, 0, 0, EXPIRE_MINAPP, 0, 0);
		// Header
		Map<String, Object> header = new HashMap<String, Object>();
		header.put("alg", "HS256");
		header.put("typ", "JWT");

		Builder builder = JWT.create().withHeader(header).withIssuer(ISSUSER).withSubject(MINAPP_SUBJECT)
				.withAudience(MINAPP_AUDIENCE).withIssuedAt(issueAt).withExpiresAt(expireDate);

		// 设置Payload
		setPayLoad(claims, builder);

		// 签名 Signature
		Algorithm algorithm = Algorithm.HMAC256(MINAPP_SECRET);
		String token = builder.sign(algorithm);
		return token;
	}

	/**
	 * 验证minapp-token获取用户
	 */
	public static JwtMinappUserDto verifyTokenUserMinapp(String token) throws Exception {
		return verifyTokenMinapp(token, JwtMinappUserDto.class);
	}

	/**
	 * 验证minapp-token
	 */
	public static <T> T verifyTokenMinapp(String token, Class<T> classOfT) throws Exception {
		Algorithm algorithm = Algorithm.HMAC256(MINAPP_SECRET);
		return verifyTokenGet(token, classOfT, algorithm);
	}

	//---------------------------------------------private---------------------------------------------------

	/**
	 * 验证token并获取payload
	 */
	private static <T> T verifyTokenGet(String token, Class<T> classOfT, Algorithm algorithm) throws Exception {
		JWTVerifier verifier = JWT.require(algorithm).withIssuer(ISSUSER).build();
		DecodedJWT jwt = verifier.verify(token);
		Map<String, Claim> claims = jwt.getClaims();

		T result = ReflectUtil.newInstance(classOfT);
		Field[] fields = ReflectUtil.getFields(classOfT);
		if (!Util.isEmpty(fields)) {
			for (Field f : fields) {
				String fieldName = f.getName();
				Claim claim = claims.get(fieldName);
				if (Util.isEmpty(claim)) {
					continue;
				}
				
				ReflectUtil.setAccessible(f);
				
				Class<?> type = f.getType();
				if (type.isAssignableFrom(Boolean.class)) {
					ReflectUtil.setFieldValue(result, f, claim.asBoolean());
				} else if (type.isAssignableFrom(Date.class)) {
					ReflectUtil.setFieldValue(result, f, claim.asDate());
				} else if (type.isAssignableFrom(Double.class)) {
					ReflectUtil.setFieldValue(result, f, claim.asDouble());
				} else if (type.isAssignableFrom(Integer.class)) {
					ReflectUtil.setFieldValue(result, f, claim.asInt());
				} else if (type.isAssignableFrom(Long.class)) {
					ReflectUtil.setFieldValue(result, f, claim.asLong());
				} else if (type.isAssignableFrom(String.class)) {
					ReflectUtil.setFieldValue(result, f, claim.asString());
				} else {
					throw new IllegalArgumentException("不支持的claim类型");
				}
			}
		}
		return result;
	}

	private static Date getAfterDate(Date date, int year, int month, int day, int hour, int minute, int second) {
		if (date == null) {
			date = new Date();
		}

		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		if (year != 0) {
			cal.add(Calendar.YEAR, year);
		}
		if (month != 0) {
			cal.add(Calendar.MONTH, month);
		}
		if (day != 0) {
			cal.add(Calendar.DATE, day);
		}
		if (hour != 0) {
			cal.add(Calendar.HOUR_OF_DAY, hour);
		}
		if (minute != 0) {
			cal.add(Calendar.MINUTE, minute);
		}
		if (second != 0) {
			cal.add(Calendar.SECOND, second);
		}
		return cal.getTime();
	}

	private static void setPayLoad(Map<String, Object> claims, Builder builder) {
		Set<Entry<String, Object>> entrySet = claims.entrySet();
		for (Entry<String, Object> en : entrySet) {
			String k = en.getKey();
			Object v = en.getValue();
			if(null == v) {
				continue ;
			}
			
			if (v instanceof Boolean) {
				builder.withClaim(k, (Boolean) v);
			} else if (v instanceof Date) {
				builder.withClaim(k, (Date) v);
			} else if (v instanceof Double) {
				builder.withClaim(k, (Double) v);
			} else if (v instanceof Integer) {
				builder.withClaim(k, (Integer) v);
			} else if (v instanceof Long) {
				builder.withClaim(k, (Long) v);
			} else if (v instanceof String) {
				builder.withClaim(k, (String) v);
			} else {
				throw new IllegalArgumentException("不支持的claim类型");
			}
		}
	}

}