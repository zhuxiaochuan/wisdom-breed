package io.renren.modules.ext.service;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.modules.ext.dto.PlaceRevisionDTO;
import io.renren.modules.ext.entity.EntRevisionDataEntity;
import io.renren.modules.ext.form.revision.PlaceRevisionQueryForm;

/**
 * 场地盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
public interface PlaceRevisionService extends CrudService<EntRevisionDataEntity, PlaceRevisionDTO>{
	
	public PageData<PlaceRevisionDTO> pageData(final PlaceRevisionQueryForm form,Pager pager);
	
}