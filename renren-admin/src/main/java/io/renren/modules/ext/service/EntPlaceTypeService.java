package io.renren.modules.ext.service;

import java.util.Map;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.dto.EntPlaceTypeDTO;
import io.renren.modules.ext.entity.EntPlaceTypeEntity;
import io.renren.modules.ext.form.place.PlaceListQueryForm;
import io.renren.modules.ext.form.placeType.PlaceTypeListQueryForm;

/**
 * 场地类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
public interface EntPlaceTypeService extends CrudService<EntPlaceTypeEntity, EntPlaceTypeDTO> {

	/**
	 * 场地类型列表
	 */
	public PageData<EntPlaceTypeDTO> pageData(final PlaceTypeListQueryForm form, Pager pager);
	/**
	 * 查询启用中的
	 */
	public PageData<EntPlaceTypeDTO> findEnableList(Map<String, Object> params,Pager pager);

}