package io.renren;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import io.renren.modules.quartz.service.CaptureImageService;

/**
 * 启动类
 * <p>
 *	用于在项目启动的时候做一些事情，比如验证参数，加载数据，定时器设置，检查开发规范等	
 *
 * @author   朱晓川
 */
@Component
public class Startup implements CommandLineRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(Startup.class);

	// 数据库连接url
	@Value("${spring.datasource.druid.url}")
	private String db_url;

	// 启用的配置文件
	@Value("${spring.profiles.active}")
	private String active;
	
	@Autowired
	private CaptureImageService captureImageService; 

	@Override
	public void run(String... args) throws Exception {
		LOGGER.info("配置文件active:" + active);
		LOGGER.info("数据库连接URL:" + db_url);
		
		quartzEnableCaptureJob();
	}
	
	private void quartzEnableCaptureJob() {
		captureImageService.quartzActiveCaptureJob(); 
	}
}
