package io.renren.common.utils;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 响应信息主体
 * <P>
 * 注意: 当返回成功结果的时候，单一参数代表的是data。不提供单参数String的ok重载，目的在于当你想返回的data本身是String的时候，结果是R.ok(String msg),而不是在data中。
 * 因此ok返回msg的格式一律使用两个参数的版本:
 *   R.ok(null,msg)
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class RR<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 成功标记
	 */
	public static final Integer SUCCESS = 0;

	/**
	 * 失败标记
	 */
	public static final Integer FAIL = 1;

	@Getter
	@Setter
	private int code;

	@Getter
	@Setter
	private String msg;

	@Getter
	@Setter
	private T data;

	public static <T> RR<T> ok() {
		return restResult(null, SUCCESS, null);
	}

	public static <T> RR<T> ok(T data) {
		return restResult(data, SUCCESS, null);
	}

	public static <T> RR<T> ok(T data, String msg) {
		return restResult(data, SUCCESS, msg);
	}

	public static <T> RR<T> failed() {
		return restResult(null, FAIL, null);
	}

	public static <T> RR<T> failed(String msg) {
		return restResult(null, FAIL, msg);
	}

	public static <T> RR<T> failed(T data) {
		return restResult(data, FAIL, null);
	}

	public static <T> RR<T> failed(T data, String msg) {
		return restResult(data, FAIL, msg);
	}

	private static <T> RR<T> restResult(T data, int code, String msg) {
		RR<T> apiResult = new RR<>();
		apiResult.setCode(code);
		apiResult.setData(data);
		apiResult.setMsg(msg);
		return apiResult;
	}

}
