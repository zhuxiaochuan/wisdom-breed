package io.renren.modules.ext.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 工控机
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
public class EntIpcExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "企业id")
    private Long enterpriseId;
    @Excel(name = "场地id")
    private Long placeId;
    @Excel(name = "栋舍id")
    private Long dormId;
    @Excel(name = "工控机ID")
    private String idNo;
    @Excel(name = "IP地址")
    private String ip;
    @Excel(name = "软件版本")
    private String softVersion;
    @Excel(name = "厂家")
    private String producer;
    @Excel(name = "型号")
    private String model;
    @Excel(name = "在线状态")
    private Integer onlineStatus;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;

}