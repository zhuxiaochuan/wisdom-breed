package io.renren.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tangzc.mpe.bind.metadata.annotation.BindField;
import com.tangzc.mpe.bind.metadata.annotation.JoinCondition;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 路由器
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_router")
public class EntRouterEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 企业id
     */
	private Long enterpriseId;
    /**
     * 场地id
     */
	private Long placeId;
    /**
     * 栋舍id
     */
	private Long dormId;
    /**
     * 厂家
     */
	private String producer;
    /**
     * 型号
     */
	private String model;
    /**
     * 内网IP
     */
	private String innerIp;
    /**
     * 外网IP
     */
	private String outterIp;
    /**
     * SIM卡号
     */
	private String simNumber;
    /**
     * SN号
     */
	private String snNumber;
    /**
     * 更新者
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;

	// 关联字段，非本实体的字段
	@BindField(entity = EnterpriseEntity.class,field = "name",conditions = { @JoinCondition(selfField = "enterpriseId",joinField = "id") })
	@TableField(exist = false)
	private String enterpriseName;

	@BindField(entity = EntPlaceEntity.class,field = "placeName",conditions = { @JoinCondition(selfField = "placeId",joinField = "id") })
	@TableField(exist = false)
	private String placeName;

	@BindField(entity = EntDormEntity.class,field = "dormName",conditions = { @JoinCondition(selfField = "dormId",joinField = "id") })
	@TableField(exist = false)
	private String dormName;
}