package io.renren.modules.ext.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.renren.common.enums.EnableEnum;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.EnableService;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntCameraDao;
import io.renren.modules.ext.dao.EntDormDao;
import io.renren.modules.ext.dao.EntImageUploadDao;
import io.renren.modules.ext.dao.EntPlaceDao;
import io.renren.modules.ext.dao.EnterpriseDao;
import io.renren.modules.ext.dto.EntDormDTO;
import io.renren.modules.ext.entity.EntCameraEntity;
import io.renren.modules.ext.entity.EntDormEntity;
import io.renren.modules.ext.entity.EntPlaceEntity;
import io.renren.modules.ext.entity.EnterpriseEntity;
import io.renren.modules.ext.form.dorm.DormListQueryForm;
import io.renren.modules.ext.service.EntDormService;
import lombok.RequiredArgsConstructor;

/**
 * 栋舍
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Service
@RequiredArgsConstructor
public class EntDormServiceImpl extends CrudServiceImpl<EntDormDao, EntDormEntity, EntDormDTO> implements EntDormService {
	
	private final EnterpriseDao enterpriseDao ;
	private final EntPlaceDao entPlaceDao ;
	private final EntDormDao entDormDao ;
	
	private final EntCameraDao entCameraDao ;
	private final EntImageUploadDao entImageUploadDao ; 
	private final MetaObjectHandler metaObjectHandler ; 
	
	private final EnableService<EntDormEntity> enableService;
	
    @Override
    public QueryWrapper<EntDormEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<EntDormEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }
    
    @Override
    public EntDormDTO dormDetail(Long id) {
    	EntDormDTO dto = get(id) ; 
    	if(!Util.isEmpty(dto)) {
    		Long enterpriseId = dto.getEnterpriseId(); 
        	
        	EnterpriseEntity enterprise = enterpriseDao.selectById(enterpriseId); 
        	if(!Util.isEmpty(enterprise)) {
        		dto.setEnterpriseName(enterprise.getName()); 
        	}
        	
        	Long placeId = dto.getPlaceId();
        	EntPlaceEntity place = entPlaceDao.selectById(placeId) ; 
        	if(!Util.isEmpty(place)) {
        		dto.setPlaceName(place.getPlaceName());
        	}
    	}
    	return dto ;
    }

	@Override
	public PageData<EntDormDTO> pageData(DormListQueryForm form, Pager pager) {
		System.out.println(metaObjectHandler); 
		List<EntDormDTO> list = entDormDao.listData(form, pager) ;   
		int total = entDormDao.listDataCount(form);  
		return getPageData(list, total, EntDormDTO.class); 
	}

	@Override
	public PageData<EntDormDTO> findEnableList(Map<String, Object> params, Pager pager) {
		String dormName = (String) params.get("name") ;
		Long entPlaceId = Long.valueOf(params.get("entPlaceId") +"")  ;

		LambdaQueryWrapper<EntDormEntity> lw = Wrappers.lambdaQuery(EntDormEntity.class);
		if(!Util.isEmpty(dormName)) {
			lw.like(EntDormEntity::getDormName, dormName);
		}
		if(!Util.isEmpty(entPlaceId)) {
			lw.eq(EntDormEntity::getPlaceId, entPlaceId);
		}
		lw.eq(EntDormEntity::getStatus, EnableEnum.ENABLE.intKey());

		Page<EntDormEntity> page = new Page<>(pager.getPageNumber(), pager.getPageSize());
		page = baseDao.selectPage(page, lw);
		return getPageData(page, currentDtoClass());
	}

	@Override
	public Long dormCameraCount(Long dormId) { 
		if(Util.isEmpty(dormId)) {
			return 0l ;
		}
		Long dormCameraCnt = entCameraDao.selectCount(Wrappers.lambdaQuery(EntCameraEntity.class).eq(EntCameraEntity::getDormId, dormId)) ; 
		return dormCameraCnt ;
	}
	
	@Transactional
	public RR enable(final Long id) {
		RR r = enableService.enable(id, baseDao);  
		return r ;
	}
	
	@Transactional
	public RR disable(final Long id) {
		RR r = enableService.disable(id, baseDao);  
		return r ;
	}

	@Override
	public List<EntDormEntity> dormList(Long placeId) {
		LambdaQueryWrapper<EntDormEntity> wrapper = Wrappers.lambdaQuery(EntDormEntity.class).eq(EntDormEntity::getPlaceId, placeId).eq(EntDormEntity::getStatus, EnableEnum.ENABLE.intKey()) ;  
		List<EntDormEntity> list = entDormDao.selectList(wrapper) ;
		return list;
	}

}