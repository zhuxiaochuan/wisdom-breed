package io.renren.modules.ext.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.renren.common.dao.BaseDao;
import io.renren.modules.ext.entity.EntRevisionTaskEntity;

/**
 * 今日任务
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Mapper
public interface TodayTaskDao extends BaseDao<EntRevisionTaskEntity> {
	
	/**
	 * 今日用户应该执行的任务
	 * <P>
	 * 不包含禁用的企业、场地、栋舍
	 */
	public List<EntRevisionTaskEntity> todayTaskForUser(@Param("userId") Long userId);
	
	/**
	 * 摄像头当日应执行的任务
	 * @param cameraId 摄像头id
	 * @return
	 */
	public List<EntRevisionTaskEntity> todayTaskForCamera(@Param("cameraId") Long cameraId);
	
}