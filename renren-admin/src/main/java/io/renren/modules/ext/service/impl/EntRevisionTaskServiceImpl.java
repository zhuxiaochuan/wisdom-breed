package io.renren.modules.ext.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tangzc.mpe.bind.Binder;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntCameraDao;
import io.renren.modules.ext.dao.EntDormTypeDao;
import io.renren.modules.ext.dao.EntImageUploadDao;
import io.renren.modules.ext.dao.EntRevisionDataDao;
import io.renren.modules.ext.dao.EntRevisionTaskCommitDao;
import io.renren.modules.ext.dao.EntRevisionTaskDao;
import io.renren.modules.ext.dao.EntRevisionTaskDetailDao;
import io.renren.modules.ext.dto.EntImageUploadDTO;
import io.renren.modules.ext.dto.EntRevisionTaskDTO;
import io.renren.modules.ext.entity.EntCameraEntity;
import io.renren.modules.ext.entity.EntDormTypeEntity;
import io.renren.modules.ext.entity.EntRevisionDataEntity;
import io.renren.modules.ext.entity.EntRevisionTaskCommitEntity;
import io.renren.modules.ext.entity.EntRevisionTaskDetailEntity;
import io.renren.modules.ext.entity.EntRevisionTaskEntity;
import io.renren.modules.ext.enums.TaskCommitStatusEnum;
import io.renren.modules.ext.enums.YesOrNoEnum;
import io.renren.modules.ext.form.revision.EnterpriseRevisionTaskQueryForm;
import io.renren.modules.ext.form.revision.SubmitTaskDataForm;
import io.renren.modules.ext.form.revision.TaskDataDetailForm;
import io.renren.modules.ext.service.EntRevisionTaskService;
import io.renren.modules.security.user.SecurityUser;
import lombok.RequiredArgsConstructor;

/**
 * 任务管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Service
@RequiredArgsConstructor
public class EntRevisionTaskServiceImpl extends CrudServiceImpl<EntRevisionTaskDao, EntRevisionTaskEntity, EntRevisionTaskDTO> implements EntRevisionTaskService {
	
	private static final Logger log = LoggerFactory.getLogger(EntRevisionTaskServiceImpl.class) ;
	
	private final EntRevisionTaskDao entRevisionTaskDao ;
	private final EntRevisionTaskCommitDao entRevisionTaskCommitDao ;
	private final EntRevisionTaskDetailDao entRevisionTaskDetailDao ;
	private final EntRevisionDataDao entRevisionDataDao ;
	private final EntDormTypeDao entDormTypeDao ;
	private final EntCameraDao entCameraDao ;
	private final EntImageUploadDao entImageUploadDao ;
	
    @Override
    public QueryWrapper<EntRevisionTaskEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<EntRevisionTaskEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

	@Override
	public PageData<EntRevisionTaskDTO> pageData(EnterpriseRevisionTaskQueryForm form, Pager pager) {
		form.setToday(false); 
		List<EntRevisionTaskEntity> list = entRevisionTaskDao.listData(form, pager) ;   
		int total = entRevisionTaskDao.listDataCount(form);  
		
		PageData<EntRevisionTaskDTO> pageData = getPageData(list, total, EntRevisionTaskDTO.class);
		return pageData;
	}
	
	/**
	 * 发布当日数据
	 */
	@Transactional
	@Override
	public RR post(Long userId) {
		if(Util.isEmpty(userId)) {
			return RR.failed("请选择用户") ;
		}
		
		Long postUser = SecurityUser.getUserId(); 
		DateTime now = DateUtil.date();
		DateTime beginOfDay = DateUtil.beginOfDay(now);  
		DateTime endOfDay = DateUtil.endOfDay(now);
		
		// 1 用户的当日的提交记录
		List<EntRevisionTaskCommitEntity> tcommitList = entRevisionTaskCommitDao.selectList(Wrappers.lambdaQuery(EntRevisionTaskCommitEntity.class)
				.eq(EntRevisionTaskCommitEntity::getUserId, userId)
				.eq(EntRevisionTaskCommitEntity::getCommitStatus, TaskCommitStatusEnum.COMMITED.intKey()) 
				.gt(EntRevisionTaskCommitEntity::getCreateDate, beginOfDay)
				.le(EntRevisionTaskCommitEntity::getCreateDate, endOfDay)) ; 
		
		if(Util.isEmpty(tcommitList)) {
			return RR.failed("用户当日未提交数据或暂无需要发布的数据") ; 
		}
	
		// 2 用户当日的所有任务数据
		LambdaQueryWrapper<EntRevisionTaskEntity> lambdaQ = Wrappers.lambdaQuery(EntRevisionTaskEntity.class).eq(EntRevisionTaskEntity::getCreator, userId).gt(EntRevisionTaskEntity::getCreateDate, beginOfDay).le(EntRevisionTaskEntity::getCreateDate, endOfDay);
		List<EntRevisionTaskEntity> allTask = entRevisionTaskDao.selectList(lambdaQ); 
		Binder.bindOn(allTask, EntRevisionTaskEntity::getDeliver);
		
		// 3 根据企业id计算场地数量与栋舍数量
		if(!Util.isEmpty(allTask)) { 
			// 盘点数据
			Map<Long, Map<String, Integer>> groupMap = allTask.stream().collect(Collectors.groupingBy(EntRevisionTaskEntity::getEnterpriseId,Collectors.collectingAndThen(Collectors.toList(), gList -> {
	            Map<String, Integer> map = new HashMap<>();
	            // 这里的数量是企业的总栋舍数量以及场地数量
	            map.put("placeCount", gList.stream().collect(Collectors.groupingBy(EntRevisionTaskEntity::getPlaceId, Collectors.toSet())).size());
	            map.put("dormCount", gList.stream().collect(Collectors.groupingBy(EntRevisionTaskEntity::getDormId, Collectors.toSet())).size());
	            return map;
	        })));
			
			// 用户当日的盘点数据
			LambdaQueryWrapper<EntRevisionDataEntity> rdw = Wrappers.lambdaQuery(EntRevisionDataEntity.class).eq(EntRevisionDataEntity::getCreator, userId).gt(EntRevisionDataEntity::getCreateDate, beginOfDay).le(EntRevisionDataEntity::getCreateDate, endOfDay);
			List<EntRevisionDataEntity> currentRevisionData = entRevisionDataDao.selectList(rdw) ; 
			if(!Util.isEmpty(currentRevisionData)) {
				return RR.failed("该用户盘点数据已发布，请勿重复发布") ;  
			}
			
			List<EntRevisionDataEntity> rData = allTask.stream().map(t -> {
				EntRevisionDataEntity rd = new EntRevisionDataEntity() ;
				BeanUtil.copyProperties(t, rd);
				
				Long enterpriseId = t.getEnterpriseId() ; 
				Map<String, Integer> map = groupMap.get(enterpriseId);
				Integer placeCount = map.get("placeCount"); 
				Integer dormCount = map.get("dormCount");
				rd.setPlaceCount(placeCount); 
				rd.setDormCount(dormCount); 
				rd.setDeliver(t.getDeliver()); 
				
				rd.setRevisionCount(t.getRevisionCount());
				rd.setDeliverCount(t.getDelivered()); 
				rd.setCreateDate(now); 
				return rd ;
			}).collect(Collectors.toList()); 
			
			entRevisionDataDao.insertBatch(rData) ;
			
			// 提交记录改为已发布
			tcommitList.forEach(tc -> {
				entRevisionTaskCommitDao.update(null, Wrappers.lambdaUpdate(EntRevisionTaskCommitEntity.class)
						.set(EntRevisionTaskCommitEntity::getCommitStatus,TaskCommitStatusEnum.POSTED.intKey())
						.set(EntRevisionTaskCommitEntity::getPostUser, postUser) 
						.set(EntRevisionTaskCommitEntity::getPostDate, now) 
						.eq(EntRevisionTaskCommitEntity::getId, tc.getId())) ; 
			});
		}
		
		return RR.ok(); 
	}

	@Override
	public RR submitTaskData(SubmitTaskDataForm form) {
		// 1 数据校验
		Long taskId = form.getTaskId(); 
		EntRevisionTaskDTO task = get(taskId); 
		if(Util.isEmpty(task)) {
			return RR.failed("任务不存在") ;
		}
		
		Long cameraId = form.getCameraId(); 
		EntCameraEntity camera = entCameraDao.selectById(cameraId) ; 
		if(Util.isEmpty(camera)) {
			return RR.failed("摄像头不存在") ;
		}
		
		Integer inputRvCount = form.getRevisionCount();  
		Integer inputDvCount = form.getDeliveredCount(); 
		
		Integer dormType = task.getDormType(); 
		EntDormTypeEntity dt = entDormTypeDao.selectById(dormType);
		if(Util.eq(YesOrNoEnum.YES.intKey(), dt.getDeliver())) {
			if(Util.isEmpty(inputDvCount)) {
				return RR.failed("请填写已分娩数量") ;
			}
		}
		if(Util.isEmpty(inputRvCount)) { 
			inputRvCount = 0 ;
		}
		if(Util.isEmpty(inputDvCount)) { 
			inputDvCount = 0 ;
		}
		
		// 2 保存摄像头盘点数据
		DateTime now = DateUtil.date(); 
		
		// 摄像头原值
		Integer cameraOriginRvcount = 0 ;
		Integer cameraOriginDvcount = 0 ;
		EntRevisionTaskDetailEntity td = entRevisionTaskDetailDao.selectOne(Wrappers.lambdaQuery(EntRevisionTaskDetailEntity.class).eq(EntRevisionTaskDetailEntity::getTaskId, taskId).eq(EntRevisionTaskDetailEntity::getCameraId, cameraId)); 
		if(Util.isEmpty(td)) {
			td = new EntRevisionTaskDetailEntity() ;
			td.setTaskId(taskId); 
			
			td.setCameraId(camera.getId()); 
			td.setCameraSn(camera.getSnNumber()); 
			td.setRevisionCount(inputRvCount); 
			td.setDelivered(inputDvCount); 
			
			td.setCompleteStatus(YesOrNoEnum.YES.intKey());
			td.setCompleteTime(now); 
			entRevisionTaskDetailDao.insert(td) ;
		}else {
			cameraOriginRvcount = td.getRevisionCount();
			cameraOriginDvcount = td.getDelivered();
			
			LambdaUpdateWrapper<EntRevisionTaskDetailEntity> lambdaUpdate = Wrappers.lambdaUpdate(EntRevisionTaskDetailEntity.class)
					.set(EntRevisionTaskDetailEntity::getRevisionCount, inputRvCount) 
					.set(EntRevisionTaskDetailEntity::getDelivered, inputDvCount) 
					.eq(EntRevisionTaskDetailEntity::getId, td.getId());   
			entRevisionTaskDetailDao.update(null, lambdaUpdate) ;
		}
		
		// 3 更新ent_revision_task
		Integer taskRvCount = task.getRevisionCount();  
		if(Util.isEmpty(taskRvCount)) { 
			taskRvCount = 0 ;
		}
		Integer taskDvCount = task.getDelivered();  
		if(Util.isEmpty(taskDvCount)) { 
			taskDvCount = 0 ;
		}
		
		log.info("摄像头:" + cameraId + " 原盘点数量:" + cameraOriginRvcount + " 当次盘点数量:" + inputRvCount); 
		log.info("摄像头:" + cameraId + " 原分娩数量:" + cameraOriginDvcount + " 当次分娩数量:" + inputDvCount); 
		
		Integer deltaRv = inputRvCount - cameraOriginRvcount ;
		Integer deltaDv = inputDvCount - cameraOriginDvcount ;
		
		log.info("任务:" + taskId + " 原盘点数量:" + taskRvCount + " 当次盘点增量:" + deltaRv); 
		log.info("任务:" + taskId + " 原分娩数量:" + taskDvCount + " 当次分娩增量:" + deltaDv); 
		
		taskRvCount += deltaRv;
		taskDvCount += deltaDv;
		
		entRevisionTaskDao.update(null, Wrappers.lambdaUpdate(EntRevisionTaskEntity.class) 
				.set(EntRevisionTaskEntity::getRevisionCount, taskRvCount)
				.set(EntRevisionTaskEntity::getDelivered, taskDvCount)
				.eq(EntRevisionTaskEntity::getId, taskId)) ;
		
		return RR.ok();
	}

	@Override
	public RR taskDataDetail(TaskDataDetailForm form) { 
		// 查询出盘点数量以及图片地址
		Long taskId = form.getTaskId(); 
		
		List<EntImageUploadDTO> data = entImageUploadDao.listImageUploadDetail(taskId) ; 
		
		return RR.ok(data);
	}
	
}