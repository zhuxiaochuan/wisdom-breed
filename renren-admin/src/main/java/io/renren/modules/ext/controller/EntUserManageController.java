package io.renren.modules.ext.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.RR;
import io.renren.common.utils.Result;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.dto.EntUserManageDTO;
import io.renren.modules.ext.form.entuser.EntUserListQueryForm;
import io.renren.modules.ext.service.EntUserManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * 企业用户管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@RestController
@RequestMapping("ext/entusermanage")
@Api(tags="企业用户管理")
public class EntUserManageController {
    @Autowired
    private EntUserManageService entUserManageService;

    @GetMapping("page")
    @ApiOperation("分页")
    @RequiresPermissions("ext:entusermanage:page")
    public Result<PageData<EntUserManageDTO>> page(final EntUserListQueryForm form,final Pager pager){
        PageData<EntUserManageDTO> page = entUserManageService.pageData( form, pager);
        return new Result<PageData<EntUserManageDTO>>().ok(page);
    }
    
    @GetMapping("{userId}/{enterpriseId}")
    @RequiresPermissions("ext:entusermanage:info")
    public Result<EntUserManageDTO> get(@PathVariable("userId") Long userId,@PathVariable("enterpriseId") Long enterpriseId){
    	EntUserManageDTO data = entUserManageService.userDetail(userId,enterpriseId);
        return new Result<EntUserManageDTO>().ok(data);
    }
    
    @PostMapping
    @LogOperation("保存")
    @RequiresPermissions("ext:entusermanage:save")
    public Result save(@RequestBody EntUserManageDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
        entUserManageService.save(dto);;
        return new Result();
    }
    
    @RequestMapping(value = { "/enable" })
    @LogOperation("启用")
    public RR enable(@RequestBody final EntUserManageDTO dto) {
		return entUserManageService.enable(dto.getUserId(),dto.getEnterpriseId());   
	}
    
    @RequestMapping(value = { "/disable" })
    @LogOperation("禁用")
    public RR disable(@RequestBody final EntUserManageDTO dto) {
		return entUserManageService.disable(dto.getUserId(),dto.getEnterpriseId());   
	}
    
    @RequestMapping(value = { "/delete" })
    @LogOperation("删除")
    public RR delete(@RequestBody final EntUserManageDTO dto) {
		return entUserManageService.delete(dto.getUserId(),dto.getEnterpriseId());   
	}
   

}