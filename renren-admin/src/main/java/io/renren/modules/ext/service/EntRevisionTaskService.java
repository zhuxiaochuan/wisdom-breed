package io.renren.modules.ext.service;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.common.utils.RR;
import io.renren.modules.ext.dto.EntRevisionTaskDTO;
import io.renren.modules.ext.entity.EntRevisionTaskEntity;
import io.renren.modules.ext.form.revision.EnterpriseRevisionTaskQueryForm;
import io.renren.modules.ext.form.revision.SubmitTaskDataForm;
import io.renren.modules.ext.form.revision.TaskDataDetailForm;

/**
 * 任务管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
public interface EntRevisionTaskService extends CrudService<EntRevisionTaskEntity, EntRevisionTaskDTO> {
	
	public PageData<EntRevisionTaskDTO> pageData(final EnterpriseRevisionTaskQueryForm form,Pager pager);
	
	/**
	 * 查询任务执行详情(任务摄像头盘点数据)
	 */
	public RR taskDataDetail(TaskDataDetailForm form);
	
	/**
	 * 发布当日数据
	 */
	public RR post(Long userId) ;
	
	/**
	 * 提交任务数据
	 * <p>
	 * 按栋舍提交
	 */
	public RR submitTaskData(SubmitTaskDataForm form) ;
	
}