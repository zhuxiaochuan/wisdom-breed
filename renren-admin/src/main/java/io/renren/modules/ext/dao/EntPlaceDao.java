package io.renren.modules.ext.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.entity.EntPlaceEntity;
import io.renren.modules.ext.form.place.PlaceListQueryForm;

/**
 * 场地
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Mapper
public interface EntPlaceDao extends BaseDao<EntPlaceEntity> {
	
	List<EntPlaceDTO> listData(@Param("form") final PlaceListQueryForm form,  @Param("pager") Pager pager);
	
	public int listDataCount(@Param("form") final PlaceListQueryForm form);
	
}