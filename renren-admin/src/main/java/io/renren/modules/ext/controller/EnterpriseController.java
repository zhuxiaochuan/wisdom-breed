package io.renren.modules.ext.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.form.dormType.DormTypeListQueryForm;
import io.renren.modules.ext.form.enterprise.EnterpriseListQueryForm;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.ext.dto.EnterpriseDTO;
import io.renren.modules.ext.service.EnterpriseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 企业
 *
 * @author zxc
 * @since 1.0.0 2022-07-13
 */
@RestController
@RequestMapping("ext/enterprise")
@Api(tags="企业")
public class EnterpriseController {
    @Autowired
    private EnterpriseService enterpriseService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("ext:enterprise:page")
    public Result<PageData<EnterpriseDTO>> page(final EnterpriseListQueryForm form, final Pager pager){
        PageData<EnterpriseDTO> page = enterpriseService.pageData(form,pager);

        return new Result<PageData<EnterpriseDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("ext:enterprise:info")
    public Result<EnterpriseDTO> get(@PathVariable("id") Long id){
        EnterpriseDTO data = enterpriseService.get(id);

        return new Result<EnterpriseDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("ext:enterprise:save")
    public Result save(@RequestBody EnterpriseDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        enterpriseService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("ext:enterprise:update")
    public Result update(@RequestBody EnterpriseDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        enterpriseService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("ext:enterprise:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        enterpriseService.delete(ids);

        return new Result();
    }

    @GetMapping("listEnable")
    public Result<PageData<EnterpriseDTO>> listEnable(@RequestParam Map<String, Object> params,final Pager pager){
    	String name = (String) params.get("name") ;
    	PageData<EnterpriseDTO> data = enterpriseService.findEnableList(name,pager);

        return new Result<PageData<EnterpriseDTO>>().ok(data);
    }


}