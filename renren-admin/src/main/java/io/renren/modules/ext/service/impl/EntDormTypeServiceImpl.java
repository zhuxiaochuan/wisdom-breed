package io.renren.modules.ext.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import io.renren.modules.ext.dao.EntDormDao;
import io.renren.modules.ext.dto.EntDormDTO;
import io.renren.modules.ext.form.dormType.DormTypeListQueryForm;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.renren.common.enums.EnableEnum;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ConvertUtils;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntDormTypeDao;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.entity.EntDormTypeEntity;
import io.renren.modules.ext.service.EntDormTypeService;

/**
 * 栋舍类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Service
@RequiredArgsConstructor
public class EntDormTypeServiceImpl extends CrudServiceImpl<EntDormTypeDao, EntDormTypeEntity, EntDormTypeDTO> implements EntDormTypeService {

	private final EntDormTypeDao entDormTypeDao ;

    @Override
    public QueryWrapper<EntDormTypeEntity> getWrapper(Map<String, Object> params){
        String status = (String)params.get("status");

        QueryWrapper<EntDormTypeEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(status), "status", status);

        return wrapper;
    }

	@Override
	public Map<Long, EntDormTypeDTO> queryEnable2Map() {
		Map<Long, EntDormTypeDTO> map = new HashMap<>() ;
		
		LambdaQueryWrapper<EntDormTypeEntity> lw = Wrappers.lambdaQuery(EntDormTypeEntity.class).eq(EntDormTypeEntity::getStatus, EnableEnum.ENABLE.intKey());
		List<EntDormTypeEntity> all = baseDao.selectList(lw); 
		if(!Util.isEmpty(all)) { 
			List<EntDormTypeDTO> list = ConvertUtils.sourceToTarget(all, EntDormTypeDTO.class);
			map = list.stream().collect(Collectors.toMap(EntDormTypeDTO::getId, e -> e));
		}
		return map;
	}

	@Override
	public PageData<EntDormTypeDTO> pageData(DormTypeListQueryForm form, Pager pager) {
		List<EntDormTypeDTO> list = entDormTypeDao.listData(form, pager) ;
		int total = entDormTypeDao.listDataCount(form);
		return getPageData(list, total, EntDormTypeDTO.class);
	}

	@Override
	public PageData<EntDormTypeDTO> findEnableList(Map<String, Object> params,Pager pager) {
		String typeName = (String) params.get("name") ;
    	
    	LambdaQueryWrapper<EntDormTypeEntity> lw = Wrappers.lambdaQuery(EntDormTypeEntity.class);
		if(!Util.isEmpty(typeName)) { 
			lw.like(EntDormTypeEntity::getTypeName, typeName);
		}
		lw.eq(EntDormTypeEntity::getStatus, EnableEnum.ENABLE.intKey());
		
		Page<EntDormTypeEntity> page = new Page<>(pager.getPageNumber(), pager.getPageSize());
		page = baseDao.selectPage(page, lw);
		
		return getPageData(page, currentDtoClass());
	}


}