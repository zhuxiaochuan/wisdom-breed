package io.renren.common.dto;


import io.renren.common.enums.EnableEnum;
import io.renren.common.utils.EnumUtil;
import io.renren.common.utils.Util;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public abstract class EnableDto {
	
	@ApiModelProperty(value = "状态")
	protected Integer status ;
	
	@ApiModelProperty(value = "状态名称")
	protected String statusName ;
	
	public String getStatusName() {
		if(!Util.isEmpty(status)) {
			statusName = EnumUtil.getValue(EnableEnum.class, status);
		}
		return statusName;
	}

}
