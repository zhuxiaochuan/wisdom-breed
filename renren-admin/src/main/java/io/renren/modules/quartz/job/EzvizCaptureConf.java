package io.renren.modules.quartz.job;

import java.io.Serializable;

import lombok.Data;

@Data
public class EzvizCaptureConf implements Serializable {
	
    private static final long serialVersionUID = 1L;
    
    private String time ;
    
    /**
     * 时限(分钟)
     */
    private int limit = 60;
    
    /**
     * 重试间隔(分钟)
     */
    private int interval = 1;

}