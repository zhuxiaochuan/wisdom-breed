package io.renren.modules.weapp.revision.form;

import io.renren.modules.weapp.revision.enums.QueryDateTypeEnum;
import lombok.Data;

@Data
public class WxRevisionQueryForm {
	
	// 企业
	private Long enterpriseId;
	
	// 场地
	private Long placeId;
	
	// 默认按周查询
	private Integer queryType = QueryDateTypeEnum.WEEK.intKey() ;
	
}
