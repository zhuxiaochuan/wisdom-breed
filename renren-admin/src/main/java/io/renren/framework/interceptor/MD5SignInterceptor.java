package io.renren.framework.interceptor;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import cn.hutool.http.HttpStatus;
import io.renren.common.utils.HttpResponseUtil;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.framework.access.AccessConfig;
import io.renren.framework.access.AccessCore;

public class MD5SignInterceptor implements  HandlerInterceptor{
	
	protected Logger logger  = LoggerFactory.getLogger(MD5SignInterceptor.class) ; 
	
	/**
     * 请求处理之前
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
    	
    	/**
		 * 请求参数:
		 * 有时像checkbox这样的组件会有一个name对应对个value的时候，所以该Map中键值对是<String,String[]>的实现
		 */
		@SuppressWarnings("unchecked")
		Map<String,String[]> sParaTemp = request.getParameterMap() ;
		Map<String,String> spara = new HashMap<String, String>() ;
		
		Set<String> keys = sParaTemp.keySet() ;
		if(!Util.isEmpty(keys)){
			for(String key : keys){
				String[] value = sParaTemp.get(key) ;
				
				//因为不存在checkbox之类的参数传递，因此取第一个值即可
				String newVal = null ;
				if(null != value && value.length > 0){
					newVal = value[0] ;
				}
				spara.put(key, newVal) ;
			}
		}
		
		String sign = (String) spara.get("sign") ;
		logger.info("请求端签名值:" + sign) ;
		if(!Util.isEmpty(sign)){
			boolean verify = AccessCore.verify(spara, AccessConfig.terminal_secret) ;
		    if(verify){
		    	return true ;
		    }else{
		    	HttpResponseUtil.jsonView(response,HttpStatus.HTTP_UNAUTHORIZED,RR.failed("签名认证失败"));
				return false;
		    }
		}else{
			HttpResponseUtil.jsonView(response,HttpStatus.HTTP_UNAUTHORIZED,RR.failed("签名不能为空"));
			return false;
		}
    }
 
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }
 
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    }
}
