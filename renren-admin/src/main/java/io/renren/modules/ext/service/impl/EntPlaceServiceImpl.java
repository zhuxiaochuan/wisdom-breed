package io.renren.modules.ext.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tangzc.mpe.bind.Binder;

import io.renren.common.enums.EnableEnum;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.EnableService;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntPlaceDao;
import io.renren.modules.ext.dao.EntPlaceTypeDao;
import io.renren.modules.ext.dao.EnterpriseDao;
import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.entity.EntPlaceEntity;
import io.renren.modules.ext.entity.EntPlaceTypeEntity;
import io.renren.modules.ext.entity.EnterpriseEntity;
import io.renren.modules.ext.form.place.PlaceListQueryForm;
import io.renren.modules.ext.service.EntPlaceService;
import lombok.RequiredArgsConstructor;

/**
 * 场地
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Service
@RequiredArgsConstructor
public class EntPlaceServiceImpl extends CrudServiceImpl<EntPlaceDao, EntPlaceEntity, EntPlaceDTO> implements EntPlaceService {
	
	// 引入关联实体dao
	private final EntPlaceTypeDao entPlaceTypeDao ;
	
	private final EntPlaceDao entPlaceDao ;
	
	private final EnterpriseDao enterpriseDao ;
	
	private final EnableService<EntPlaceEntity> enableService;

    @Override
    public QueryWrapper<EntPlaceEntity> getWrapper(Map<String, Object> params){
    	String status = (String)params.get("status");

        QueryWrapper<EntPlaceEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(status), "status", status);

        return wrapper;
    }
    
    @Override
    public PageData<EntPlaceDTO> page(Map<String, Object> params) {
        IPage<EntPlaceEntity> page = baseDao.selectPage(
            getPage(params, null, false),
            getWrapper(params)
        );
        
        // 查询填充关联字段
        Binder.bind(page);
        
        return getPageData(page, currentDtoClass()); 
    }
    
    private void addRelateInfo(IPage<EntPlaceEntity> page) {  
    	List<EntPlaceEntity> records = page.getRecords();
    	if(!Util.isEmpty(records)) {
    		// 提取关联字段，方便批量查询
            Set<Long> rids = records.stream().map(EntPlaceEntity::getPlaceType).collect(Collectors.toSet());
            
            // 查询关联实体
            List<EntPlaceTypeEntity> rLst = entPlaceTypeDao.selectList(Wrappers.lambdaQuery(EntPlaceTypeEntity.class).in(EntPlaceTypeEntity::getId, rids)); 
            // 构造映射关系，方便key -> value匹配
            Map<Long, String> map = rLst.stream().collect(Collectors.toMap(EntPlaceTypeEntity::getId, EntPlaceTypeEntity::getTypeName));
            // 将补充信息添加到实体中
            page.convert(e -> {
            	e.setPlaceTypeName(map.get(e.getPlaceType()));
            	return e ;
            });  
    	}
    }

	@Override
	public PageData<EntPlaceDTO> findEnableList(Map<String, Object> params,Pager pager) {  
		String placeName = (String) params.get("name") ;
    	Long enterpriseId = Long.valueOf(params.get("enterpriseId") +"")  ;
    	
    	LambdaQueryWrapper<EntPlaceEntity> lw = Wrappers.lambdaQuery(EntPlaceEntity.class);
		if(!Util.isEmpty(placeName)) { 
			lw.like(EntPlaceEntity::getPlaceName, placeName);
		}
		if(!Util.isEmpty(enterpriseId)) { 
			lw.eq(EntPlaceEntity::getEnterpriseId, enterpriseId);
		}
		lw.eq(EntPlaceEntity::getStatus, EnableEnum.ENABLE.intKey());
		
		Page<EntPlaceEntity> page = new Page<>(pager.getPageNumber(), pager.getPageSize());
		page = baseDao.selectPage(page, lw);
		return getPageData(page, currentDtoClass());
	}

	@Override
	public PageData<EntPlaceDTO> pageData(PlaceListQueryForm form, Pager pager) {
		List<EntPlaceDTO> list = entPlaceDao.listData(form, pager) ;   
		int total = entPlaceDao.listDataCount(form);  
		return getPageData(list, total, EntPlaceDTO.class); 
	}

	@Transactional
	public RR enable(Long id) {
		RR r = enableService.enable(id, baseDao);  
		return r ;
	}

	@Transactional
	public RR disable(Long id) {
		RR r = enableService.disable(id, baseDao);  
		return r ;
	}

	@Override
	public EntPlaceDTO placeDetail(Long id) {
		EntPlaceDTO dto = get(id) ; 
    	if(!Util.isEmpty(dto)) {
    		Long enterpriseId = dto.getEnterpriseId(); 
        	EnterpriseEntity enterprise = enterpriseDao.selectById(enterpriseId); 
        	if(!Util.isEmpty(enterprise)) {
        		dto.setEnterpriseName(enterprise.getShortName()); 
        	}
        	
    	}
    	return dto ;
	}

	@Override
	public EntPlaceEntity selectEnabledById(Serializable id) { 
		LambdaQueryWrapper<EntPlaceEntity> qw = Wrappers.lambdaQuery(EntPlaceEntity.class).eq(EntPlaceEntity::getId, id).eq(EntPlaceEntity::getStatus, EnableEnum.ENABLE.intKey()) ;
		EntPlaceEntity one = entPlaceDao.selectOne(qw); 
		Binder.bindOn(one, EntPlaceEntity::getKeyWord);
		return one;
	}
}