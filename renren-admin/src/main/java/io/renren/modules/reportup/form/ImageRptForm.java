package io.renren.modules.reportup.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ImageRptForm {

	@NotNull(message="上报类型不能为空")
	private Integer type;
	
	@NotEmpty(message="录像机SN不能为空")
	private String videoSn; 
	
	@NotNull(message="通道号不能为空")
	private Integer channel; 
	
	@ApiModelProperty(value = "图片地址")
	private String imageUrl;

}
