package io.renren.modules.weapp.index.form;

import java.util.Date;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

@Data
public class WxIndexQueryForm {
	
	// 企业
	private Long enterpriseId;
	
	// 场地
	private Long placeId;
	
	/*创建时间*/
	private Date date ; 
	
	public Date getEndTime() {
		if(!Util.isEmpty(date)) {
			date = DateUtil.endOfDay(date); 
		}
		return date ;
	}
	
}
