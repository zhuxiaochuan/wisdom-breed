package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 图片上传表
 *
 * @author zxc 
 * @since 1.0.0 2022-07-23
 */
@Data
@ApiModel(value = "图片上传表")
public class EntImageUploadDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "id")
	private Long id;
	
	@ApiModelProperty(value = "任务id")
	private Long taskId;

	@ApiModelProperty(value = "企业id")
	private Long enterpriseId;

	@ApiModelProperty(value = "场地id")
	private Long placeId;

	@ApiModelProperty(value = "栋舍id")
	private Long dormId;

	@ApiModelProperty(value = "摄像头id")
	private Long cameraId;

	@ApiModelProperty(value = "抓取状态")
	private Integer catchStatus;

	@ApiModelProperty(value = "上传状态")
	private Integer uploadStatus;

	@ApiModelProperty(value = "图片url")
	private String imageUrl;

	@ApiModelProperty(value = "创建日期")
	private Date createDate;

	@ApiModelProperty(value = "抓取时间")
	private Date catchDate;

	@ApiModelProperty(value = "上传时间")
	private Date uploadDate;
	
	private Integer deliver;
	private String enterpriseName;
	private String placeName;
	private String dormTypeName;
	private String dormName;
	
	private String cameraSn;
	private String cameraName;
	private int revisionCount;
	private int delivered;
	
	private String videoSn;
	private Integer channel;
}