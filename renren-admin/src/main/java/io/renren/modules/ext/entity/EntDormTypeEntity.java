package io.renren.modules.ext.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.renren.common.entity.BaseEntity;
import io.renren.modules.ext.enums.YesOrNoEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 栋舍类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_dorm_type")
public class EntDormTypeEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 类型名称
     */
	private String typeName;
	
	/**
	 * 是否分娩舍
	 */
	private Integer deliver = YesOrNoEnum.NO.intKey();
	
    /**
     * 更新者
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;
	/**
     * 状态
     */
	private Integer status;
}