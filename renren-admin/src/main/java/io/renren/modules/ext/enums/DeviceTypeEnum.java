package io.renren.modules.ext.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.renren.common.enums.IEnum;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum DeviceTypeEnum implements IEnum {
	
	CAMERA(1,"摄像头"),IPC(2,"工控机");

	@EnumValue
	private int key;

    private String value;
    
	DeviceTypeEnum(Integer key, String value){
        this.key = key;
        this.value = value;
    }

    @Override
	public String key() {
		return String.valueOf(key);
	}

	@Override
	public String value() {
		return value;
	}

	public int intKey() {
		return key;
	}


}
