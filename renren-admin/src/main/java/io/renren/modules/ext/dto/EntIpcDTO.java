package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 工控机改成硬盘录像机表名还用之前的只改字段
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@ApiModel(value = "硬盘录像机")
public class
EntIpcDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "id")
	private Long id;

	@ApiModelProperty(value = "企业id")
	private Long enterpriseId;

	@ApiModelProperty(value = "场地id")
	private Long placeId;

	@ApiModelProperty(value = "栋舍id")
	private Long dormId;

	@ApiModelProperty(value = "录像机SN")
	private String videoSn;

	@ApiModelProperty(value = "IP地址")
	private String ip;

	@ApiModelProperty(value = "萤石云验证码")
	private String fluoriteCaptcha;

	@ApiModelProperty(value = "在线状态")
	private Integer onlineStatus;

	@ApiModelProperty(value = "厂家")
	private String producer;

	@ApiModelProperty(value = "型号")
	private String model;

	@ApiModelProperty(value = "接入类型1:有线宽带 2:4G路由")
	private Integer accessType;

	@ApiModelProperty(value = "创建者")
	private Long creator;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;

	@ApiModelProperty(value = "更新者")
	private Long updater;

	@ApiModelProperty(value = "更新时间")
	private Date updateDate;
	
	@ApiModelProperty(value = "上报时间")
	private Date reportTime;

	private String enterpriseName;
	private String placeName;
	private String dormName;
	private String updaterName;
	private String updaterMobile;
	private Integer cameraCount;

}