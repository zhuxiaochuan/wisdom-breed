package io.renren.modules.ext.dao;

import org.apache.ibatis.annotations.Mapper;

import io.renren.common.dao.BaseDao;
import io.renren.modules.ext.entity.EntRevisionDataEntity;

/**
 * 企业盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Mapper
public interface EntRevisionDataDao extends BaseDao<EntRevisionDataEntity> {
	
}