package io.renren.framework.jwt;

import lombok.Data;

/**
 * 小程序端JWT对应的用户信息，可根据实际需要添加字段
 * <p>
 * 注意: 不要将密码等重要信息放在JWT中
 * 
 * @author   朱晓川
 */
@Data
public class JwtMinappUserDto {
	// 用户id
	private Long userId;
	// 手机号
	private String tel;
	// 姓名
	private String realName;
	// 头像
	private String headUrl;

}
