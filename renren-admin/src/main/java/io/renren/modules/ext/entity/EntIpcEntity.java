package io.renren.modules.ext.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tangzc.mpe.bind.metadata.annotation.BindField;
import com.tangzc.mpe.bind.metadata.annotation.JoinCondition;

import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 工控机改成硬盘录像机表名还用之前的只改字段
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_ipc")
public class EntIpcEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 企业id
     */
	private Long enterpriseId;
    /**
     * 场地id
     */
	private Long placeId;

	/**
	 * 栋舍id
	 */
	private Long dormId;

     /**
     * 录像机SN
     */
	private String videoSn;
    /**
     * IP地址
     */
	private String ip;
    /**
     * 萤石云验证码
     */
	private String fluoriteCaptcha;
	/**
	 * 1:有线宽带 2:4G路由
	 */
	private Integer accessType;
    /**
     * 厂家
     */
	private String producer;
    /**
     * 型号
     */
	private String model;
	/**
	 * 上报时间
	 */
	private Date reportTime;
    /**
     * 更新者
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;

	// 关联字段，非本实体的字段
	@BindField(entity = EnterpriseEntity.class,field = "name",conditions = { @JoinCondition(selfField = "enterpriseId",joinField = "id") })
	@TableField(exist = false)
	private String enterpriseName;

	@BindField(entity = EntPlaceEntity.class,field = "placeName",conditions = { @JoinCondition(selfField = "placeId",joinField = "id") })
	@TableField(exist = false)
	private String placeName;
	
	@TableField(exist = false)
	private Integer onlineStatus;
	
}