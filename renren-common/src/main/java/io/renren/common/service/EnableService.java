package io.renren.common.service;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import io.renren.common.enums.EnableEnum;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;

/**
 * 单表数据启用禁用服务。
 * <p>
 * 用于根据id启用/禁用某条数据
 */
@Component
public class EnableService<T> { 
	
	@Transactional
	public RR enable(final Long id,BaseMapper<T> baseMapper) {
		if(Util.isEmpty(id)) {
			return RR.failed("id不能为空");
		}
		T entity = baseMapper.selectById(id); 
		if(Util.isEmpty(entity)) {
			return RR.failed("数据不存在");
		}
		
		UpdateWrapper<T> wrapper = new UpdateWrapper<>() ;
		wrapper.set("status", EnableEnum.ENABLE.intKey());
		wrapper.eq("id", id);
		baseMapper.update(entity, wrapper); 
		return RR.ok();  
	}
	
	@Transactional
	public RR disable(final Long id,BaseMapper<T> baseMapper) {
		if(Util.isEmpty(id)) {
			return RR.failed("id不能为空");
		}
		T entity = baseMapper.selectById(id); 
		if(Util.isEmpty(entity)) {
			return RR.failed("数据不存在");
		}
		
		UpdateWrapper<T> wrapper = new UpdateWrapper<>() ;
		wrapper.set("status", EnableEnum.DISABLE.intKey());
		wrapper.eq("id", id);
		
		baseMapper.update(entity, wrapper); 
		return RR.ok();  
	}

}
