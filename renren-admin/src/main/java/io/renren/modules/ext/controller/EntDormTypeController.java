package io.renren.modules.ext.controller;

import java.util.Map;

import io.renren.modules.ext.dto.EntDormDTO;
import io.renren.modules.ext.form.dorm.DormListQueryForm;
import io.renren.modules.ext.form.dormType.DormTypeListQueryForm;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.service.EntDormTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 栋舍类型
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@RestController
@RequestMapping("ext/entdormtype")
@Api(tags="栋舍类型")
public class EntDormTypeController {
    @Autowired
    private EntDormTypeService entDormTypeService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("ext:entdormtype:page")
    public Result<PageData<EntDormTypeDTO>> page(final DormTypeListQueryForm form, final Pager pager){
        PageData<EntDormTypeDTO> page = entDormTypeService.pageData(form,pager);

        return new Result<PageData<EntDormTypeDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("ext:entdormtype:info")
    public Result<EntDormTypeDTO> get(@PathVariable("id") Long id){
        EntDormTypeDTO data = entDormTypeService.get(id);

        return new Result<EntDormTypeDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("ext:entdormtype:save")
    public Result save(@RequestBody EntDormTypeDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        entDormTypeService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("ext:entdormtype:update")
    public Result update(@RequestBody EntDormTypeDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        entDormTypeService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("ext:entdormtype:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        entDormTypeService.delete(ids);

        return new Result();
    }
    
    @GetMapping("listEnable")
    public Result<PageData<EntDormTypeDTO>> listEnable(@RequestParam Map<String, Object> params,final Pager pager){
    	PageData<EntDormTypeDTO> data = entDormTypeService.findEnableList(params,pager);   

        return new Result<PageData<EntDormTypeDTO>>().ok(data);
    }

}