package io.renren.modules.ext.form.ipc;

import java.util.Date;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

@Data
public class IPCListQueryForm {
	
	private Long id;
	
	// 企业
	private String enterpriseName;
		
	// 场地
	private String placeName;
	
	// 关键字
	private String keyword;
	
	// 状态
	private Integer status;
	
	/*创建时间*/
	private Date beginTime ;
	private Date endTime ;
	
	private Integer onlineThreshold;
	
	
	public Date getBeginTime() {
		if(!Util.isEmpty(beginTime)) {
			beginTime = DateUtil.beginOfDay(beginTime);
		}
		return beginTime;
	}
	
	public Date getEndTime() {
		if(!Util.isEmpty(endTime)) {
			endTime = DateUtil.endOfDay(endTime); 
		}
		return endTime ;
	}
}
