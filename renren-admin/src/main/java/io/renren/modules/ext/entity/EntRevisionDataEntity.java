package io.renren.modules.ext.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 企业盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_revision_data")
public class EntRevisionDataEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 任务提交id
	 */
	private Long commitId;
    /**
     * 企业id
     */
	private Long enterpriseId;
    /**
     * 企业名称
     */
	private String enterpriseName;
    /**
     * 场地id
     */
	private Long placeId;
    /**
     * 场地名称
     */
	private String placeName;
    /**
     * 场地数量
     */
	private Integer placeCount;
    /**
     * 栋舍id
     */
	private Long dormId;
    /**
     * 栋舍名称
     */
	private String dormName;
    /**
     * 栋舍数量
     */
	private Integer dormCount;
    /**
     * 栋舍类型
     */
	private Integer dormType;
	/**
	 * 是否分娩舍
	 */
	private Integer deliver;
    /**
     * 栋舍类型名称
     */
	private String dormTypeName;
    /**
     * 盘点数量
     */
	private Integer revisionCount = 0;
	private Integer deliverCount = 0;
	
	public void setRevisionCount(Integer revisionCount) {
		this.revisionCount = (null == revisionCount ? 0 : revisionCount) ;
	}
	
	public void setDeliverCount(Integer deliverCount) {
		this.deliverCount = (null == deliverCount ? 0 : deliverCount) ;
	}
}