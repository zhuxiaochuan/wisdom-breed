package io.renren.modules.ext.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.annotation.LogOperation;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.utils.RR;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.ext.dto.EntPlaceDTO;
import io.renren.modules.ext.form.place.PlaceListQueryForm;
import io.renren.modules.ext.service.EntPlaceService;
import io.swagger.annotations.Api;


/**
 * 场地
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@RestController
@RequestMapping("ext/entplace")
@Api(tags="场地")
public class EntPlaceController {
    @Autowired
    private EntPlaceService entPlaceService;

    @GetMapping("page")
    @RequiresPermissions("ext:entplace:page")
    public Result<PageData<EntPlaceDTO>> page(final PlaceListQueryForm form,final Pager pager){
        PageData<EntPlaceDTO> page = entPlaceService.pageData(form,pager);
        return new Result<PageData<EntPlaceDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @RequiresPermissions("ext:entplace:info")
    public Result<EntPlaceDTO> get(@PathVariable("id") Long id){
        EntPlaceDTO data = entPlaceService.placeDetail(id);

        return new Result<EntPlaceDTO>().ok(data);
    }

    @PostMapping
    @LogOperation("保存")
    @RequiresPermissions("ext:entplace:save")
    public Result save(@RequestBody EntPlaceDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        entPlaceService.save(dto);

        return new Result();
    }

    @PutMapping
    @LogOperation("修改")
    @RequiresPermissions("ext:entplace:update")
    public Result update(@RequestBody EntPlaceDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        entPlaceService.update(dto);

        return new Result();
    }

    @RequestMapping(value = { "/enable" })
    @LogOperation("启用")
    public RR enable(@RequestBody final EntPlaceDTO dto) {
		return entPlaceService.enable(dto.getId());   
	}
    
    @RequestMapping(value = { "/disable" })
    @LogOperation("禁用")
    public RR disable(@RequestBody final EntPlaceDTO dto) {
		return entPlaceService.disable(dto.getId());   
	}
    
    @GetMapping("listEnable")
    public Result<PageData<EntPlaceDTO>> listEnable(@RequestParam Map<String, Object> params,final Pager pager){
    	PageData<EntPlaceDTO> data = entPlaceService.findEnableList(params,pager);   

        return new Result<PageData<EntPlaceDTO>>().ok(data);
    }
    
    @DeleteMapping
    @LogOperation("删除")
    @RequiresPermissions("ext:entplace:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        entPlaceService.delete(ids);

        return new Result();
    }

}