package io.renren.modules.ext.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tangzc.mpe.bind.metadata.annotation.BindField;
import com.tangzc.mpe.bind.metadata.annotation.JoinCondition;

import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 场地
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("ent_place")
public class EntPlaceEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 企业id
     */
	private Long enterpriseId;
    /**
     * 场地类型
     */
	private Long placeType;
    /**
     * 场地名称
     */
	private String placeName;
    /**
     * 更新者
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;
	
	/**
     * 状态
     */
	private Integer status;
	
	// 需要关联查询,非数据库字段
	@BindField(entity = EntPlaceTypeEntity.class,field = "typeName",conditions = { @JoinCondition(selfField = "placeType",joinField = "id") })
	@TableField(exist = false) 
	private String placeTypeName;
	
	@BindField(entity = EntPlaceTypeEntity.class,field = "keyWord",conditions = { @JoinCondition(selfField = "placeType",joinField = "id") })
	@TableField(exist = false) 
	private String keyWord;
	
	 /**
     * 地区代码
     */
	private String areaCode;
    /**
     * 详细地址
     */
	private String address;
    /**
     * 联系人
     */
	private String contactPerson;
    /**
     * 联系电话
     */
	private String contactNumber;
}