package io.renren.modules.ext.service;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.modules.ext.dto.EntRevisionDataDTO;
import io.renren.modules.ext.entity.EntRevisionDataEntity;
import io.renren.modules.ext.form.revision.EnterpriseRevisionQueryForm;

/**
 * 企业盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
public interface EntRevisionDataService extends CrudService<EntRevisionDataEntity, EntRevisionDataDTO> {
	
	public PageData<EntRevisionDataDTO> pageData(final EnterpriseRevisionQueryForm form,Pager pager);

}