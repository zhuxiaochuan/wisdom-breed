package io.renren.modules.weapp.revision.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;


@Data
public class DeliverSheetDto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    // 日期
    private Date day;
    
    // 类型id
    private Long id ;

    // 类型名称
	private String name;
	
	private Long dormId ;
	
	private String dormName;
	
	// 数量
	private Integer revisionCount = 0; 
	// 已分娩
	private Integer deliverCount = 0; 
	
	public void setRevisionCount(Integer revisionCount) { 
		this.revisionCount = (null == revisionCount ? 0 : revisionCount) ;
	}
	
	public void setDeliverCount(Integer deliverCount) { 
		this.deliverCount = (null == deliverCount ? 0 : deliverCount) ;
	}

}