package io.renren.modules.weapp.login.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import io.renren.common.utils.IpUtils;
import io.renren.common.utils.RR;
import io.renren.common.utils.Util;
import io.renren.framework.jwt.JwtMinappUserDto;
import io.renren.framework.jwt.UserTokenManager;
import io.renren.modules.ext.dao.EntUserManageDao;
import io.renren.modules.ext.dto.EntUserManageDTO;
import io.renren.modules.ext.dto.UserPlaceDTO;
import io.renren.modules.ext.service.EntUserManageService;
import io.renren.modules.log.entity.SysLogLoginEntity;
import io.renren.modules.log.enums.LoginOperationEnum;
import io.renren.modules.log.enums.LoginStatusEnum;
import io.renren.modules.log.service.SysLogLoginService;
import io.renren.modules.security.password.PasswordUtils;
import io.renren.modules.sys.dto.SysUserDTO;
import io.renren.modules.sys.enums.UserStatusEnum;
import io.renren.modules.sys.service.SysUserService;
import io.renren.modules.weapp.login.form.WxLoginForm;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class WxLoginService {
	
	private final SysUserService sysUserService ;
	
	private final EntUserManageDao entUserManageDao ;
	
	private final EntUserManageService entUserManageService;
	
	private final SysLogLoginService sysLogLoginService;
	
	/**
	 * 登录
	 */
	public RR login(HttpServletRequest request,final WxLoginForm form) {
		String tel = form.getTel(); 
		String inputPassword = form.getPassword(); 
		
		// 用户信息
		SysUserDTO user = sysUserService.getByTel(tel);  
		if(Util.isEmpty(user)) { 
			return RR.failed("用户不存在");
		}

		// 密码错误
		if(!PasswordUtils.matches(inputPassword, user.getPassword())){
			return RR.failed("密码错误");
		}

		// 账号停用
		if(user.getStatus() == UserStatusEnum.DISABLE.value()){
			return RR.failed("账号停用");
		}
		
		Long userId = user.getId(); 
		// 场地权限
		List<UserPlaceDTO> userPlaceList = entUserManageDao.userPlaceList(userId, null) ; 
		if(Util.isEmpty(userPlaceList)) { 
			return RR.failed("无任何场地权限，请联系管理员");
		}
		UserPlaceDTO up = userPlaceList.get(0) ;
		
		//登录成功
		SysLogLoginEntity log = new SysLogLoginEntity();
		log.setOperation(LoginOperationEnum.LOGIN.value());
		log.setCreateDate(new Date());
		log.setIp(IpUtils.getIpAddr(request));
		log.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));
		log.setIp(IpUtils.getIpAddr(request));
		log.setStatus(LoginStatusEnum.SUCCESS.value());
		log.setCreator(user.getId());
		log.setCreatorName(user.getRealName());
		sysLogLoginService.save(log);
		
		// 登录成功 生成jwt
		JwtMinappUserDto dto = new JwtMinappUserDto();
		dto.setUserId(userId);
		dto.setTel(user.getMobile());
		dto.setRealName(user.getRealName()); 
		dto.setHeadUrl(user.getHeadUrl()); 
		
		user.setEnterpriseId(up.getEnterpriseId());
		user.setEnterpriseName(up.getEnterpriseName());
		user.setPlaceId(up.getPlaceId());
		user.setPlaceName(up.getPlaceName());
		
		// 不返回明文密码
		user.setClearPassword(null);
		
		EntUserManageDTO userDetail = entUserManageService.userDetail(userId, up.getEnterpriseId()); 
		if(!Util.isEmpty(userDetail)) {
			user.setJob(userDetail.getJob());  
		} 
		
		String jwt = UserTokenManager.generateToken4Minapp(dto); 
		Map<String,Object> data = new HashMap<>() ;
		data.put("user", user) ;
		data.put("token", jwt) ;
		return RR.ok(data) ;
	}


}