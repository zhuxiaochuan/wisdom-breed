package io.renren.modules.quartz.payload;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * payload 必须实现序列化，否则无法存储到数据库
 */
@Data
public class CapturePayloadDto implements Serializable{ 
	
	private static final long serialVersionUID = 1L;
	
	@NotNull(message = "任务执行时间不能为空")
	private Date startDate ;
	
	private String taskId ;
	
	/**
	 * 录像机SN
	 */
	private String videoSn ;
	
	/**
	 * 通道号
	 */
	private Integer channel;

}
