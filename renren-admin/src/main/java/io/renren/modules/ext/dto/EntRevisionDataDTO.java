package io.renren.modules.ext.dto;

import java.io.Serializable;
import java.util.Date;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 企业盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Data
@ApiModel(value = "企业盘点")
public class EntRevisionDataDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Long id;
	
	@ApiModelProperty(value = "提交记录id")
	private Long commitId;

	@ApiModelProperty(value = "企业id")
	private Long enterpriseId;

	@ApiModelProperty(value = "企业名称")
	private String enterpriseName;

	@ApiModelProperty(value = "场地id")
	private Long placeId;

	@ApiModelProperty(value = "场地名称")
	private String placeName;

	@ApiModelProperty(value = "场地数量")
	private Integer placeCount;

	@ApiModelProperty(value = "栋舍id")
	private Long dormId;

	@ApiModelProperty(value = "栋舍名称")
	private String dormName;

	@ApiModelProperty(value = "栋舍数量")
	private Integer dormCount;

	@ApiModelProperty(value = "栋舍类型")
	private Integer dormType;
	
	@ApiModelProperty(value = "是否分娩舍")
	private Integer deliver;

	@ApiModelProperty(value = "栋舍类型名称")
	private String dormTypeName;

	@ApiModelProperty(value = "盘点数量")
	private Integer revisionCount;
	
	@ApiModelProperty(value = "已分娩")
	private Integer deliverCount;

	@ApiModelProperty(value = "创建者")
	private Long creator;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;
	private String day;
	
	@ApiModelProperty(value = "相对于前一天的增量")
	private Integer deltaLastCount;
	
	// 未分娩相对前天
	private Integer deltaLastCountUn;
	
	public String getDay() {
		if(!Util.isEmpty(createDate)) {
			day = DateUtil.format(createDate, "yyyy-MM-dd") ;
		}
		return day ;
	}


}