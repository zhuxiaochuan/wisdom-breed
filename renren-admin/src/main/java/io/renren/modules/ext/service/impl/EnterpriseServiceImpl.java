package io.renren.modules.ext.service.impl;

import java.util.List;
import java.util.Map;

import io.renren.modules.ext.dao.EntPlaceTypeDao;
import io.renren.modules.ext.dto.EntPlaceTypeDTO;
import io.renren.modules.ext.form.enterprise.EnterpriseListQueryForm;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.renren.common.enums.EnableEnum;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EnterpriseDao;
import io.renren.modules.ext.dto.EnterpriseDTO;
import io.renren.modules.ext.entity.EnterpriseEntity;
import io.renren.modules.ext.service.EnterpriseService;

/**
 * 企业
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Service
@RequiredArgsConstructor
public class EnterpriseServiceImpl extends CrudServiceImpl<EnterpriseDao, EnterpriseEntity, EnterpriseDTO> implements EnterpriseService {

	private final EnterpriseDao enterpriseDao ;

    @Override
    public QueryWrapper<EnterpriseEntity> getWrapper(Map<String, Object> params){
    	String status = (String)params.get("status");

        QueryWrapper<EnterpriseEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(status), "status", status);

        return wrapper;
    }

	@Override
	public PageData<EnterpriseDTO> pageData(EnterpriseListQueryForm form, Pager pager) {
		List<EnterpriseDTO> list = enterpriseDao.listData(form, pager) ;
		int total = enterpriseDao.listDataCount(form);
		return getPageData(list, total, EnterpriseDTO.class);
	}

	@Override
	public PageData<EnterpriseDTO> findEnableList(Map<String, Object> params, Pager pager) {
		return null;
	}

	@Override
	public PageData<EnterpriseDTO> findEnableList(String name,Pager pager) {
		LambdaQueryWrapper<EnterpriseEntity> lw = Wrappers.lambdaQuery(EnterpriseEntity.class); 
		if(!Util.isEmpty(name)) {
			lw.like(EnterpriseEntity::getName, name);
		}
		lw.eq(EnterpriseEntity::getStatus, EnableEnum.ENABLE.intKey());
		
		Page<EnterpriseEntity> page = new Page<>(pager.getPageNumber(), pager.getPageSize());
		page = baseDao.selectPage(page, lw);
		return getPageData(page, currentDtoClass());
	}


}