package io.renren.modules.ext.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tangzc.mpe.bind.Binder;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.ext.dao.EntPlaceTypeDao;
import io.renren.modules.ext.dao.EntRouterDao;
import io.renren.modules.ext.dto.EntIpcDTO;
import io.renren.modules.ext.dto.EntPlaceTypeDTO;
import io.renren.modules.ext.dto.EntRouterDTO;
import io.renren.modules.ext.entity.EntIpcEntity;
import io.renren.modules.ext.entity.EntRouterEntity;
import io.renren.modules.ext.form.placeType.PlaceTypeListQueryForm;
import io.renren.modules.ext.form.router.RouterListQueryForm;
import io.renren.modules.ext.service.EntRouterService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 路由器
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Service
@RequiredArgsConstructor
public class EntRouterServiceImpl extends CrudServiceImpl<EntRouterDao, EntRouterEntity, EntRouterDTO> implements EntRouterService {

    private final EntRouterDao entRouterDao ;

    @Override
    public QueryWrapper<EntRouterEntity> getWrapper(Map<String, Object> params){
        String status = (String)params.get("status");

        QueryWrapper<EntRouterEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(status), "status", status);

        return wrapper;
    }

    @Override
    public PageData<EntRouterDTO> page(Map<String, Object> params) {
        IPage<EntRouterEntity> page = baseDao.selectPage(
                getPage(params, null, false),
                getWrapper(params)
        );

        // 查询填充关联字段
        Binder.bind(page);

        return getPageData(page, currentDtoClass());
    }

    @Override
    public PageData<EntRouterDTO> pageData(RouterListQueryForm form, Pager pager) {
        List<EntRouterDTO> list = entRouterDao.listData(form, pager) ;
        int total = entRouterDao.listDataCount(form);
        return getPageData(list, total, EntRouterDTO.class);
    }

    @Override
    public PageData<EntRouterDTO> findEnableList(Map<String, Object> params, Pager pager) {
        return null;
    }
}