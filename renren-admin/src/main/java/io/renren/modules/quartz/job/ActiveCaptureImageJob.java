package io.renren.modules.quartz.job;

import java.io.Serializable;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.hutool.core.date.DateUtil;
import io.renren.modules.quartz.service.CaptureImageService;
import lombok.extern.slf4j.Slf4j;

/**
 * 激活每个摄像头图片抓取任务的JOB
 * 
 * @author   朱晓川
 */
@Component
@Slf4j
public class ActiveCaptureImageJob implements Job ,Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 注入业务service
	 */
	@Autowired
	private CaptureImageService captureImageService ;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("=======================执行激活任务,时间:" + DateUtil.date()); 
		try {
			captureImageService.startCptureImgTask() ;
        } catch (Exception e) {
        	// sleep for 1 minute
        	try {
				Thread.sleep(1 * 60000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			} 
        	
        	// fire it again
        	JobExecutionException jobEx = new JobExecutionException(e); 
        	jobEx.setRefireImmediately(true);
        	throw jobEx;
        }
	}
}
