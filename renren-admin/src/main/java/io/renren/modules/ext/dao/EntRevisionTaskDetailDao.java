package io.renren.modules.ext.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.ext.entity.EntRevisionTaskDetailEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 任务详情
 *
 * @author zxc 
 * @since 1.0.0 2022-07-27
 */
@Mapper
public interface EntRevisionTaskDetailDao extends BaseDao<EntRevisionTaskDetailEntity> {
	
}