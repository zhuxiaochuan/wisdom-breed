package io.renren.modules.ext.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.entity.EntRevisionTaskEntity;
import io.renren.modules.ext.form.revision.EnterpriseRevisionTaskQueryForm;

/**
 * 任务管理
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Mapper
public interface EntRevisionTaskDao extends BaseDao<EntRevisionTaskEntity> {
	
	public List<EntRevisionTaskEntity> listData(@Param("form") final EnterpriseRevisionTaskQueryForm form, @Param("pager") Pager pager);
	
	public int listDataCount(@Param("form") final EnterpriseRevisionTaskQueryForm form);
	
	public void insertBatchXml(List<EntRevisionTaskEntity> list);
	
}