package io.renren.framework.vendors.ezviz.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.base.Preconditions;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import io.renren.common.utils.Util;
import io.renren.framework.vendors.ezviz.dto.EzvizDeviceInfoDto;
import io.renren.framework.vendors.ezviz.dto.EzvizImage;
import io.renren.framework.vendors.ezviz.dto.EzvizLiveDto;
import io.renren.framework.vendors.ezviz.dto.EzvizResult;
import io.renren.framework.vendors.ezviz.form.EzvizLiveForm;
import io.renren.framework.vendors.ezviz.util.EzvizCloudUtils;
import io.renren.framework.vendors.oss.OSSFactory;

@Service
public class EzvizCloudService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EzvizCloudService.class);

    /**
     * 硬盘录像机抓拍
     * 
     * @param deviceSerial 序列号
     * @param channelNo    通道号
     * @return
     */
    public String deviceCapture(final String deviceSerial,final String channelNo) {
    	Preconditions.checkArgument(!Util.isEmpty(deviceSerial), "序列号非空");
    	Preconditions.checkArgument(!Util.isEmpty(channelNo), "通道号非空");
    	
    	String url = "https://open.ys7.com/api/lapp/device/capture" ;
		Map<String, Object> params = new HashMap<>();
		
		String accessToken = EzvizCloudUtils.getAccessToken() ; 
		params.put("accessToken", accessToken);
		
		params.put("deviceSerial", deviceSerial);
		params.put("channelNo", channelNo);
		// 高清720p
		params.put("quality", 1);
		
		String json = HttpRequest.post(url).form(params).execute().body() ;
		EzvizResult result = JSONUtil.toBean(json, EzvizResult.class);
		if(Util.eq("200", result.getCode())) {  
			Object obj = result.getData();
			EzvizImage et = JSONUtil.toBean(obj.toString(), EzvizImage.class);
			String picUrl = et.getPicUrl();
			
			byte[] imgBytes = HttpUtil.downloadBytes(picUrl); 
			UriComponents uc = UriComponentsBuilder.fromUriString(picUrl).build();
			String ucpath = uc.getPath(); 
			String extension = FilenameUtils.getExtension(ucpath); 
			
			String imgUrl = OSSFactory.build().uploadSuffix(imgBytes, extension) ;
			return imgUrl ;
		}else if(Util.eq("10002", result.getCode())) {
			EzvizCloudUtils.invalidToken();
			throw new IllegalStateException("token已失效");
		}else {
			LOGGER.info("=======================录像机SN[" +deviceSerial + "]通道[" +channelNo+"]，抓拍失败.result:" + json) ;
			throw new IllegalStateException("抓拍失败");
		}
    }
    
    /**
     * 获取直播地址
     */
    public EzvizLiveDto deviceLiveUrl(final EzvizLiveForm form) {
    	Preconditions.checkNotNull(form) ;
    	Preconditions.checkArgument(!Util.isEmpty(form.getDeviceSerial()), "序列号非空");
    	Preconditions.checkArgument(!Util.isEmpty(form.getChannelNo()), "通道号非空");
    	
    	String url = "https://open.ys7.com/api/lapp/v2/live/address/get" ;
		Map<String, Object> params = new HashMap<>();
		
		String accessToken = EzvizCloudUtils.getAccessToken() ; 
		params.put("accessToken", accessToken);
		
		params.put("deviceSerial", form.getDeviceSerial());
		params.put("channelNo", form.getChannelNo());
		
		// 协议
		params.put("protocol", form.getProtocol());
		// 清晰度
		params.put("quality", form.getQuality());
		
		String code = form.getCode(); 
		if(!Util.isEmpty(code)) { 
			params.put("code", code);
		}
		
		String json = HttpRequest.post(url).form(params).execute().body() ;
		EzvizResult result = JSONUtil.toBean(json, EzvizResult.class);
		if(Util.eq("200", result.getCode())) {  
			Object obj = result.getData();
			EzvizLiveDto et = JSONUtil.toBean(obj.toString(), EzvizLiveDto.class);
			return et;
		}else if(Util.eq("10002", result.getCode())) {
			EzvizCloudUtils.invalidToken();
			throw new IllegalStateException("token已失效");
		}else{
			LOGGER.info(json); 
			throw new IllegalStateException("播放地址获取失败");
		}
    }
    
    /**
     * 查询设备状态
     */
    public EzvizDeviceInfoDto deviceInfo(final String deviceSerial) {
    	Preconditions.checkArgument(!Util.isEmpty(deviceSerial), "序列号非空");
    	
    	String url = "https://open.ys7.com/api/lapp/device/info" ;
		Map<String, Object> params = new HashMap<>();
		
		String accessToken = EzvizCloudUtils.getAccessToken() ; 
		params.put("accessToken", accessToken);
		params.put("deviceSerial", deviceSerial);
		
		String json = HttpRequest.post(url).form(params).execute().body() ;
		EzvizResult result = JSONUtil.toBean(json, EzvizResult.class);
		if(Util.eq("200", result.getCode())) {  
			Object obj = result.getData();
			EzvizDeviceInfoDto et = JSONUtil.toBean(obj.toString(), EzvizDeviceInfoDto.class);
			return et;
		}else if(Util.eq("10002", result.getCode())) {
			EzvizCloudUtils.invalidToken();
			throw new IllegalStateException("token已失效");
		}else {
			LOGGER.info(json); 
			throw new IllegalStateException("设备查询失败");
		}
    }
}