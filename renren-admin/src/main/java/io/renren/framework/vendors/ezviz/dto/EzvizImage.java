package io.renren.framework.vendors.ezviz.dto;

import lombok.Data;

@Data
public class EzvizImage {
	
	private String picUrl ;

}
