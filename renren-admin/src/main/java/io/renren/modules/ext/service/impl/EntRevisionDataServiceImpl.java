package io.renren.modules.ext.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.renren.common.enums.EnableEnum;
import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.Util;
import io.renren.modules.ext.dao.EntDormDao;
import io.renren.modules.ext.dao.EntPlaceDao;
import io.renren.modules.ext.dao.EntRevisionDataDao;
import io.renren.modules.ext.dto.DormTypeGroupCountDTO;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.dto.EntRevisionDataDTO;
import io.renren.modules.ext.entity.EntDormEntity;
import io.renren.modules.ext.entity.EntPlaceEntity;
import io.renren.modules.ext.entity.EntRevisionDataEntity;
import io.renren.modules.ext.enums.YesOrNoEnum;
import io.renren.modules.ext.form.revision.EnterpriseRevisionQueryForm;
import io.renren.modules.ext.service.EntDormTypeService;
import io.renren.modules.ext.service.EntRevisionDataService;
import lombok.RequiredArgsConstructor;

/**
 * 企业盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Service
@RequiredArgsConstructor
public class EntRevisionDataServiceImpl extends CrudServiceImpl<EntRevisionDataDao, EntRevisionDataEntity, EntRevisionDataDTO> implements EntRevisionDataService {
	
	private final EntRevisionDataDao entRevisionDataDao ;
	
	private final EntPlaceDao entPlaceDao ;
	
	private final EntDormDao entDormDao ;
	
	private final EntDormTypeService entDormTypeService ;
	
    @Override
    public QueryWrapper<EntRevisionDataEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<EntRevisionDataEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

	@Override
	public PageData<EntRevisionDataDTO> pageData(EnterpriseRevisionQueryForm form, Pager pager) {
		// 1 先按照企业分组进行分页查询
		QueryWrapper<EntRevisionDataEntity> qw = new QueryWrapper<>();
		
		String enterpriseName = form.getEnterpriseName();
		if(!Util.isEmpty(enterpriseName)) {
			qw.and(wrapper -> wrapper.like("enterprise_name", enterpriseName));
		}
		
		Date date = form.getDate(); 
		if(!Util.isEmpty(date)) {
			DateTime beginOfDay = DateUtil.beginOfDay(date); 
			DateTime endOfDay = DateUtil.endOfDay(date); 
			qw.and(wrapper -> wrapper.gt("create_date", beginOfDay)); 
			qw.and(wrapper -> wrapper.le("create_date", endOfDay)); 
		}
		
		qw.groupBy("enterprise_id","DATE_FORMAT(create_date, '%Y-%m-%d')");
		qw.orderByDesc("create_date");
		
		Page<EntRevisionDataEntity> page = new Page<>(pager.getPageNumber(), pager.getPageSize());
        Page<EntRevisionDataEntity> pg = entRevisionDataDao.selectPage(page, qw);
        
		// 2 查询栋舍详情数据并且设置到返回数据中
        Map<String,Object> extra = new HashMap<>() ;
       
        Map<Long, EntDormTypeDTO> dormTypeMap = entDormTypeService.queryEnable2Map(); 
        List<EntDormTypeDTO> dormTypeList = new ArrayList<>() ;
		Set<Long> dormTypeIdSet = dormTypeMap.keySet(); 
		if(!Util.isEmpty(dormTypeIdSet)) {
			for(Long id : dormTypeIdSet) {
				EntDormTypeDTO type = dormTypeMap.get(id); 
				dormTypeList.add(type) ;
			}
		}
		
		// 分别统计各栋舍类型的数量
		Map<String,DormTypeGroupCountDTO> tbleData = new HashMap<>() ;
		
        List<EntRevisionDataEntity> records = pg.getRecords();
        if(!Util.isEmpty(records)) {
        	List<Long> ids = records.stream().map(EntRevisionDataEntity::getEnterpriseId).collect(Collectors.toList());  
        	records.stream().forEach(r -> {
        		// 场地数量与栋舍数量
        		Long placeCount = entPlaceDao.selectCount(Wrappers.lambdaQuery(EntPlaceEntity.class).eq(EntPlaceEntity::getEnterpriseId, r.getEnterpriseId()).eq(EntPlaceEntity::getStatus,EnableEnum.ENABLE.intKey())); 
        		r.setPlaceCount(Integer.valueOf(placeCount + ""));
        		
        		Long dormCount = entDormDao.selectCount(Wrappers.lambdaQuery(EntDormEntity.class).eq(EntDormEntity::getEnterpriseId, r.getEnterpriseId()).eq(EntDormEntity::getStatus,EnableEnum.ENABLE.intKey())); 
        		r.setDormCount(Integer.valueOf(dormCount + ""));
        	});
        	
        	List<EntRevisionDataEntity> listData = entRevisionDataDao.selectList(Wrappers.lambdaQuery(EntRevisionDataEntity.class).in(EntRevisionDataEntity::getEnterpriseId, ids));
        	if(!Util.isEmpty(listData)) {
        		for(EntRevisionDataEntity one : listData) {
        			Long dormTypeId = one.getDormType().longValue();  
        			Date createDate = one.getCreateDate(); 
        			String day = DateUtil.format(createDate, "yyyy-MM-dd") ; 
        			
        			String key = one.getEnterpriseId() + "-" + dormTypeId + "-" + day;
        			
        			EntDormTypeDTO dormType = dormTypeMap.get(dormTypeId);
        			DormTypeGroupCountDTO cd = tbleData.get(key); 
        			if(null == cd) {
        				cd = new DormTypeGroupCountDTO() ;
        				cd.setLabel(dormType.getTypeName()); 
        				cd.setCount(one.getRevisionCount()); 
        				
        				tbleData.put(key, cd) ;
        			}else {
        				cd.setCount(cd.getCount() + one.getRevisionCount());
        			}
        			
        			// 企业按天统计 分娩数量(不包含非分娩舍)
    				if(Util.eq(dormType.getDeliver(), YesOrNoEnum.YES.intKey())) {
    					String deliverKey = one.getEnterpriseId() + "-已分娩-" + day; 
        				DormTypeGroupCountDTO deliverCd = tbleData.get(deliverKey);
    					if(null == deliverCd) {
    						deliverCd = new DormTypeGroupCountDTO() ;
    						deliverCd.setLabel(dormType.getTypeName()); 
    						deliverCd.setCount(one.getDeliverCount()); 
            				
            				tbleData.put(deliverKey, deliverCd) ;
        				}else {
        					deliverCd.setCount(deliverCd.getCount() + one.getDeliverCount());
        				}
    					
    					String undeliverKey = one.getEnterpriseId() + "-未分娩-" + day; 
        				DormTypeGroupCountDTO undeliverCd = tbleData.get(undeliverKey); 
        				if(null == undeliverCd) {
        					undeliverCd = new DormTypeGroupCountDTO() ;
    						undeliverCd.setLabel(dormType.getTypeName()); 
    						
    						Integer undeliverCount = one.getRevisionCount() - one.getDeliverCount();
    						undeliverCd.setCount(undeliverCount); 
    						
    						tbleData.put(undeliverKey, undeliverCd) ;
        					
        				}else {
        					Integer undeliverCount = one.getRevisionCount() - one.getDeliverCount();
        					undeliverCd.setCount(undeliverCd.getCount() + undeliverCount);
        				}
        			}
        		}
        		
        		// 补足不存在的栋舍类型数据
        		for(EntRevisionDataEntity one : listData) {
        			if(!Util.isEmpty(dormTypeList)) {
        				for(EntDormTypeDTO dt : dormTypeList) {
        					Long typeId = dt.getId(); 
        					Date createDate = one.getCreateDate(); 
        					String day = DateUtil.format(createDate, "yyyy-MM-dd") ;
                			String key = one.getEnterpriseId() + "-" + typeId + "-" + day; 
                			
                			DormTypeGroupCountDTO cd = tbleData.get(key); 
                			if(null == cd) {
                				cd = new DormTypeGroupCountDTO() ;
                				cd.setLabel(dt.getTypeName()); 
                				cd.setCount(0); 
                				
                				tbleData.put(key, cd) ;
                			}
        				}
        			}
        		}
        	}
        }
        
        PageData<EntRevisionDataDTO> pageData = getPageData(pg, EntRevisionDataDTO.class); 
        
        extra.put("dormTypeList", dormTypeList);
        extra.put("tbleData", tbleData);
        pageData.setExtra(extra); 
		return pageData; 
	}
}