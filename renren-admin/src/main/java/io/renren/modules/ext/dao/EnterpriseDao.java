package io.renren.modules.ext.dao;

import io.renren.common.dao.BaseDao;
import io.renren.common.page.Pager;
import io.renren.modules.ext.dto.EntDormTypeDTO;
import io.renren.modules.ext.dto.EnterpriseDTO;
import io.renren.modules.ext.entity.EnterpriseEntity;
import io.renren.modules.ext.form.dormType.DormTypeListQueryForm;
import io.renren.modules.ext.form.enterprise.EnterpriseListQueryForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 企业
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
@Mapper
public interface EnterpriseDao extends BaseDao<EnterpriseEntity> {
    public List<EnterpriseDTO> listData(@Param("form") final EnterpriseListQueryForm form, @Param("pager") Pager pager);

    public int listDataCount(@Param("form") final EnterpriseListQueryForm form);
}