package io.renren.modules.ext.service;

import java.util.List;

import io.renren.common.page.PageData;
import io.renren.common.page.Pager;
import io.renren.common.service.CrudService;
import io.renren.framework.vendors.ezviz.dto.EzvizDeviceInfoDto;
import io.renren.modules.ext.dto.EntIpcDTO;
import io.renren.modules.ext.entity.EntIpcEntity;
import io.renren.modules.ext.form.ipc.IPCListQueryForm;

/**
 * 工控机
 *
 * @author zxc 
 * @since 1.0.0 2022-07-13
 */
public interface EntIpcService extends CrudService<EntIpcEntity, EntIpcDTO> {
	
	public PageData<EntIpcDTO> pageData(IPCListQueryForm form, Pager pager);
	
	List<EntIpcEntity> findByPlace(final Long placeId) ;

	List<EzvizDeviceInfoDto> getDeviceInfo(List<String> deviceSerialList);

}