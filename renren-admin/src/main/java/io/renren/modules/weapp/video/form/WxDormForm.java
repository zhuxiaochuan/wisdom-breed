package io.renren.modules.weapp.video.form;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class WxDormForm {
	
	// 场地
	@NotNull(message="场地id不能为空")
	private Long placeId;
	
}
