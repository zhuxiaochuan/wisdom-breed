package io.renren.modules.ext.form.enterprise;

import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.Util;
import lombok.Data;

import java.util.Date;

@Data
public class EnterpriseListQueryForm {
	
	// 企业名称
	private String name;
		
	// 企业简称
	private String shortName;
	
	// 联系人
	private String contactPerson;
	
	/*创建时间*/
	private Date beginTime ;
	private Date endTime ;
	
	
	public Date getBeginTime() {
		if(!Util.isEmpty(beginTime)) {
			beginTime = DateUtil.beginOfDay(beginTime);
		}
		return beginTime;
	}
	
	public Date getEndTime() {
		if(!Util.isEmpty(endTime)) {
			endTime = DateUtil.endOfDay(endTime); 
		}
		return endTime ;
	}
}
