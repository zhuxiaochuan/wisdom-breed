package io.renren.framework.vendors.ezviz.dto;

import io.renren.framework.vendors.ezviz.enums.EzvizOnlineStatusEnum;
import lombok.Data;

@Data
public class EzvizDeviceInfoDto {
	
	private String deviceSerial ;
	private String deviceName ;
	private String model ;
	private String signal ;
	private String netAddress ;
	
	private int status = EzvizOnlineStatusEnum.OFFLINE.intKey() ;

}
