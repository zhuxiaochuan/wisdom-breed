package io.renren.modules.ext.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 场地盘点
 *
 * @author zxc 
 * @since 1.0.0 2022-07-14
 */
@Data
public class PlaceRevisionExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "企业id")
    private Long enterpriseId;
    @Excel(name = "企业名称")
    private String enterpriseName;
    @Excel(name = "场地id")
    private Long placeId;
    @Excel(name = "场地名称")
    private String placeName;
    @Excel(name = "场地数量")
    private Integer placeCount;
    @Excel(name = "栋舍id")
    private Long dormId;
    @Excel(name = "栋舍名称")
    private String dormName;
    @Excel(name = "栋舍数量")
    private Integer dormCount;
    @Excel(name = "栋舍类型")
    private Integer dormType;
    @Excel(name = "栋舍类型名称")
    private String dormTypeName;
    @Excel(name = "盘点数量")
    private Integer revisionCount;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}