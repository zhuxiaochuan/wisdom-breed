package io.renren.framework.vendors.quartz.form;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import cn.hutool.core.date.DateUnit;
import lombok.Data;

/**
 * 重复任务
 */
@Data
public class RepeatJobForm<T> implements Serializable{ 
	
	private static final long serialVersionUID = 1L;
	
	@NotNull(message = "任务执行时间不能为空")
	private Date startDate ;
	
	// 默认1分钟执行1次
	private int interval = 1 ;
	
	private DateUnit dateUnit = DateUnit.MINUTE;
	
	/**
	 * 任务id
	 * <p>
	 * 建议用有意义的值进行标记，比如[业务模块 + 关键数据id]。这里的设置不影响job的执行  
	 */
	private String taskId ;
	
	/**
	 * 需要保存到任务中的数据
	 */
	private T data ;

}
